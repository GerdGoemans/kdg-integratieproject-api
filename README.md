# Integratieproject 1 - 2019/2020

REST API geschreven in ASP.NET core voor de educatieve stemtest  
Een project door **Centennials**.

## Beschrijving

De backend is een REST API geschreven in ASP.NET core. Er wordt extensief gebruik gemaakt van **dependency injection** om alle services te injecteren in de controllers zonder al te veel _overhead_. Dit project bevat alle **business logica** van de educatieve stemtest. Als databron wordt er gebruik gemaakt van **Entitiy Framework Core**, het type database kan veranderd worden in het configuratie bestand _(zie instructies voor meer informatie)_.

## Contributors / Leden

Hieronder is een overzicht van alle leden die hebben deelgenomen aan dit project. Onderstaande lijst is alfabetisch gesorteerd op achternaam.

| Achternaam | Voornaam |
| ---------- | -------- |
| Anthonisen | Bert     |
| El Nasiri  | Issam    |
| Goemans    | Gerd     |
| Koyen      | Joren    |
| Maene      | Kevin    |
| Vroemans   | Anton    |

## Software vereisten

> Volgende software is nodig om de applicatie te kunnen _builden_ en _runnen_.

- [dotnet core](https://dotnet.microsoft.com/download) · v3.1

## Instructies

> Clone de repository naar een gewenste locatie met behulp van ssh of http.

```bash
git clone git@gitlab.com:kdg-ti/integratieproject-1/centennials/api.git
```

```bash
git clone https://gitlab.com/kdg-ti/integratieproject-1/centennials/api.git
```

---

Configureer de applicatie naar eigen wens in de `WebAPI/appsettings.json` file. Hieronder zie je een voorbeeld van de applicatie settings.

```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*",
  "AppSettings": {
    "Secret": "<random concatenation of characters>",
    "Namespace": "centenials.stemtest",
    "Origins": ["http://localhost:8080", "http://localhost"],
    "DatabaseType": "UseSqlite",
    "ConnectionString": "",
    "RedisConnection": "",
    "InitializeData": true,
    "SuperAdminEmail": "root@email.com",
    "SuperAdminPassword": "toor",
    "SendGrid": {
      "apiKey": "sendGridKeyHere",
      "name": "Stemtest LIVE",
      "mail": "noreply@stemtest.live"
    }
  }
}
```

> Om de applicatie op te starten moet u navigeren naar de root van de repository en het volgende command uitvoeren.

```bash
# start dotnet applicatie
dotnet run --project WebAPI
```
