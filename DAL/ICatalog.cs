﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CENT.DAL
{
    public interface ICatalog<T>
    {
        public Task<T> GetById(string id);

        public Task<List<T>> GetAll();

        public Task<T> Add(T t);

        public Task<List<T>> AddAll(T[] ts);

        public bool Delete(T t);
    }
}