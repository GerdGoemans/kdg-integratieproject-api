﻿using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain.UserPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.DAL.UserPackage

{
    public class UserCatalog : Catalog<User>
    {
        public UserCatalog(StemtestDbContext context)
        {
            Collection = context.Users;
        }

        public Task<User> GetByEmail(string email)
        {
            return Collection.FirstOrDefaultAsync(u => u.Email.ToLower() == email.ToLower());
        }

        public Task<User> GetByResetKey(string key)
        {
            return Collection.FirstOrDefaultAsync(u => u.ResetHash.ToLower() == key.ToLower());
        }

        public Task<int> GetTeacherCountBySchool(School school)
        {
            return Collection.CountAsync(user => user.Role == Role.TEACHER && ((Teacher) user).School == school);
        }

        public IQueryable<User> GetTeachersOrderedByDate(int limitAmount, School school)
        {
            return Collection.OrderByDescending(user => user.Role == Role.TEACHER && ((Teacher) user).School == school)
                .Take(limitAmount);
        }

        public bool CheckIfAnySuperAdminIsPresent()
        {
            return Collection.Any(u => u.Role == Role.SUPER_ADMIN);
        }
    }
}