﻿using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain.UserPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.DAL.UserPackage
{
    public class SchoolCatalog : Catalog<School>
    {
        public SchoolCatalog(StemtestDbContext context)
        {
            Collection = context.Schools;
        }

        public Task<School> GetSchoolByAdmin(string admin)
        {
            return Collection.FirstOrDefaultAsync(school => school.Admin.Id == admin);
        }

        public Task<School> GetSchoolByUser(User user)
        {
            if (user is Teacher teacher) return Collection.FirstOrDefaultAsync(school => school.Id == teacher.SchoolId);

            return Collection.FirstOrDefaultAsync(school => school.Admin == user);
        }

        public IQueryable<School> GetSchoolsOrderedByDate(int limitAmount)
        {
            return Collection.OrderByDescending(s => s.CreatedDate).Take(limitAmount);
        }
    }
}