﻿using System.Linq;
using CENT.BL.Domain.UsageDataPackage;

namespace CENT.DAL.UsageDataPackage
{
    public class PageVisitCatalog : Catalog<PageVisit>
    {
        public PageVisitCatalog(StemtestDbContext context)
        {
            Collection = context.PageVisits;
        }

        public IOrderedQueryable<PageVisitResult> GetPageVisits()
        {
            return Collection.GroupBy(pageVisit => pageVisit.ToPage)
                .Select(g => new PageVisitResult
                {
                    GroupByKey = g.Key,
                    Count = g.Count()
                }).OrderByDescending(result => result.Count);
        }
    }
}