﻿using CENT.BL.Domain.OrganisationPackage;

namespace CENT.DAL.OrganisationPackage
{
    public class FaqCatalog : Catalog<FAQ>
    {
        public FaqCatalog(StemtestDbContext context)
        {
            Collection = context.Faqs;
        }
    }
}