﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain.PartyPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.DAL.PartyPackage
{
    public class PartyCatalog : Catalog<Party>
    {
        public PartyCatalog(StemtestDbContext context)
        {
            Collection = context.Parties;
        }

        public Task<List<Party>> GetAllParties(bool includeArchived = false)
        {
            return Collection.Where(party => party.IsArchived == false || includeArchived).ToListAsync();
        }

        public Task<Party> GetByName(string name)
        {
            return Collection.FirstOrDefaultAsync(p => p.Name == name);
        }
    }
}