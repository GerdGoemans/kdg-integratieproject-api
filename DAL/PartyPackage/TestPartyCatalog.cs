using CENT.BL.Domain.PartyPackage;

namespace CENT.DAL.PartyPackage
{
    public class TestPartyCatalog : Catalog<TestParty>
    {
        public TestPartyCatalog(StemtestDbContext context)
        {
            Collection = context.TestParties;
        }
    }
}