using CENT.BL.Domain.TestPackage;

namespace CENT.DAL.TestPackage
{
    public class StatementCatalog : Catalog<Statement>
    {
        public StatementCatalog(StemtestDbContext context)
        {
            Collection = context.Statements;
        }
    }
}