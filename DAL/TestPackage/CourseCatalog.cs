﻿using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain.TestPackage;
using CENT.BL.Domain.UserPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.DAL.TestPackage
{
    public class CourseCatalog : Catalog<Course>
    {
        public CourseCatalog(StemtestDbContext context)
        {
            Collection = context.Courses;
        }

        public Task<int> GetCourseCountBySchool(School school)
        {
            return Collection.CountAsync(course =>
                course.Owner.AdminSchool == school || ((Teacher) course.Owner).School == school);
        }

        public IQueryable<Course> GetCoursesOrderedByDate(int limitAmount, string userId)
        {
            return Collection.Where(c => c.Owner.Id == userId).OrderByDescending(c => c.CreatedDate).Take(limitAmount);
        }
    }
}