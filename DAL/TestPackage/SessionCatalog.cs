﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain.TestPackage;
using CENT.BL.Domain.UserPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.DAL.TestPackage
{
    public class SessionCatalog : Catalog<Session>
    {
        public SessionCatalog(StemtestDbContext context)
        {
            Collection = context.Sessions;
        }

        public Session GetSessionByPin(string pin)
        {
            var result = Collection.FirstOrDefault(s => s.Pin == pin);
            if (result == null)
                return new Session
                {
                    Id = ""
                };

            return result;
        }


        public async Task<Session> GetSessionsByPin(string pin)
        {
            await foreach (var session in Collection)
                if (session.Pin == pin)
                    return session;

            return null;
        }


        public Task<List<Session>> GetAllSessionsOfTeacher(User user, Course course,
            List<SessionStatus> sessionStatuses)
        {
            if (course == null)
                return Collection.Where(session => session.Owner == user && sessionStatuses.Contains(session.Status))
                    .ToListAsync();
            return Collection.Where(session =>
                    session.Owner == user && session.Course == course && sessionStatuses.Contains(session.Status))
                .ToListAsync();
        }

        public Task<int> GetSessionCountBySchool(School school)
        {
            return Collection.CountAsync(session =>
                session.Owner.AdminSchool == school || ((Teacher) session.Owner).School == school);
        }

        public IQueryable<Session> GetSessionsOfUserByCreateDate(string teacherId, int limitAmount)
        {
            return Collection.Where(session => session.Owner.Id == teacherId).OrderByDescending(s => s.CreatedDate)
                .Take(limitAmount);
        }

        public Task<Session> GetSessionByVoteTestId(string testId)
        {
            return Collection
                .Where(session => session.Test.Id == testId && session.Test.IsVotingPoll &&
                                  session.Test.VoteTestEnabled &&
                                  session.Status != SessionStatus.CLOSED).FirstOrDefaultAsync();
        }
    }
}