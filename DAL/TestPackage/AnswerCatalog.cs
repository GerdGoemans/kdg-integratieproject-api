using CENT.BL.Domain.TestPackage;

namespace CENT.DAL.TestPackage
{
    public class AnswerCatalog : Catalog<Answer>
    {
        public AnswerCatalog(StemtestDbContext context)
        {
            Collection = context.Answers;
        }
    }
}