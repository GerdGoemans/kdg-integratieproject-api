﻿using System.Linq;
using CENT.BL.Domain.TestPackage;
using CENT.BL.Domain.UsageDataPackage;

namespace CENT.DAL.TestPackage
{
    public class QuestionCatalog : Catalog<Question>
    {
        public QuestionCatalog(StemtestDbContext context)
        {
            Collection = context.Questions;
        }
    }
}