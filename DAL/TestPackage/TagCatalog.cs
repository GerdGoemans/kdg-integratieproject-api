﻿using System.Threading.Tasks;
using CENT.BL.Domain.TestPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.DAL.TestPackage
{
    public class TagCatalog : Catalog<Tag>
    {
        public TagCatalog(StemtestDbContext context)
        {
            Collection = context.Tags;
        }

        /**
         * Fetches a tag from the database by name.
         */
        public Task<Tag> GetTagByName(string name)
        {
            return Collection.FirstOrDefaultAsync(t => t.Name == name);
        }
    }
}