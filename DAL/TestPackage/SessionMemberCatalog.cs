using System.Threading.Tasks;
using CENT.BL.Domain.TestPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.DAL.TestPackage
{
    public class SessionMemberCatalog : Catalog<SessionMember>
    {
        public SessionMemberCatalog(StemtestDbContext context)
        {
            Collection = context.SessionMembers;
        }

        public Task<int> GetConnectedSessionMembersForSession(string sessionId)
        {
            return Collection.CountAsync(sm => sm.Session.Id == sessionId);
        }
    }
}