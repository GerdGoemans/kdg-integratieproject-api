﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain.TestPackage;
using CENT.BL.Domain.UsageDataPackage;
using CENT.BL.Domain.UserPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.DAL.TestPackage
{
    public class TestCatalog : Catalog<Test>
    {
        public TestCatalog(StemtestDbContext context)
        {
            Collection = context.Tests;
        }
        
        public IQueryable<TestResults> GetTestsUsageData()
        {
            return Collection.GroupBy(test => test.Questions.Count).Select(tests => new TestResults
            {
                QuestionCount = tests.Key,
                NumberOfTests = tests.Count()
            }).OrderByDescending(results => results.NumberOfTests);
        }

        public async Task<List<Test>> GetTestsByTagId(string tagId)
        {
            var testsIds = await Collection
                .SelectMany(test => test.TestTags)
                .Where(tt => tt.Tag.Id == tagId)
                .Select(tt => tt.Test.Id)
                .ToListAsync();

            return await Collection
                .Where(test => testsIds.Contains(test.Id))
                .ToListAsync();
        }

        public async Task<List<Test>> GetPublicTests()
        {
            return await Collection.Where(test => test.Visibility != Visibility.PRIVATE).ToListAsync();
        }

        public async Task<List<Test>> GetPopularTests()
        {
            return await Collection.Where(test => test.Visibility != Visibility.PRIVATE && test.AmountUsed != 0)
                .OrderByDescending(t => t.AmountUsed).Take(8).ToListAsync();
        }

        public async Task<List<Test>> GetActiveVoteTests()
        {
            return await Collection.Where(test => test.IsVotingPoll && test.VoteTestEnabled).ToListAsync();
        }

        public Task<int> GetTestCountBySchool(School school)
        {
            return Collection.CountAsync(test =>
                test.Owner.AdminSchool == school || ((Teacher) test.Owner).School == school);
        }
    }
}