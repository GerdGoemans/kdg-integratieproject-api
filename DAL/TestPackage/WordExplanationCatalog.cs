﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain.TestPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.DAL.TestPackage
{
    public class WordExplanationCatalog : Catalog<WordExplanation>
    {
        public WordExplanationCatalog(StemtestDbContext context)
        {
            Collection = context.WordExplanations;
        }

        public Task<WordExplanation> GetByWordForQuestion(string word, string questionId)
        {
            return Collection
                .FirstOrDefaultAsync(w => w.Word.ToLower() == word.ToLower() && w.Question.Id == questionId);
        }

        public Task<WordExplanation> GetByWordForTest(string word, string testId)
        {
            return Collection
                .FirstOrDefaultAsync(w => w.Word.ToLower() == word.ToLower() && w.Test.Id == testId);
        }

        public Task<List<WordExplanation>> GetWordExplanationsByTestId(string testId)
        {
            return Collection.Where(w => w.Test.Id == testId).ToListAsync();
        }
    }
}