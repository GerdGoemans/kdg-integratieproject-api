using CENT.BL.Domain.TestPackage;

namespace CENT.DAL.TestPackage
{
    public class QuestionNodeCatalog : Catalog<QuestionNode>
    {
        public QuestionNodeCatalog(StemtestDbContext context)
        {
            Collection = context.QuestionNodes;
        }
    }
}