﻿using System.Threading.Tasks;
using CENT.BL.Domain.TestPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.DAL.TestPackage
{
    public class TestSettingsCatalog : Catalog<TestSetting>
    {
        public TestSettingsCatalog(StemtestDbContext context)
        {
            Collection = context.TestSettings;
        }
    }
}