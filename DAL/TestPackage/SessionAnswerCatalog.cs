using CENT.BL.Domain.TestPackage;

namespace CENT.DAL.TestPackage
{
    public class SessionAnswerCatalog : Catalog<SessionAnswer>
    {
        public SessionAnswerCatalog(StemtestDbContext context)
        {
            Collection = context.SessionAnswers;
        }
    }
}