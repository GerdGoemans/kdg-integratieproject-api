using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CENT.BL.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CENT.DAL
{
    /**
     * Generic catalog with a number of default CRUD methods
     */
    public class Catalog<T> : ICatalog<T> where T : class, IDable<string>
    {
        protected DbSet<T> Collection { set; get; }

        public Task<T> GetById(string id)
        {
            return Collection.FirstOrDefaultAsync(o => o.Id == id);
        }

        public Task<List<T>> GetAll()
        {
            return Collection.ToListAsync();
        }

        public async Task<T> Add(T t)
        {
            t.Id = Guid.NewGuid().ToString();
            var result = await Collection.AddAsync(t);
            return result.Entity;
        }

        public async Task<List<T>> AddAll(T[] ts)
        {
            var entities = new List<T>();
            foreach (var t in ts) entities.Add(await Add(t));

            return entities;
        }

        public bool Delete(T t)
        {
            return Collection.Remove(t) != null;
        }

        public Task<List<T>> GetAll(Expression<Func<T, bool>> predicate)
        {
            return Collection.Where(predicate).ToListAsync();
        }

        public Task<T> Find(Expression<Func<T, bool>> predicate)
        {
            return Collection.Where(predicate).FirstOrDefaultAsync();
        }
    }
}