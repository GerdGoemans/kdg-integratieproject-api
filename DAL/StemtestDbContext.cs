using System;
using System.Threading;
using System.Threading.Tasks;
using CENT.BL.Domain.Interfaces;
using CENT.BL.Domain.OrganisationPackage;
using CENT.BL.Domain.PartyPackage;
using CENT.BL.Domain.TestPackage;
using CENT.BL.Domain.UsageDataPackage;
using CENT.BL.Domain.UserPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.DAL
{
    public sealed class StemtestDbContext : DbContext
    {
        public StemtestDbContext(DbContextOptions<StemtestDbContext> options) :
            base(options)
        {
            Database.EnsureCreated();
        }

        //Domain/userPackage
        public DbSet<User> Users { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<School> Schools { get; set; }

        //Domain/PartyPackage
        public DbSet<Party> Parties { get; set; }
        public DbSet<TestParty> TestParties { get; set; }

        //Domain/OrganisationPackage
        public DbSet<FAQ> Faqs { get; set; }

        //Domain/TestPackage
        public DbSet<Course> Courses { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<SessionMember> SessionMembers { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<TestSetting> TestSettings { get; set; }
        public DbSet<WordExplanation> WordExplanations { get; set; }
        public DbSet<PageVisit> PageVisits { get; set; }
        public DbSet<Statement> Statements { get; set; }
        public DbSet<SessionAnswer> SessionAnswers { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<QuestionNode> QuestionNodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // base on model creating
            base.OnModelCreating(modelBuilder);

            // Unique constraint on user e-mail
            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();

            // -- question -> answer -------
            modelBuilder.Entity<Question>()
                .HasMany(q => q.Answers)
                .WithOne(a => a.Question)
                .OnDelete(DeleteBehavior.Cascade);

            // -- answer -> statement ------
            modelBuilder.Entity<Answer>()
                .HasMany(a => a.Statements)
                .WithOne(s => s.Answer)
                .OnDelete(DeleteBehavior.Cascade);

            // -- session member -----------
            modelBuilder.Entity<SessionMember>().HasKey(member => member.Opaque);

            // -- session -> questionNode --
            modelBuilder.Entity<QuestionNode>()
                .HasOne(qn => qn.Next)
                .WithMany()
                .HasPrincipalKey(qn => qn.Id)
                .HasForeignKey(qn => qn.NextId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(false);


            // -- party <->  test ----------
            modelBuilder.Entity<TestParty>().HasKey(tp => new {tp.PartyId, tp.TestId});
            modelBuilder.Entity<TestParty>()
                .HasOne(tp => tp.Party)
                .WithMany(p => p.TestParties)
                .HasForeignKey(tp => tp.PartyId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<TestParty>()
                .HasOne(tp => tp.Test)
                .WithMany(pt => pt.TestParties)
                .HasForeignKey(tp => tp.TestId)
                .OnDelete(DeleteBehavior.Restrict);

            // -- test <-> tag -------------
            modelBuilder.Entity<TestTag>().HasKey(tt => new
            {
                tt.TagId, tt.TestId
            });

            modelBuilder.Entity<TestTag>()
                .HasOne(tt => tt.Tag)
                .WithMany(t => t.TestTags)
                .HasForeignKey(tt => tt.TagId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<TestTag>()
                .HasOne(tt => tt.Test)
                .WithMany(t => t.TestTags)
                .HasForeignKey(tt => tt.TestId)
                .OnDelete(DeleteBehavior.Restrict);

            // -- test <-> course ------------
            modelBuilder.Entity<TestCourse>().HasKey(tc => new
            {
                tc.CourseId, tc.TestId
            });

            modelBuilder.Entity<TestCourse>()
                .HasOne(tc => tc.Course)
                .WithMany(c => c.TestCourses)
                .HasForeignKey(tc => tc.CourseId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<TestCourse>()
                .HasOne(tc => tc.Test)
                .WithMany(t => t.TestCourses)
                .HasForeignKey(tc => tc.TestId)
                .OnDelete(DeleteBehavior.Restrict);

            // -- test <-> user --------
            modelBuilder.Entity<UserTest>().HasKey(tc => new
            {
                tc.UserId, tc.TestId
            });

            modelBuilder.Entity<UserTest>()
                .HasOne(u => u.User)
                .WithMany(u => u.FavouriteTests)
                .HasForeignKey(u => u.UserId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<UserTest>()
                .HasOne(t => t.Test)
                .WithMany(t => t.UserFavourites)
                .HasForeignKey(tc => tc.TestId)
                .OnDelete(DeleteBehavior.Restrict);

            // -- school <-> admin/user ----
            modelBuilder.Entity<School>()
                .HasMany(s => s.Teachers)
                .WithOne(t => t.School)
                .HasForeignKey(t => t.SchoolId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<School>()
                .HasOne(s => s.Admin)
                .WithOne(u => u.AdminSchool)
                .HasForeignKey<User>(u => u.AdminSchoolId)
                .OnDelete(DeleteBehavior.Restrict);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            var now = DateTime.Now;
            foreach (var changedEntity in ChangeTracker.Entries())
                if (changedEntity.Entity is EntityBase entity)
                    switch (changedEntity.State)
                    {
                        case EntityState.Added:
                            entity.CreatedDate = now;
                            entity.UpdateDate = now;
                            break;
                        case EntityState.Modified:
                            Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                            entity.UpdateDate = now;
                            break;
                    }

            return base.SaveChangesAsync(cancellationToken);
        }
    }
}