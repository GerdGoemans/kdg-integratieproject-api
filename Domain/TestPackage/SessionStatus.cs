﻿namespace CENT.BL.Domain.TestPackage
{
    public enum SessionStatus
    {
        PREPARED = 1,
        OPEN,
        ACTIVE,
        CLOSED
    }
}