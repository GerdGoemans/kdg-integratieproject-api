﻿namespace CENT.BL.Domain.TestPackage
{
    public class TestTag
    {
        public string TagId { get; set; }
        public virtual Tag Tag { get; set; }
        public string TestId { get; set; }
        public virtual Test Test { get; set; }
    }
}