﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CENT.BL.Domain.Interfaces;
using CENT.BL.Domain.UserPackage;

namespace CENT.BL.Domain.TestPackage
{
    public class Session : EntityBase
    {
        public string Pin { get; set; }
        [Required] public string Name { get; set; }
        [Required] public virtual SessionStatus Status { get; set; }
        public GameType Type { get; set; }

        // -- relations --------------------
        [Required] public virtual User Owner { get; set; }
        [Required] public virtual Test Test { get; set; }
        public virtual TestSetting Setting { get; set; }
        public virtual ICollection<QuestionNode> Questions { get; set; } = new List<QuestionNode>();
        public virtual QuestionNode CurrentQuestion { get; set; }
        public virtual Course Course { get; set; }

        // -- helpers ----------------------
        public QuestionNode CreateNextNode(Question question)
        {
            var last = Questions
                .OrderBy(q => q.Index)
                .LastOrDefault();
            QuestionNode node = null;

            if (last == null)
            {
                node = new QuestionNode
                {
                    Index = 0,
                    Value = question
                };
            }
            else
            {
                // add new node
                node = new QuestionNode
                {
                    Index = last.Index + 1,
                    Value = question
                };

                // update previous node
                last.Next = node;
                last.NextId = node.Id;
            }

            // return node
            return node;
        }

        /**
         * Retrieves the next question node
         */
        public QuestionNode GetNextQuestion()
        {
            // get first question from question nodes
            if (CurrentQuestion == null)
            {
                var first = Questions.FirstOrDefault(q => q.Index == 0);
                if (first == null) return null;
                CurrentQuestion = first;
                return first;
            }

            // get next question
            CurrentQuestion = CurrentQuestion.Next;
            return CurrentQuestion;
        }
    }
}