﻿namespace CENT.BL.Domain.TestPackage
{
    public enum GameType
    {
        PARTY_GAME = 1,
        DEBATE_GAME,
        NORMAL_GAME
    }
}