﻿namespace CENT.BL.Domain.TestPackage
{
    public enum Visibility
    {
        PRIVATE = 1,
        SCHOOL,
        GLOBAL
    }
}