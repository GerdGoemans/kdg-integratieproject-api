﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CENT.BL.Domain.Exception;
using CENT.BL.Domain.Interfaces;

namespace CENT.BL.Domain.TestPackage
{
    public class Question : EntityBase
    {
        [Required] [MinLength(2)] public string QuestionString { get; set; }
        public string Video { get; set; }
        [Required] [MinLength(2)] public string LanguageCode { get; set; }
        [Required] public bool AllowSkip { get; set; }
        [Required] public bool AllowGiveArgument { get; set; }
        [Required] public bool ForceGiveArgument { get; set; }

        // -- navigational properties ------
        public virtual ICollection<WordExplanation> WordExplanations { get; set; } = new List<WordExplanation>();
        public virtual ICollection<Answer> Answers { get; set; } = new List<Answer>();
        public virtual Test Test { get; set; }

        // -- helper methods ---------------
        public Answer GetMatchingAnswer(string answer, bool forceThrow = true)
        {
            var match = Answers.FirstOrDefault(a => a.Value.ToLower().Equals(answer.ToLower()));
            if (match == null && forceThrow)
                throw new NotFoundException($"No matching answer was found for question with id {Id}");
            return match;
        }
    }
}