using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.Interfaces;

namespace CENT.BL.Domain.TestPackage
{
    public class Answer : EntityBase
    {
        [Required] public string Value { get; set; }

        /**
         * Value used when answer is part of a multiple choice test.
         */
        public bool? Correct { get; set; }

        /**
         * All the session answers given to a certain answer.
         */
        public virtual ICollection<SessionAnswer> SessionAnswers { get; set; } = new List<SessionAnswer>();

        /**
         * All the statements assigned to a certain answer.
         */
        public virtual ICollection<Statement> Statements { get; set; } = new List<Statement>();

        /**
         * The questions assigned to the answer.
         */
        [Required]
        public virtual Question Question { get; set; }
    }
}