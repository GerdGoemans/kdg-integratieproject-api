namespace CENT.BL.Domain.TestPackage
{
    public class TestCourse
    {
        public string TestId { get; set; }
        public virtual Test Test { get; set; }
        public string CourseId { get; set; }
        public virtual Course Course { get; set; }
    }
}