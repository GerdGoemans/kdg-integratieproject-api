﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CENT.BL.Domain.Interfaces;
using CENT.BL.Domain.PartyPackage;
using CENT.BL.Domain.UserPackage;

namespace CENT.BL.Domain.TestPackage
{
    public class Test : EntityBase
    {
        [Required] public string Title { get; set; }
        [Required] public virtual Visibility Visibility { get; set; }
        [Required] [MinLength(2)] public string LanguageCode { get; set; }
        public virtual TestSetting Setting { get; set; } = new TestSetting();
        [Required] public virtual User Owner { get; set; }
        public bool IsVotingPoll { get; set; }
        public bool IsLocked { get; set; }

        [Required]
        public bool VoteTestEnabled { get; set; } = false; //For vote tests only, determines whether people can play it

        public int AmountUsed { get; set; }

        // -- relations --------------------
        public virtual ICollection<WordExplanation> WordExplanations { get; set; } = new List<WordExplanation>();
        [Required] public virtual ICollection<TestParty> TestParties { get; set; } = new List<TestParty>();
        [Required] public virtual ICollection<Statement> Statements { get; set; } = new List<Statement>();
        [Required] public virtual ICollection<Question> Questions { get; set; } = new List<Question>();
        [Required] public virtual ICollection<TestTag> TestTags { get; set; } = new List<TestTag>();
        [Required] public virtual ICollection<TestCourse> TestCourses { get; set; } = new List<TestCourse>();
        [Required] public virtual ICollection<UserTest> UserFavourites { get; set; } = new List<UserTest>();


        // -- helpers ----------------------
        /**
         * Assign the current test to the course passed as parameter.
         */
        public void AddToCourse(Course course)
        {
            var c = TestCourses.FirstOrDefault(c => c.Course.Id == course.Id);
            if (c != null) return;
            TestCourses.Add(new TestCourse
            {
                Course = course,
                Test = this
            });
        }

        /**
         * Removes the test from the course passed as parameter .
         */
        public void RemoveFromCourse(string courseId)
        {
            var c = TestCourses.FirstOrDefault(c => c.Course.Id == courseId);
            if (c == null) return;
            TestCourses.Remove(c);
        }

        public void AddParty(Party party)
        {
            var match = TestParties.FirstOrDefault(tp => tp.Party.Id == party.Id);
            if (match != null) return;
            TestParties.Add(new TestParty
            {
                Party = party,
                Test = this
            });
        }
    }
}