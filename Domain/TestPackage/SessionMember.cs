﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.Interfaces;

namespace CENT.BL.Domain.TestPackage
{
    public class SessionMember : EntityBase
    {
        [Required] public string Opaque { get; set; }
        public string PartyId { get; set; }
        public int CurrentIndex { get; set; }
        public string ConnectionId { get; set; }

        // -- navigational properties ------
        public virtual ICollection<SessionAnswer> Answers { get; set; } = new List<SessionAnswer>();
        [Required] public virtual Session Session { get; set; }
    }
}