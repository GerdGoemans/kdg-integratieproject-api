using CENT.BL.Domain.Interfaces;

namespace CENT.BL.Domain.TestPackage
{
    public class SessionAnswer : EntityBase
    {
        public string Argument { get; set; }

        // -- navigational properties ------
        public virtual SessionMember SessionMember { get; set; }
        public virtual Answer Answer { get; set; }
    }
}