﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.Interfaces;

namespace CENT.BL.Domain.TestPackage
{
    public class Tag : EntityBase
    {
        [Required] public string Name { get; set; }

        public string Color { get; set; }
        public virtual ICollection<TestTag> TestTags { get; set; } = new List<TestTag>();
    }
}