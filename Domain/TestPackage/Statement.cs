using CENT.BL.Domain.Interfaces;
using CENT.BL.Domain.PartyPackage;

namespace CENT.BL.Domain.TestPackage
{
    public class Statement : EntityBase
    {
        public string Argument { get; set; }
        
        /**
         * The party that has given a certain statement
         * about a question
         */
        public virtual Party Party { get; set; }

        public virtual Answer Answer { get; set; }
        public virtual Test Test { get; set; }
    }
}