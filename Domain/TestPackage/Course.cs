﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.Interfaces;
using CENT.BL.Domain.UserPackage;

namespace CENT.BL.Domain.TestPackage
{
    public class Course : EntityBase
    {
        [Required] public string Name { get; set; }

        [Required] public int AmountOfStudents { get; set; }

        public string SchoolYear { get; set; }

        [Required] public virtual User Owner { get; set; }

        public virtual ICollection<TestCourse> TestCourses { get; set; } = new List<TestCourse>();
    }
}