// ReSharper disable InconsistentNaming

namespace CENT.BL.Domain.TestPackage
{
    public class CommonAnswerValue
    {
        public static readonly string AGREE_VALUE = "AGREE";
        public static readonly string DISAGREE_VALUE = "DISAGREE";
        public static readonly string NO_OPINION_VALUE = "NO_OPINION";
    }
}