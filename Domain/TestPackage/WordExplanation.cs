﻿using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.Interfaces;

namespace CENT.BL.Domain.TestPackage
{
    public class WordExplanation : EntityBase
    {
        [Required] [MinLength(2)] public string Word { get; set; }
        [Required] [MinLength(2)] public string Explanation { get; set; }

        // navigational properties
        public virtual Question Question { get; set; }
        public virtual Test Test { get; set; }
    }
}