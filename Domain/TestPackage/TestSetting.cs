﻿using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.Interfaces;
using CENT.BL.Domain.ValidationAttributes;

namespace CENT.BL.Domain.TestPackage
{
    public class TestSetting : EntityBase
    {
        public TestSetting()
        {
        }

        public TestSetting(TestSetting clone)
        {
            AllowFreeAnswering = clone.AllowFreeAnswering;
            AllowSeeScore = clone.AllowSeeScore;
            AllowViewAnswersWhenFinished = clone.AllowViewAnswersWhenFinished;
            TrueColor = clone.TrueColor;
            FalseColor = clone.FalseColor;
        }

        [Required] public bool AllowFreeAnswering { get; set; }
        [Required] public bool AllowSeeScore { get; set; } = true;
        [Required] public bool AllowViewAnswersWhenFinished { get; set; } = true;
        [Required] [IsHexColorCode] public string TrueColor { get; set; } = "#00FF00";
        [Required] [IsHexColorCode] public string FalseColor { get; set; } = "#FF0000";
    }
}