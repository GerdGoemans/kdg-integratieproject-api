using CENT.BL.Domain.Interfaces;

namespace CENT.BL.Domain.TestPackage
{
    public class QuestionNode : EntityBase
    {
        public int Index { get; set; }
        public string NextId { get; set; }

        // -- relations --------------------
        public virtual QuestionNode Next { get; set; }
        public virtual Question Value { get; set; }
    }
}