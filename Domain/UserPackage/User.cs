﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.Interfaces;
using CENT.BL.Domain.ValidationAttributes;

namespace CENT.BL.Domain.UserPackage
{
    public class User : EntityBase
    {
        public User()
        {
        }

        public User(string email, string password, Role? role)
        {
            Email = email;
            Password = password;
            Role = role;
        }

        [Required]
        [MinLength(2)]
        [IsValidEmail]
        public string Email { get; set; }

        public string Password { get; set; }
        [Required] public Role? Role { get; set; }
        public virtual School AdminSchool { get; set; }
        public string AdminSchoolId { get; set; }
        public virtual ICollection<UserTest> FavouriteTests { get; set; } = new List<UserTest>();

        public string ResetHash { get; set; }
    }
}