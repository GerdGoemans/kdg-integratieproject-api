using System.ComponentModel;

namespace CENT.BL.Domain.UserPackage
{
    public enum Role
    {
        [Description("teacher")] TEACHER = 1,
        [Description("admin")] ADMIN,
        [Description("super_admin")] SUPER_ADMIN
    }
}