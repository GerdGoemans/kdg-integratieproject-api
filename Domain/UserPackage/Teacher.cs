﻿using System.ComponentModel.DataAnnotations;

namespace CENT.BL.Domain.UserPackage
{
    public class Teacher : User
    {
        public Teacher(string email, string password,
            string firstName, string lastName)
            : base(email, password, UserPackage.Role.TEACHER)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public Teacher()
        {
            Role = UserPackage.Role.TEACHER;
        }

        [Required] [MinLength(2)] public string FirstName { get; set; }
        [Required] [MinLength(2)] public string LastName { get; set; }
        [Required] public virtual School School { get; set; }
        public string SchoolId { get; set; }
    }
}