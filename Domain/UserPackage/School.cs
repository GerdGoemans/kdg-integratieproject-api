﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.Interfaces;
using CENT.BL.Domain.ValidationAttributes;

namespace CENT.BL.Domain.UserPackage
{
    public class School : EntityBase
    {
        [Required] [MinLength(2)] public string Name { get; set; }
        [Required] [MinLength(2)] public string Address { get; set; }
        public string Phone { get; set; }

        [Required]
        [MinLength(2)]
        [IsValidEmail]
        public string Email { get; set; }

        [Required] [MinLength(2)] public string Lang { get; set; }
        public virtual ICollection<Teacher> Teachers { get; set; } = new List<Teacher>();
        [Required] public virtual User Admin { get; set; }
        public bool IsBlocked { get; set; } = false;
    }
}