﻿using CENT.BL.Domain.TestPackage;

namespace CENT.BL.Domain.UserPackage
{
    public class UserTest
    {
        public virtual Test Test { get; set; }
        public string TestId { get; set; }

        public virtual User User { get; set; }
        public string UserId { get; set; }
    }
}