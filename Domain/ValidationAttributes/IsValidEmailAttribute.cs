﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace CENT.BL.Domain.ValidationAttributes
{
    /**
     * Custom validation attribute for emails
     */
    [AttributeUsage(AttributeTargets.Property)]
    public class IsValidEmailAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var inputValue = value as string;
            if (string.IsNullOrEmpty(inputValue)) return false;

            //Regex to match this email format: https://tools.ietf.org/html/rfc2822
            var rfc2822EmailRegex =
                @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
            return Regex.IsMatch(inputValue, rfc2822EmailRegex, RegexOptions.IgnoreCase);
        }
    }
}