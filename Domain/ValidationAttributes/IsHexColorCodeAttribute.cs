﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace CENT.BL.Domain.ValidationAttributes
{
    /**
     * Custom validation attribute for validation of hex-colors
     */
    [AttributeUsage(AttributeTargets.Property)]
    public class IsHexColorCodeAttribute : ValidationAttribute
    {
        public bool IsRequired { get; set; }

        public override bool IsValid(object value)
        {
            var inputValue = value as string;
            if (string.IsNullOrEmpty(inputValue))
                return !IsRequired; //In case a property isn't required we don't want to mark it as invallid

            var pattern = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$"; //regex to match valid hex codes

            var match = Regex.Match(inputValue, pattern);
            return match.Success;
        }
    }
}