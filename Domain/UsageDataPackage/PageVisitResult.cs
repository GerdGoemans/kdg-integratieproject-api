﻿using System.ComponentModel.DataAnnotations;

namespace CENT.BL.Domain.UsageDataPackage
{
    public class PageVisitResult
    {
        [Required] public string GroupByKey { get; set; }
        [Required] public int Count { get; set; }
    }
}