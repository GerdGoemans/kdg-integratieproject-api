﻿using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.Interfaces;

namespace CENT.BL.Domain.UsageDataPackage
{
    public class PageVisit : IDable<string>
    {
        [Required] public string FromPage { get; set; }
        [Required] public string ToPage { get; set; }
        [Required] public string IpHash { get; set; } = "";
        [Required] public int Role { get; set; } = 0;
        public string Id { get; set; }
    }
}