﻿using System.ComponentModel.DataAnnotations;

namespace CENT.BL.Domain.UsageDataPackage
{
    public class TestResults
    {
        [Required] public int QuestionCount { get; set; }
        [Required] public int NumberOfTests { get; set; }
    }
}