﻿using System.ComponentModel.DataAnnotations;

namespace CENT.BL.Domain.UsageDataPackage
{
    public class SchoolResult
    {
        [Required] public string SchoolId { get; set; }
        [Required] public string Name { get; set; }
        [Required] public string Address { get; set; }
        [Required] public string Email { get; set; }
        [Required] public int NumberOfCourses { get; set; }
        [Required] public int NumberOfTeachers { get; set; }
        [Required] public int NumberOfSession { get; set; }
        [Required] public int NumberOfTests { get; set; }
    }
}