﻿namespace CENT.BL.Domain.PageParameters
{
    public class TestPageParameters : PageParameters
    {
        public bool OnlyFavourites { get; set; } = false;
        public bool OnlySharedTest { get; set; } = false;
        public bool OnlyVoteTest { get; set; } = false;
    }
}