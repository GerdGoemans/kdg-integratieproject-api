﻿namespace CENT.BL.Domain.PageParameters
{
    public class PageParameters
    {
        private const int MaxPageSize = 6;
        public int Page { get; set; } = 1;
        public string Query { get; set; }
        private int PageSize { get; } = 6;

        public int GetPageSize()
        {
            return PageSize;
        }
    }
}