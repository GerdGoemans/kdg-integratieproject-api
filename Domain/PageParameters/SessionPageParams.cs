﻿namespace CENT.BL.Domain.PageParameters
{
    public class SessionPageParams : PageParameters
    {
        public string ClassId { get; set; }
    }
}