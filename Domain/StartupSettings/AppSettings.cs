using System.Collections.Generic;

namespace CENT.BL.Domain.StartupSettings
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string Namespace { get; set; }
        public IEnumerable<string> Origins { get; set; }
        public bool InitializeData { get; set; }
        public string DatabaseType { get; set; }
        public string ConnectionString { get; set; }

        public string SuperAdminEmail { get; set; }
        public string SuperAdminPassword { get; set; }
        public Dictionary<string, string> SendGrid { get; set; }
        public string RedisConnection { get; set; }
    }
}