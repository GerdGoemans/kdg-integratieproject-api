using CENT.BL.Domain.Interfaces;
using CENT.BL.Domain.TestPackage;

namespace CENT.BL.Domain.PartyPackage
{
    public class TestParty : EntityBase
    {
        public virtual Party Party { get; set; }
        public virtual Test Test { get; set; }

        // needed for EF core
        public string PartyId { get; set; }
        public string TestId { get; set; }
    }
}