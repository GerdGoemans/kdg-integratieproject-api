﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.Interfaces;
using CENT.BL.Domain.TestPackage;
using CENT.BL.Domain.ValidationAttributes;

namespace CENT.BL.Domain.PartyPackage
{
    public class Party : EntityBase
    {
        [Required] [MinLength(2)] public string Name { get; set; }

        [Required] public string Slogan { get; set; }

        [Required]
        [StringLength(7)] //Hex color codes: #000000 => #FFFFFFF 
        [IsHexColorCode]
        public string Color { get; set; }

        public string Logo { get; set; }
        public bool IsArchived { get; set; } = false;

        // -- relations --------------------
        public virtual ICollection<TestParty> TestParties { get; set; } = new List<TestParty>();
        public virtual ICollection<Statement> Statements { get; set; } = new List<Statement>();
    }
}