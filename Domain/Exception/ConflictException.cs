namespace CENT.BL.Domain.Exception
{
    public class ConflictException : HttpException
    {
        public ConflictException(string message) : base(message, 409)
        {
        }
    }
}