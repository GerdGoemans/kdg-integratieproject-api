namespace CENT.BL.Domain.Exception
{
    public class ForbiddenException : HttpException
    {
        public ForbiddenException(string message) : base(message, 403)
        {
        }
    }
}