namespace CENT.BL.Domain.Exception
{
    public class BadRequestException : HttpException
    {
        public BadRequestException(string message) : base(message, 400)
        {
        }
    }
}