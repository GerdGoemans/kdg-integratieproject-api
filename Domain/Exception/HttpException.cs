namespace CENT.BL.Domain.Exception
{
    public class HttpException : System.Exception
    {
        // -- constructor ------------------
        public HttpException(string message) : base(message)
        {
        }

        public HttpException(string message, int status) : base(message)
        {
            Status = status;
        }

        public int Status { get; set; } = 500; // default 500 -> internal server error 
    }
}