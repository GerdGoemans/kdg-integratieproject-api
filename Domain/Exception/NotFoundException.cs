namespace CENT.BL.Domain.Exception
{
    public class NotFoundException : HttpException
    {
        public NotFoundException(string message) : base(message, 404)
        {
        }
    }
}