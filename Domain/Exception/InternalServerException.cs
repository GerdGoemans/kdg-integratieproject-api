namespace CENT.BL.Domain.Exception
{
    public class InternalServerException : HttpException
    {
        public InternalServerException(string message) : base(message, 500)
        {
        }
    }
}