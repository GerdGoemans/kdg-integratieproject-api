using System;

namespace CENT.BL.Domain.Interfaces
{
    public interface IEntityDate
    {
        DateTime CreatedDate { get; set; }
        DateTime UpdateDate { get; set; }
    }
}