namespace CENT.BL.Domain.Interfaces
{
    public interface IDable<T>
    {
        public T Id { get; set; }
    }
}