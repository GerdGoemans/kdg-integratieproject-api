using System;
using System.ComponentModel.DataAnnotations;

namespace CENT.BL.Domain.Interfaces
{
    public class EntityBase : IDable<string>, IEntityDate
    {
        [MaxLength(85)] public string Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}