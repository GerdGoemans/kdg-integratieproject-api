﻿using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.Interfaces;

namespace CENT.BL.Domain.OrganisationPackage
{
    public class FAQ : EntityBase
    {
        [Required] [MinLength(5)] public string Question { get; set; }
        [Required] [MinLength(5)] public string Answer { get; set; }
        [Required] [MinLength(2)] public string LanguageCode { get; set; }
    }
}