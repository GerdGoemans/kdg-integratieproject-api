﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.Core.Internal;
using CENT.BL.Domain;
using CENT.BL.Domain.Exception;
using CENT.BL.Domain.Extensions;
using CENT.BL.Domain.PageParameters;
using CENT.BL.Domain.PartyPackage;
using CENT.BL.Domain.TestPackage;
using CENT.BL.Domain.UserPackage;
using CENT.DAL;
using CENT.DAL.TestPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.BL
{
    public class SessionService : TransactionalService
    {
        // -- catalogs ---------------------
        private readonly SessionCatalog _sessionCatalog;
        private readonly SessionMemberCatalog _sessionMemberCatalog;
        private readonly SessionAnswerCatalog _sessionAnswerCatalog;
        private readonly AnswerCatalog _answerCatalog;
        private readonly QuestionNodeCatalog _questionNodeCatalog;

        // -- services ---------------------
        private readonly PartyService _partyService;
        private readonly SettingsService _settingsService;
        private readonly UserService _userService;
        private readonly CourseService _courseService;
        private readonly AuthService _authService;
        private readonly TestService _testService;
        private readonly QuestionService _questionService;

        public SessionService(
            SessionCatalog sessionCatalog,
            SessionMemberCatalog sessionMemberCatalog,
            SessionAnswerCatalog sessionAnswerCatalog,
            PartyService partyService,
            SettingsService settingsService,
            UserService userService,
            CourseService courseService,
            AuthService authService,
            TestService testService,
            AnswerCatalog answerCatalog,
            QuestionNodeCatalog questionNodeCatalog,
            QuestionService questionService,
            StemtestDbContext context) : base(context)
        {
            _sessionCatalog = sessionCatalog;
            _sessionMemberCatalog = sessionMemberCatalog;
            _sessionAnswerCatalog = sessionAnswerCatalog;
            _partyService = partyService;
            _settingsService = settingsService;
            _userService = userService;
            _authService = authService;
            _courseService = courseService;
            _testService = testService;
            _answerCatalog = answerCatalog;
            _questionNodeCatalog = questionNodeCatalog;
            _questionService = questionService;
        }

        /**
         * Generates a new unique pin to be used by a session.
         */
        private string GenerateSessionPin()
        {
            Random wk = new Random();
            string pin;
            do
            {
                pin = wk.Next(100000, 999999).ToString();
            } while (_sessionCatalog.GetSessionByPin(pin).Id != "");

            return pin;
        }

        public async Task<Session> GetSessionsByPin(string pin)
        {
            var s = await _sessionCatalog.GetSessionsByPin(pin);
            if (s == null) throw new NotFoundException("No session found with pin " + pin);
            return s;
        }

        public async Task<SessionMember> GetSessionMemberByOpaque(string opaque)
        {
            var sm = await _sessionMemberCatalog.Find(o => o.Opaque == opaque);
            if (sm == null) throw new NotFoundException("No session member found with opaque " + opaque);
            return sm;
        }

        public async Task<SessionMember> AddMemberToSession(SessionMember sessionMember)
        {
            SessionMember sm = await _sessionMemberCatalog.Add(sessionMember);
            await Commit();
            return sm;
        }

        public async Task UpdateSessionMemberPartyId(SessionMember sessionMember)
        {
            var entity = await GetSessionMemberByOpaque(sessionMember.Opaque);
            entity.PartyId = sessionMember.PartyId;
            await Commit();
        }

        /**
         * Create a session from a given test as well as generate a pin
         * for the session and set the status of the session to prepared.
         */
        public async Task<Session> CreateSession(string testId,
            string invokerId,
            List<string> questions,
            GameType type,
            bool? randomized,
            bool? allowFreeAnswering,
            bool? allowSeeScore,
            bool? allowViewAnswersWhenFinished,
            string trueColor,
            string falseColor,
            string courseId = "")
        {
            Test test = await _testService.GetTestById(testId, invokerId);
            return await CreateSession(test, invokerId, questions, type, randomized, allowFreeAnswering, allowSeeScore,
                allowViewAnswersWhenFinished, trueColor, falseColor, courseId);
        }

        /**
         * Create a session from a given test as well as generate a pin
         * for the session and set the status of the session to prepared.
         */
        public async Task<Session> CreateSession(
            Test test,
            string invokerId,
            List<string> questions,
            GameType type,
            bool? randomized,
            bool? allowFreeAnswering,
            bool? allowSeeScore,
            bool? allowViewAnswersWhenFinished,
            string trueColor,
            string falseColor,
            string courseId = ""
        )
        {
            //lock test
            test.IsLocked = true;

            // increase the amount the test has been used
            test.AmountUsed++;

            // check if invoker has access to test (based on visibility)
            var invoker = await _userService.GetUser(invokerId);
            _authService.IsAllowedToViewTest(invoker, test);

            // check if settings are correct
            if (type == GameType.DEBATE_GAME && allowFreeAnswering.HasValue && allowFreeAnswering.Value)
                throw new BadRequestException("unable to create debate game in free answering mode");

            // create new session object
            var session = new Session
            {
                Owner = test.Owner,
                Status = SessionStatus.PREPARED,
                Name = test.Title,
                Type = type,
                Test = test,
            };

            // randomize if needed
            if (randomized.HasValue && randomized.Value)
                questions.Shuffle();

            // gather questions
            foreach (var questionId in questions)
            {
                var question = await _questionService.GetQuestionById(questionId);
                var node = session.CreateNextNode(question);
                await _questionNodeCatalog.Add(node);
                session.Questions.Add(node);
            }

            // add session to course
            if (!string.IsNullOrEmpty(courseId))
            {
                Course course = await _courseService.GetCourseById(courseId, invokerId);
                session.Course = course;
            }

            // clone settings from test and update where needed
            TestSetting setting = new TestSetting(test.Setting);
            setting.AllowFreeAnswering = allowFreeAnswering ?? setting.AllowFreeAnswering;
            setting.AllowSeeScore = allowSeeScore ?? setting.AllowSeeScore;
            setting.AllowViewAnswersWhenFinished = allowViewAnswersWhenFinished ?? setting.AllowViewAnswersWhenFinished;
            setting.TrueColor = trueColor ?? setting.TrueColor;
            setting.FalseColor = falseColor ?? setting.FalseColor;


            //create session setting for session based of test setting
            session.Setting = await _settingsService.CreateTestSettings(setting);

            //add session
            return await _sessionCatalog.Add(session);
        }

        /**
         * Select which party a given sessionMember has chosen to play as in a party game session
         */
        public async Task<SessionMember> SelectParty(string opaque, string partyId)
        {
            //Although we could just put the partyId straight in the sessionmember we're fetching the party anyway to check that the partyId is a valid one
            Party party = await _partyService.GetParty(partyId);

            SessionMember sessionMember = await GetSessionMemberByOpaque(opaque);
            sessionMember.PartyId = party.Id;

            await UpdateSessionMemberPartyId(sessionMember);
            return sessionMember;
        }

        /**
         * Change session status
         */
        public async Task<Session> ChangeSessionStatus(string id, SessionStatus status)
        {
            var session = await GetSessionById(id);
            return await ChangeSessionStatus(session, status);
        }

        /**
         * Change session status
         */
        public async Task<Session> ChangeSessionStatus(Session session, SessionStatus status)
        {
            await _userService.SchoolBlockedCheck(session.Owner);
            session.Status = status;
            if (session.Status == SessionStatus.CLOSED) session.Pin = "";
            else if (session.Status == SessionStatus.OPEN) session.Pin = GenerateSessionPin();
            await Commit();
            return session;
        }

        public Task<int> GetConnectedSessionMembersForSession(string sessionId)
        {
            return _sessionMemberCatalog.GetConnectedSessionMembersForSession(sessionId);
        }

        public async Task<IEnumerable<SessionMember>> GetAllSessionMembersBySession(Session session)
        {
            IEnumerable<SessionMember> sessionMembers =
                await _sessionMemberCatalog.GetAll(sm => sm.Session.Id == session.Id);

            if (sessionMembers == null) throw new NotFoundException("There are no sessionmembers in given session");

            return sessionMembers;
        }

        public async Task<Session> GetSessionById(string id)
        {
            Session s = await _sessionCatalog.GetById(id);
            if (s == null) throw new NotFoundException("Session with id " + id + " was not found");
            await _userService.SchoolBlockedCheck(s.Owner);
            return s;
        }

        /**
         * Get a paged list of all session from a given teacher
         */
        public async Task<PagedList<Session>> GetAllSessionsFromTeacher(string teacherId,
            SessionPageParams pageParameters)
        {
            User user = await _userService.GetUser(teacherId);
            int page = pageParameters.Page;
            int pageSize = pageParameters.GetPageSize();
            Course course = null;
            if (!pageParameters.ClassId.IsNullOrEmpty())
            {
                course = await _courseService.GetCourseById(pageParameters.ClassId, teacherId);
            }

            List<SessionStatus> statuses = new List<SessionStatus>();
            statuses.Add(SessionStatus.PREPARED);
            List<Session> rawSessionQuery = await _sessionCatalog.GetAllSessionsOfTeacher(user, course, statuses);


            List<Session> sessionQuery;
            //filter if a search query is passed from the UI, if not just pass all tests along
            if (string.IsNullOrEmpty(pageParameters.Query))
            {
                sessionQuery = rawSessionQuery;
            }
            else
            {
                sessionQuery = rawSessionQuery.FindAll(s => s.Name.ContainsIgnoreCase(pageParameters.Query));
            }

            var tests = sessionQuery.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            PagedList<Session> sessionPagedList = new PagedList<Session>(tests.ToList(), sessionQuery.Count(),
                pageParameters.Page,
                pageParameters.GetPageSize());

            return sessionPagedList;
        }

        public async Task<IEnumerable<Session>> GetLatestSessions(string teacherId)
        {
            IEnumerable<Session> sessions =
                await (_sessionCatalog.GetSessionsOfUserByCreateDate(teacherId, 8)).ToListAsync();
            if (sessions == null) throw new NotFoundException("No sessions found");
            return sessions;
        }

        /**
         * Registers an answer of a student for a specific session.
         */
        public Task<SessionAnswer> CreateSessionAnswer(
            SessionMember sessionMember,
            Answer answer,
            string argument
        )
        {
            // increase session members index
            sessionMember.CurrentIndex += 1;

            // create answer for session member
            var sa = new SessionAnswer
            {
                Argument = argument,
                SessionMember = sessionMember,
                Answer = answer
            };

            return _sessionAnswerCatalog.Add(sa);
        }

        /**
         * Retrieves a single answer from the database.
         */
        public Task<Answer> GetAnswerById(string answerId)
        {
            return _answerCatalog.GetById(answerId);
        }

        public async Task<IEnumerable<SessionMember>> GetSessionMembersByParty(string partyId)
        {
            return await _sessionMemberCatalog.GetAll(sm => sm.PartyId == partyId);
        }

        public async Task<IEnumerable<SessionAnswer>> GetSessionAnswersBySessionMember(string smId)
        {
            return await _sessionAnswerCatalog.GetAll(sm => sm.SessionMember.Id == smId);
        }

        /**
         * Returns the pin of the session for a given vote test
         * If no session for this test exists yet a new one will be made
         */
        public async Task<string> GetVoteTestSessionPin(Test test)
        {
            var session = await _sessionCatalog.GetSessionByVoteTestId(test.Id);
            if (session == null)
                session = await CreateSession(test, test.Owner.Id,
                    test.Questions.Select(question => question.Id).ToList(), GameType.NORMAL_GAME, true, true, true,
                    true, null, null);

            if (session.Status != SessionStatus.ACTIVE) await ChangeSessionStatus(session, SessionStatus.ACTIVE);
            if (string.IsNullOrEmpty(session.Pin)) session.Pin = GenerateSessionPin();

            return session.Pin;
        }
    }
}