﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain.Exception;
using CENT.BL.Domain.UserPackage;
using CENT.DAL;
using CENT.DAL.UserPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.BL
{
    public class UserService : TransactionalService
    {
        private readonly MailService _mailService;
        private readonly AuthService _authService;
        
        private readonly UserCatalog _userCatalog;
        private readonly SchoolCatalog _schoolCatalog;
        
        private readonly Random _random;
        private const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        public UserService(
            MailService mailService,
            UserCatalog userCatalog,
            AuthService authService,
            SchoolCatalog schoolCatalog,
            StemtestDbContext context
        ) : base(context)
        {
            _mailService = mailService;
            _userCatalog = userCatalog;
            _authService = authService;
            _schoolCatalog = schoolCatalog;
            _random = new Random();
        }

        /**
         * Returns a user from the database with the matching id
         */
        public async Task<User> GetUser(string id)
        {
            var user = await _userCatalog.GetById(id);
            if (user == null) throw new NotFoundException("no user found with id " + id);
            await SchoolBlockedCheck(user);
            return user;
        }

        public async Task<User> GetUserByEmail(string email)
        {
            var user = await _userCatalog.GetByEmail(email);
            if (user == null)
                throw new NotFoundException("No user found with email " + email); // no user found with email
            await SchoolBlockedCheck(user);
            return user;
        }

        /**
         * Updates a user from the database.
         */
        public async Task<User> UpdateUser(
            string id, string invokerId,
            string firstName, string lastName,
            string email, string password, string newPassword)
        {
            User invoker = await GetUser(invokerId);
            User user = await GetUser(id);
            _authService.ForceInvocationCheck(user, invoker);

            // update basic information
            user.Email = email ?? user.Email;

            // hash password if passed
            if (!String.IsNullOrEmpty(password) && !String.IsNullOrEmpty(newPassword))
            {
                if (!BCrypt.Net.BCrypt.Verify(password, user.Password))
                    throw new BadRequestException("Incorrect password");
                string hash = BCrypt.Net.BCrypt.HashPassword(newPassword);
                user.Password = hash;
            }

            if (!(user is Teacher teacher)) return user;

            // update other settings if teacher account
            teacher.FirstName = firstName ?? teacher.FirstName;
            teacher.LastName = lastName ?? teacher.LastName;

            return teacher;
        }

        /*
         * Simplified registration method for simpler user types such as
         * admins and super admins.
         */
        public Task<User> Register(string email, string password, Role role)
        {
            return Register(email, password, role, null, null);
        }

        /**
         * Registers a user account. If role is null account will be of type
         * teacher. BCrypt password hashing is used to mask the password
         */
        public async Task<User> Register(string email, string password,
            Role? role, string firstName, string lastName)
        {
            var hash = BCrypt.Net.BCrypt.HashPassword(password);
            var activeRole = role ?? Role.TEACHER;

            // create user account
            var user = activeRole == Role.TEACHER
                ? new Teacher(email, hash, firstName, lastName)
                : new User(email, hash, role);

            user = await _userCatalog.Add(user);
            await Commit();
            return user;
        }

        /**
         * Sends the user an email to reset their password
         */
        public async Task<bool> RequestNewPassword(string email)
        {
            var user = await GetUserByEmail(email);

            if (user == null) return false;

            user.ResetHash = new string(Enumerable.Repeat(Chars, 32).Select(s => s.ToUpper()[_random.Next(s.Length)])
                .ToArray());
            await _mailService.SendMail(user.Email, null, "Wachtwoord vergeten",
                @"../WebAPI/Emails/forgotPassword.html", new
                {
                    RESETHASH = user.ResetHash
                });
            return true;
        }

        /**
         * Sets the user's password to a new, random, password and sends this to the user
         */
        public async Task<bool> SetNewPassword(string resetKey)
        {
            if (resetKey.Length != 32) return false;
            var user = await _userCatalog.GetByResetKey(resetKey);

            if (user == null) return false;

            var newPassword =
                new string(Enumerable.Repeat(Chars, 12).Select(s => s[_random.Next(s.Length)]).ToArray());
            user.Password = BCrypt.Net.BCrypt.HashPassword(newPassword);
            user.ResetHash = ""; // Prevent double use of keys
            await Commit();

            await _mailService.SendMail(user.Email, null, "Nieuw wachtwoord", @"../WebAPI/Emails/newPassword.html", new
            {
                NEWPASSWORD = newPassword
            });
            return true;
        }

        public async Task<User> CreateAdmin(string email, string schoolName)
        {
            var cyphers = Enumerable.Repeat(Chars, 12).Select(s => s[_random.Next(s.Length)]).ToArray();
            var password = new string(cyphers);

            // register new admin account for school
            var user = await Register(email, password, Role.ADMIN);

            await _mailService.SendMail(email, null, "Admin Account Created", @"../WebAPI/Emails/newAdmin.html", new
            {
                SCHOOL = schoolName,
                EMAIL = email,
                NEWPASSWORD = password
            });

            return user;
        }

        public async Task DeleteTeacher(string adminId, string teacherId)
        {
            var teacher = await GetUser(teacherId) as Teacher;
            if (teacher == null) throw new NotFoundException("Could not find teacher with Id " + teacherId);
            if (teacher.School.Admin.Id != adminId)
                throw new ForbiddenException("You are not allowed to delete this teacher account");
            SchoolBlockedCheck(teacher.School);
            if (_userCatalog.Delete(teacher)) return;
            throw new InternalServerException("Could not delete teacher with id " + teacherId);
        }

        public async Task<Teacher> CreateTeacher(string adminId, string email, string firstName, string lastName)
        {
            var school = await _schoolCatalog.GetSchoolByAdmin(adminId);
            if (school == null) throw new NotFoundException("Could not find a school associated with admin " + adminId);
            SchoolBlockedCheck(school);

            var newPassword =
                new string(Enumerable.Repeat(Chars, 12).Select(s => s[_random.Next(s.Length)]).ToArray());

            var teacher = new Teacher
            {
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                School = school,
                Password = BCrypt.Net.BCrypt.HashPassword(newPassword)
            };
            teacher = await _userCatalog.Add(teacher) as Teacher;

            await _mailService.SendMail(teacher.Email, teacher.FirstName + " " + teacher.LastName,
                "Registratie Stemtest voltooid", @"../WebAPI/Emails/newTeacher.html", new
                {
                    SCHOOL = school.Name,
                    EMAIL = email,
                    NEWPASSWORD = newPassword
                });
            school.Teachers.Add(teacher);
            await Commit();


            return teacher;
        }

        /**
         * Checks if the super admins have blocked the school of a given user
         */
        public async Task<bool> SchoolBlockedCheck(User user)
        {
            if (user.Role == Role.SUPER_ADMIN) return true;
            var school = await _schoolCatalog.GetSchoolByUser(user);
            if (school == null) throw new NotFoundException("Could not find school for user " + user.Id);
            return SchoolBlockedCheck(school);
        }

        /**
         * Checks if the super admins have blocked a given school
         */
        public bool SchoolBlockedCheck(School school)
        {
            if (school.IsBlocked) throw new ForbiddenException("School with Id " + school.Id + " has been blocked.");
            return true;
        }

        public async Task<List<Teacher>> GetLatestTeachers(School school)
        {
            var users = await _userCatalog.GetTeachersOrderedByDate(8, school).ToListAsync();
            var teachers = new List<Teacher>();

            foreach (var user in users)
                if (user is Teacher teacher)
                    teachers.Add(teacher);

            return teachers;
        }

        /**
         * Checks if any super admin is in the system
         * If no super admin is present, a new one is made
         */
        public async Task<bool> SuperAdminCheck(string email, string password)
        {
            if (_userCatalog.CheckIfAnySuperAdminIsPresent()) return false;
            await Register(email, password, Role.SUPER_ADMIN);
            return true;
        }
    }
}