﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain.Exception;
using CENT.BL.Domain.TestPackage;
using CENT.DAL;
using CENT.DAL.TestPackage;

namespace CENT.BL
{
    public class TagService : TransactionalService
    {
        private readonly TagCatalog _tagCatalog;

        public TagService(TagCatalog tagCatalog, StemtestDbContext context) : base(context)
        {
            _tagCatalog = tagCatalog;
        }

        /**
         * Creates a tag.
         */
        public async Task<Tag> CreateTag(string name, string color = "#f1c40f")
        {
            var returnTag = await _tagCatalog.GetTagByName(name); //first check if it exists
            if (returnTag != null) return returnTag; //if it exists, return it

            returnTag = await _tagCatalog.Add(new Tag
            {
                Name = name,
                Color = color
            });

            if (await Commit() < 1)
                throw new InternalServerException("Expected more than 0 entries to be committed but got 0");
            return returnTag;
        }

        /**
         * Updates a given tag
         */
        public async Task<Tag> UpdateTag(string tagId, string name, string color = "#f1c40f")
        {
            var t = await GetTagById(tagId);
            t.Name = name;
            t.Color = color;
            await Commit();
            return t;
        }

        /**
         * Finds a tag by name
         */
        public async Task<Tag> GetTagByName(string name)
        {
            var t = await _tagCatalog.GetTagByName(name);
            if (t == null) throw new NotFoundException("No tag found with name " + name);
            return t;
        }

        /**
         * Finds a tag by id
         */
        public async Task<Tag> GetTagById(string tagId)
        {
            var t = await _tagCatalog.GetById(tagId);
            if (t == null) throw new NotFoundException("No tag found with id " + tagId);
            return t;
        }

        /**
         * Retrieves all tags
         */
        public Task<List<Tag>> GetAllTags()
        {
            return _tagCatalog.GetAll();
        }

        /**
         * Adds a given tag to a given test
         */
        public async Task<bool> AddTagToTest(Test test, string tagId)
        {
            var tag = await GetTagById(tagId);

            //Try to fetch an already existing test tag from test with the matching tag id
            var returnTt = test.TestTags.FirstOrDefault(tt => tt.TagId == tag.Id);
            //if is not null test tag already exists
            if (returnTt != null) throw new ConflictException("Tag with id " + tagId + " already exists");

            var testTag = new TestTag
            {
                Tag = tag,
                Test = test
            };

            test.TestTags.Add(testTag); //add the new test tag to the test and commit
            return await Commit() > 0;
        }

        /**
         * Removes a given tag from a given test
         */
        public async Task<bool> RemoveTagFromTest(Test test, string tagId)
        {
            var tag = await GetTagById(tagId);

            var returnTt = test.TestTags.First(testTag => testTag.Tag.Id == tag.Id);
            //if is null we can't delete it, return false
            if (returnTt == null)
                throw new NotFoundException("Could not remove tag with id " + tagId +
                                            " because test did not contain any tags with this id");

            if (!test.TestTags.Remove(returnTt))
                throw new InternalServerException(
                    "Could not remove test tag from test"); //if we can't remove, throw an error
            if (await Commit() < 0)
                throw new InternalServerException("0 entities committed while more than 0 were expected");
            return true;
        }
    }
}