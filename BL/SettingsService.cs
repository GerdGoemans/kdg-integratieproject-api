﻿using System.Threading.Tasks;
using CENT.BL.Domain.Exception;
using CENT.BL.Domain.TestPackage;
using CENT.DAL;
using CENT.DAL.TestPackage;

namespace CENT.BL
{
    public class SettingsService : TransactionalService
    {
        private readonly TestSettingsCatalog _testSettingsCatalog;

        public SettingsService(TestSettingsCatalog testSettingsCatalog,
            StemtestDbContext context) : base(context)
        {
            _testSettingsCatalog = testSettingsCatalog;
        }

        public Task<TestSetting> CreateTestSettings(TestSetting testSetting)
        {
            return _testSettingsCatalog.Add(testSetting);
        }

        public async Task<string> SetTrueColor(string settingsId, string trueColor)
        {
            var testSetting = await GetSettingsById(settingsId);
            testSetting.TrueColor = trueColor;
            await Commit();
            return testSetting.TrueColor;
        }

        public async Task<string> SetFalseColor(string settingsId, string falseColor)
        {
            var testSetting = await GetSettingsById(settingsId);
            testSetting.FalseColor = falseColor;
            await Commit();
            return testSetting.FalseColor;
        }

        /**
         * Gets, updates and commits testsettings for the given Id
         */
        public async Task<TestSetting> SetTestSettings(
            string settingsId,
            bool? allowFreeAnswering,
            bool? allowSeeScore,
            bool? allowViewAnswersWhenFinished,
            string correctColor,
            string wrongColor)
        {
            var testSetting = await GetSettingsById(settingsId);

            return await SetTestSettings(testSetting, allowFreeAnswering, allowSeeScore, allowViewAnswersWhenFinished,
                correctColor, wrongColor);
        }

        /**
         * Updates and commits given testSettings
         */
        public async Task<TestSetting> SetTestSettings(
            TestSetting setting,
            bool? allowFreeAnswering,
            bool? allowSeeScore,
            bool? allowViewAnswersWhenFinished,
            string correctColor,
            string wrongColor)
        {
            setting.AllowFreeAnswering = allowFreeAnswering ?? setting.AllowFreeAnswering;
            setting.AllowSeeScore = allowSeeScore ?? setting.AllowSeeScore;
            setting.AllowViewAnswersWhenFinished = allowViewAnswersWhenFinished ?? setting.AllowViewAnswersWhenFinished;
            setting.TrueColor = correctColor ?? setting.TrueColor;
            setting.FalseColor = wrongColor ?? setting.FalseColor;
            await Commit();
            return setting;
        }

        public async Task<TestSetting> GetSettingsById(string settingsId)
        {
            var testSetting = await _testSettingsCatalog.GetById(settingsId);
            if (testSetting == null) throw new NotFoundException("Could not find testSettings");
            return testSetting;
        }

        public bool Delete(TestSetting testsettings)
        {
            return _testSettingsCatalog.Delete(testsettings);
        }
    }
}