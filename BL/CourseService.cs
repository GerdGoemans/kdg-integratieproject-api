﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain.Exception;
using CENT.BL.Domain.TestPackage;
using CENT.BL.Domain.UserPackage;
using CENT.DAL;
using CENT.DAL.TestPackage;
using CENT.DAL.UserPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.BL
{
    public class CourseService : TransactionalService
    {
        private readonly CourseCatalog _courseCatalog;
        private readonly UserCatalog _userCatalog;
        private readonly TestCatalog _testCatalog;
        private readonly AuthService _authService;

        public CourseService(CourseCatalog courseCatalog, UserCatalog userCatalog,
            TestCatalog testCatalog, AuthService authService,
            StemtestDbContext context) : base(context)
        {
            _courseCatalog = courseCatalog;
            _userCatalog = userCatalog;
            _testCatalog = testCatalog;
            _authService = authService;
        }

        /**
         * Checks if a given user is allowed to access a given course
         */
        private bool IsAllowedToAccess(string invokerId, Course course)
        {
            return course.Owner.Id == invokerId;
        }

        public async Task<Course> CreateCourse(string name, int amountOfStudents, string schoolYear, string userId)
        {
            User owner = await GetUserById(userId);
            Course course = await _courseCatalog.Add(
                new Course
                {
                    Name = name,
                    AmountOfStudents = amountOfStudents,
                    SchoolYear = schoolYear,
                    Owner = owner
                });
            await Commit();
            return course;
        }

        private async Task<User> GetUserById(string userId)
        {
            User u = await _userCatalog.GetById(userId);
            if (u == null) throw new NotFoundException("User with id " + userId + " was not found");
            return u;
        }

        public async Task<IEnumerable<Course>> GetCoursesByUserId(string userId)
        {
            IEnumerable<Course> courses =
                (await _courseCatalog.GetAll()).Where(c => c.Owner.Id == userId).AsEnumerable();
            if (courses == null) throw new NotFoundException("No courses found with userid \"" + userId + "\"");
            return courses;
        }

        public async Task<IEnumerable<Course>> GetLatestCourses(string userId)
        {
            IEnumerable<Course> courses = await (_courseCatalog.GetCoursesOrderedByDate(8, userId).ToListAsync());
            if (courses == null) throw new NotFoundException("No courses found");
            return courses;
        }

        public async Task<bool> DeleteCourse(string courseId, string invokerId)
        {
            Course course = await GetCourseById(courseId, invokerId);
            bool removed = _courseCatalog.Delete(course);
            await Commit();
            return removed;
        }

        public async Task<Course> UpdateCourse(string courseId, string name, int amountOfStudents, string schoolYear,
            List<string> testIds, string invokerId)
        {
            Course course = await GetCourseById(courseId, invokerId);
            if (!IsAllowedToAccess(invokerId, course))
                throw new ForbiddenException("User with id " + invokerId + " isn't allowed to edit this course");
            course.Name = name;
            course.AmountOfStudents = amountOfStudents;
            course.SchoolYear = schoolYear;

            if (testIds != null)
            {
                course.TestCourses.Clear();
                foreach (string testId in testIds)
                {
                    if (await AddTestToCourse(courseId, testId, invokerId) == null)
                        throw new InternalServerException("Failed to add TEST " + testId + " to course.");
                }
            }

            await Commit();
            return course;
        }

        public async Task<Course> AddTestToCourse(string courseId, string testId, string invokerId)
        {
            Course course = await GetCourseById(courseId, invokerId);
            if (!IsAllowedToAccess(invokerId, course))
                throw new ForbiddenException("User with id " + invokerId + " isn't allowed to edit this course");
            var test = await GetTestById(testId);
            test.AddToCourse(course);
            await Commit();
            return course;
        }

        /**
         * Checks if the invoker has the permission to modify the test and then
         * removes the course with the passed id from the test.
         */
        public async Task RemoveTestFromCourse(string courseId, string testId, string invokerId)
        {
            var test = await GetTestById(testId);
            var invoker = await GetUserById(invokerId);

            // perform authorization check
            _authService.ForceInvocationCheck(test.Owner, invoker);

            // remove test from course
            test.RemoveFromCourse(courseId);
            await Commit();
        }

        private async Task<Test> GetTestById(string testId)
        {
            var test = await _testCatalog.GetById(testId);
            if (test == null) throw new NotFoundException("Couldn't find a test with id " + testId);
            return test;
        }

        public async Task<Course> GetCourseById(string courseId, string invokerId)
        {
            var course = await _courseCatalog.GetById(courseId);
            if (course == null) throw new NotFoundException("No course with id \"" + courseId + "\" found");
            if (IsAllowedToAccess(invokerId, course)) return course;
            throw new ForbiddenException("User with id " + invokerId + " is not allowed to access this course");
        }
    }
}