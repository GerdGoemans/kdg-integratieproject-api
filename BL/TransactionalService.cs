using System;
using System.Threading.Tasks;
using CENT.BL.Domain.Interfaces;
using CENT.DAL;
using Microsoft.EntityFrameworkCore;

namespace CENT.BL
{
    public class TransactionalService : IService, IDisposable
    {
        private readonly DbContext _context;

        public TransactionalService(StemtestDbContext context)
        {
            _context = context;
        }

        public async void Dispose()
        {
            await Commit();
        }

        public Task<int> Commit()
        {
            return _context.SaveChangesAsync();
        }
    }
}