﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CENT.BL.Domain.Exception;
using CENT.BL.Domain.PartyPackage;
using CENT.DAL;
using CENT.DAL.PartyPackage;

namespace CENT.BL
{
    public class PartyService : TransactionalService
    {
        private readonly PartyCatalog _partyCatalog;
        private readonly TestPartyCatalog _testPartyCatalog;

        public PartyService(PartyCatalog partyCatalog,
            TestPartyCatalog testPartyCatalog,
            StemtestDbContext context) : base(context)
        {
            _partyCatalog = partyCatalog;
            _testPartyCatalog = testPartyCatalog;
        }

        public async Task<Party> GetParty(string id, bool forceThrow = true)
        {
            var party = await _partyCatalog.GetById(id);
            if (party == null && forceThrow) throw new NotFoundException("Party with id " + id + " was not found");
            return party;
        }

        /**
         * Fetches a party by name from the database
         */
        public Task<Party> GetPartyByName(string name)
        {
            return _partyCatalog.GetByName(name);
        }

        public Task<List<Party>> GetAllParties(bool includeArchived = false)
        {
            return _partyCatalog.GetAllParties(includeArchived);
        }

        public async Task<Party> CreateParty(string name, string slogan, string color, string logo)
        {
            var p = await _partyCatalog.Add(new Party
            {
                Name = name,
                Slogan = slogan,
                Logo = logo,
                Color = color,
                TestParties = new List<TestParty>()
            });
            await Commit();
            return p;
        }

        /**
         * We don't actually delete parties, we simply mark them as archived
         */
        public async Task DeleteParty(string partyId)
        {
            var party = await GetParty(partyId);
            party.IsArchived = true;
            await Commit();
        }

        public async Task<Party> EditParty(string partyId, string partyName, string partySlogan, string partyColor,
            string partyLogo)
        {
            var party = await GetParty(partyId);
            party.Name = partyName;
            party.Slogan = partySlogan;
            party.Color = partyColor;
            party.Logo = partyLogo;
            await Commit();
            return party;
        }

        /**
         * Deletes a test party from the database.
         */
        public bool DeleteTestParty(TestParty testParty)
        {
            return _testPartyCatalog.Delete(testParty);
        }
    }
}