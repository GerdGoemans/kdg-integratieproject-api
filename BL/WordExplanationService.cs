﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain.TestPackage;
using CENT.DAL;
using CENT.DAL.TestPackage;

namespace CENT.BL
{
    public class WordExplanationService : TransactionalService
    {
        private WordExplanationCatalog _wordExplanationCatalog;

        public WordExplanationService(WordExplanationCatalog wordExplanationCatalog, StemtestDbContext context) :
            base(context)
        {
            _wordExplanationCatalog = wordExplanationCatalog;
        }

        /**
         * Adds a word explanation to a test
         */
        public async Task<Test> AddWordExplanationToTest(Test test, string word, string explanation)
        {
            var expl = await _wordExplanationCatalog.Add(new WordExplanation
            {
                Explanation = explanation,
                Word = word
            });

            test.WordExplanations.Add(expl);
            return test;
        }

        /**
         * Adds a word explanation to a question
         */
        public async Task<Question> AddWordExplanationToQuestion(Question question, string word, string explanation)
        {
            var expl = await _wordExplanationCatalog.Add(new WordExplanation
            {
                Explanation = explanation,
                Word = word
            });

            question.WordExplanations.Add(expl);
            return question;
        }

        /**
         * Removes any word explanations from the test that is not in the passed list
         * and adds the ones that are in the list but aren't in the test to the test
         */
        public async Task<List<WordExplanation>> PurgeWordExplanations(List<WordExplanation> wordExplanations,
            Test test)
        {
            var weList = new List<WordExplanation>();
            var newWords = wordExplanations.Select(w => w.Word).ToList();
            //remove omitted word explanations
            var omitted = test.WordExplanations.Where(w => !newWords.Contains(w.Word)).ToList();
            foreach (var wordExplanation in omitted) _wordExplanationCatalog.Delete(wordExplanation);

            foreach (var we in wordExplanations)
                if (test.WordExplanations.Count(w => w.Word == we.Word) == 0)
                {
                    //create new word explanation
                    var wordExplanation = new WordExplanation
                    {
                        Word = we.Word,
                        Explanation = we.Explanation
                    };
                    wordExplanation = await _wordExplanationCatalog.Add(wordExplanation);
                    weList.Add(wordExplanation);
                }
                else
                {
                    var wordExplanation =
                        await _wordExplanationCatalog.GetByWordForTest(we.Word, test.Id);
                    wordExplanation.Explanation = we.Explanation;
                    weList.Add(wordExplanation);
                }

            return weList;
        }

        public async Task<Test> AddWordExplanationsToTest(IEnumerable<WordExplanation> explanations, Test test)
        {
            foreach (var wordExplanation in explanations)
                test.WordExplanations.Add(await _wordExplanationCatalog.Add(new WordExplanation
                {
                    Explanation = wordExplanation.Explanation,
                    Word = wordExplanation.Word
                }));

            return test;
        }

        public Task<List<WordExplanation>> GetWordExplanationsByTestId(string testId)
        {
            return _wordExplanationCatalog.GetWordExplanationsByTestId(testId);
        }
        
        public bool DeleteWordExplanationsFromTest(Test test)
        {
            var wordExplanations = test.WordExplanations;
            foreach (var wordExplanation in wordExplanations)
            {
                var deleted = _wordExplanationCatalog.Delete(wordExplanation);
                if (!deleted) return false;
            }

            return true;
        }
    }
}