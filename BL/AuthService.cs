using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using CENT.BL.Domain.Exception;
using CENT.BL.Domain.Interfaces;
using CENT.BL.Domain.StartupSettings;
using CENT.BL.Domain.TestPackage;
using CENT.BL.Domain.UserPackage;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace CENT.BL
{
    public class AuthService : IService
    {
        private readonly AppSettings _appSettings;

        public AuthService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        /**
         * Authenticate a user and create a JWT token
         * that is valid for 7 days.
         */
        public string Authenticate(User user, string password)
        {
            var valid = BCrypt.Net.BCrypt.Verify(password, user.Password);

            // check if login is valid
            if (!valid) throw new BadRequestException("Incorrect password");

            // describe JWT token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var roleAsString = Enum.GetName(typeof(Role), user.Role!.Value);
            var descriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id),
                    new Claim(ClaimTypes.Role, roleAsString)
                }),
                Issuer = _appSettings.Namespace,
                Audience = _appSettings.Namespace,
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            // create token
            var token = tokenHandler.CreateToken(descriptor);
            return tokenHandler.WriteToken(token);
        }

        /**
         * Checks if a user a subordinate of another user.
         * When the subordinate is a teacher both the admin account of his school and
         * any super admin account is his superior. When the subordinate is an admin
         * then a super admin account can only be his superior.
         */
        public bool IsSubordinate(User subordinate, User superior)
        {
            // super admin are always superior
            if (superior.Role == Role.SUPER_ADMIN) return true;

            // teacher are subordinates to their schools admin
            if (subordinate is Teacher teacher)
            {
                return teacher.School.Admin.Id == superior.Id;
            }

            return false;
        }

        /**
         * Checks if a user is allowed to invoke actions for another user/owner.
         */
        public bool IsAllowedToInvokeFor(User owner, User invoker)
        {
            if (IsSubordinate(owner, invoker)) return true;
            return owner.Id == invoker.Id;
        }

        /**
         * Forces a check to see if the invoker is allowed to invoke for the owner.
         * If the invoker is not allowed the method will throw an ForbiddenException.
         */
        public void ForceInvocationCheck(User owner, User invoker)
        {
            if (!IsAllowedToInvokeFor(owner, invoker))
                throw new ForbiddenException(
                    $"invoker with id '{invoker.Id}' is not allowed to execute actions on behalf of user with id '{owner.Id}'");
        }

        /**
         * Checks whether a certain user is allowed to view a test.
         * When a test is marked as global it is visible to everyone.
         * When a tests visibility is scoped to school level users
         * must be allowed to invoke for each other. Privately scoped
         * tests are only allowed to be accessed by their owner.
         */
        public bool IsAllowedToViewTest(User invoker, Test test)
        {
            switch (test.Visibility)
            {
                case Visibility.GLOBAL:
                    return true;
                case Visibility.SCHOOL:
                    return IsAllowedToInvokeFor(test.Owner, invoker);
                case Visibility.PRIVATE:
                    return invoker.Id.Equals(test.Owner.Id);
                default:
                    throw new InternalServerException("Unrecognized value of visibility found");
            }
        }

        /**
         * Force checks if the invoker is allowed to view a certain test.
         * Throws a forbidden exception when not allowed. 
         */
        public void ForceAllowedViewTestCheck(User invoker, Test test)
        {
            var allowed = IsAllowedToViewTest(invoker, test);
            if (!allowed)
                throw new ForbiddenException($"invoker with id {invoker.Id} is not" +
                                             $" allowed to access test with id {test.Id}");
        }

        /**
         * Checks whether a user account is activated.
         * Only contains logic for admins and teachers.
         * Super admin accounts can not be disabled.
         */
        public bool IsUserActivated(User user)
        {
            if (user.AdminSchool != null) return !user.AdminSchool.IsBlocked;

            if (user is Teacher teacher) return !teacher.School.IsBlocked;

            return true;
        }

        public void ForceCheckUserActivated(User user)
        {
            var allowed = IsUserActivated(user);
            if (!allowed)
                throw new ForbiddenException($"user account with id {user.Id} is not allowed to perform any actions");
        }
    }
}