﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CENT.BL.Domain.UsageDataPackage;
using CENT.BL.Domain.UserPackage;
using CENT.DAL;
using CENT.DAL.TestPackage;
using CENT.DAL.UsageDataPackage;
using CENT.DAL.UserPackage;
using Microsoft.EntityFrameworkCore;

namespace CENT.BL
{
    public class UsageDataService : TransactionalService
    {
        private readonly CourseCatalog _courseCatalog;
        private readonly PageVisitCatalog _pageVisitCatalog;
        private readonly SchoolCatalog _schoolCatalog;
        private readonly SessionCatalog _sessionCatalog;
        private readonly TestCatalog _testCatalog;
        private readonly UserCatalog _userCatalog;

        public UsageDataService(PageVisitCatalog pageVisitCatalog, TestCatalog testCatalog,
            SessionCatalog sessionCatalog, CourseCatalog courseCatalog, UserCatalog userCatalog,
            SchoolCatalog schoolCatalog, StemtestDbContext context) : base(context)
        {
            _pageVisitCatalog = pageVisitCatalog;
            _testCatalog = testCatalog;
            _sessionCatalog = sessionCatalog;
            _userCatalog = userCatalog;
            _courseCatalog = courseCatalog;
            _schoolCatalog = schoolCatalog;
        }

        public async Task PageVisit(string fromPage, string toPage, int role, string ipHash)
        {
            await _pageVisitCatalog.Add(new PageVisit
            {
                FromPage = fromPage,
                ToPage = toPage,
                Role = role,
                IpHash = ipHash
            });
        }

        public Task<List<PageVisitResult>> GetPageVisits()
        {
            return _pageVisitCatalog.GetPageVisits().ToListAsync();
        }

        public Task<List<TestResults>> GetTestsUsageDate()
        {
            return _testCatalog.GetTestsUsageData().ToListAsync();
        }

        public async Task<IEnumerable<SchoolResult>> GetSchoolData()
        {
            IEnumerable<School> schools = await _schoolCatalog.GetAll();
            var results = new List<SchoolResult>();

            foreach (var school in schools)
            {
                var numberOfCourses = await _courseCatalog.GetCourseCountBySchool(school);
                var numberOfSessions = await _sessionCatalog.GetSessionCountBySchool(school);
                var numberOfTeachers = await _userCatalog.GetTeacherCountBySchool(school);
                var numberOfTests = await _testCatalog.GetTestCountBySchool(school);

                results.Add(new SchoolResult
                {
                    SchoolId = school.Id,
                    Name = school.Name,
                    Address = school.Address,
                    Email = school.Email,
                    NumberOfCourses = numberOfCourses,
                    NumberOfSession = numberOfSessions,
                    NumberOfTeachers = numberOfTeachers,
                    NumberOfTests = numberOfTests
                });
            }

            return results;
        }
    }
}