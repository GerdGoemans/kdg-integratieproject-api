using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CENT.BL.Domain.Interfaces;
using CENT.BL.Domain.StartupSettings;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace CENT.BL
{
    public class MailService : ISingleton
    {
        private readonly AppSettings _appSettings;

        public MailService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public string RenderMail(string path, object values)
        {
            using (var reader = new StreamReader(path))
            {
                var output = reader.ReadToEnd();
                foreach (var p in values.GetType().GetProperties())
                    output = output.Replace("[" + p.Name + "]", p.GetValue(values, null) as string ?? string.Empty);
                return output;
            }
        }

        public string StripTags(string html)
        {
            return Regex.Replace(html, "<.*?>", string.Empty);
        }

        public Task SendMail(string receiverMail, string receiverName, string subject, string mailPath, object values)
        {
            if (receiverName == null) receiverName = receiverMail.Split('@')[0];
            var htmlContent = RenderMail(mailPath, values);
            var plainTextContent = StripTags(htmlContent);

            var client = new SendGridClient(_appSettings.SendGrid["apiKey"]);
            var from = new EmailAddress(_appSettings.SendGrid["mail"], _appSettings.SendGrid["name"]);
            var to = new EmailAddress(receiverMail, receiverName);
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            return client.SendEmailAsync(msg);
        }
    }
}