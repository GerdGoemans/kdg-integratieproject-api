﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain.Exception;
using CENT.BL.Domain.TestPackage;
using CENT.DAL;
using CENT.DAL.TestPackage;

namespace CENT.BL
{
    public class QuestionService : TransactionalService
    {
        private readonly WordExplanationService _wordExplanationService;

        private readonly QuestionCatalog _questionCatalog;
        private readonly AnswerCatalog _answerCatalog;
        private readonly WordExplanationCatalog _wordExplanationCatalog;
        private readonly StatementCatalog _statementCatalog;

        public QuestionService(QuestionCatalog questionCatalog, AnswerCatalog answerCatalog,
            WordExplanationCatalog wordExplanationCatalog, StatementCatalog statementCatalog,
            WordExplanationService wordExplanationService,
            StemtestDbContext context) : base(context)
        {
            _questionCatalog = questionCatalog;
            _answerCatalog = answerCatalog;
            _wordExplanationCatalog = wordExplanationCatalog;
            _statementCatalog = statementCatalog;
            _wordExplanationService = wordExplanationService;
        }

        public async Task<Question> GetQuestionById(string questionId)
        {
            Question q = await _questionCatalog.GetById(questionId);
            if (q == null) throw new NotFoundException("No question found that matches given questionId");
            return q;
        }

        /**
         * Updates a single question by id
         */
        public async Task<Question> UpdateQuestion(
            string questionId,
            string questionString,
            string video,
            string languageCode,
            bool allowSkip,
            bool allowGiveArg,
            bool forceGiveArg,
            bool isMultipleChoice,
            List<string> correctAnswers,
            List<string> incorrectAnswers,
            List<WordExplanation> wordExplanations)
        {
            Question question = await GetQuestionById(questionId);

            // check validity of settings
            if (forceGiveArg && !allowGiveArg)
                throw new BadRequestException("Unable to disable argument option when options are forced");

            // create list of all needed answers
            var neededAnswers = new List<string>();

            // add multiple choice answers to needed list
            if (isMultipleChoice)
            {
                neededAnswers.AddRange(correctAnswers);
                neededAnswers.AddRange(incorrectAnswers);
            }
            else
            {
                neededAnswers.AddRange(new[] {CommonAnswerValue.AGREE_VALUE, CommonAnswerValue.DISAGREE_VALUE});
            }

            // add skip answer to needed list
            if (allowSkip)
            {
                neededAnswers.Add(CommonAnswerValue.NO_OPINION_VALUE);
            }

            // remove unneeded answer
            var unneededAnswers = question.Answers.Where(a => !neededAnswers.Contains(a.Value)).ToList();
            foreach (var unneededAnswer in unneededAnswers)
            {
                _answerCatalog.Delete(unneededAnswer);
            }

            // add answers that are not found
            foreach (var neededAnswer in neededAnswers)
            {
                var answer = question.GetMatchingAnswer(neededAnswer, false);

                // update question if multiple choice
                if (answer != null && isMultipleChoice)
                {
                    answer.Correct = neededAnswer != CommonAnswerValue.NO_OPINION_VALUE
                        ? correctAnswers.Contains(neededAnswer)
                        : (bool?) null;
                }

                // skip further actions if answer existed
                if (answer != null) continue;

                await _answerCatalog.Add(new Answer
                {
                    Question = question,
                    Correct = isMultipleChoice && neededAnswer != CommonAnswerValue.NO_OPINION_VALUE
                        ? correctAnswers.Contains(neededAnswer)
                        : (bool?) null,
                    Value = neededAnswer
                });
            }

            // update question settings
            question.QuestionString = questionString;
            question.Video = video;
            question.LanguageCode = languageCode;
            question.AllowSkip = allowSkip;
            question.AllowGiveArgument = allowGiveArg;
            question.ForceGiveArgument = forceGiveArg;

            //handle word explanations
            var weList = new List<WordExplanation>();
            var newWords = wordExplanations.Select(w => w.Word).ToList();
            //remove omitted word explanations
            var omitted = question.WordExplanations.Where(w => !newWords.Contains(w.Word)).ToList();
            foreach (var wordExplanation in omitted)
            {
                _wordExplanationCatalog.Delete(wordExplanation);
            }

            foreach (var we in wordExplanations)
            {
                if (question.WordExplanations.Count(w => w.Word == we.Word) == 0)
                {
                    //create new word explanation
                    WordExplanation wordExplanation = new WordExplanation
                    {
                        Word = we.Word,
                        Explanation = we.Explanation
                    };
                    wordExplanation = await _wordExplanationCatalog.Add(wordExplanation);
                    weList.Add(wordExplanation);
                }
                else
                {
                    WordExplanation wordExplanation =
                        await _wordExplanationCatalog.GetByWordForQuestion(we.Word, questionId);
                    wordExplanation.Explanation = we.Explanation;
                    weList.Add(wordExplanation);
                }
            }

            question.WordExplanations = weList;
            await Commit();
            return question;
        }

        /**
         * Deletes a question from the database,
         * together with all it's child elements.
         */
        public async Task DeleteQuestion(Question question)
        {
            foreach (var questionAnswer in question.Answers)
            {
                foreach (var questionAnswerStatement in questionAnswer.Statements)
                {
                    _statementCatalog.Delete(questionAnswerStatement);
                }

                _answerCatalog.Delete(questionAnswer);
            }

            _questionCatalog.Delete(question);
            await Commit();
        }

        /**
         * Creates a question and generates the needed answers for
         * that specific test.
         */
        public async Task<Question> CreateQuestion(
            Test test,
            string questionString,
            string video,
            string languageCode,
            bool isMultipleChoice,
            bool? allowSkip,
            bool? allowArgument,
            bool? forceArgument,
            List<string> correctAnswers,
            List<string> incorrectAnswers,
            List<WordExplanation> wordExplanations
        )
        {
            var q = new Question
            {
                QuestionString = questionString,
                Video = video,
                LanguageCode = languageCode,
                Test = test,
                AllowSkip = allowSkip ?? false,
                AllowGiveArgument = allowArgument ?? true,
                ForceGiveArgument = forceArgument ?? false
            };


            // check validity of settings
            if (q.ForceGiveArgument && !q.AllowGiveArgument)
                throw new BadRequestException("Unable to disable argument option when options are forced");

            // save entity to database
            await _questionCatalog.Add(q);


            // generate answers
            var answerEntities = new List<Answer>();
            if (!isMultipleChoice)
            {
                answerEntities.Add(new Answer {Question = q, Value = CommonAnswerValue.AGREE_VALUE});
                answerEntities.Add(new Answer {Question = q, Value = CommonAnswerValue.DISAGREE_VALUE});
            }
            else
            {
                if (correctAnswers == null || incorrectAnswers == null)
                    throw new BadRequestException(
                        "a predefined set of answers are required when making a multiple choice question");

                correctAnswers.ForEach(a => answerEntities.Add(new Answer {Question = q, Value = a, Correct = true}));
                incorrectAnswers.ForEach(a =>
                    answerEntities.Add(new Answer {Question = q, Value = a, Correct = false}));
            }

            // add optional skip answer
            if (q.AllowSkip) answerEntities.Add(new Answer {Question = q, Value = CommonAnswerValue.NO_OPINION_VALUE});

            await _answerCatalog.AddAll(answerEntities.ToArray());

            //add word explanations
            if (wordExplanations != null)
                foreach (var exp in wordExplanations)
                    q = await _wordExplanationService.AddWordExplanationToQuestion(q, exp.Word, exp.Explanation);

            // commit
            await Commit();
            return q;
        }

        public bool DeleteQuestionsFromTest(Test test)
        {
            bool deleted;
            var questions = test.Questions;
            foreach (var question in questions)
            {
                //answers
                foreach (var questionAnswer in question.Answers)
                {
                    deleted = _answerCatalog.Delete(questionAnswer);
                    if (!deleted) return false;
                    //statements on answers
                    foreach (var questionAnswerStatement in questionAnswer.Statements)
                    {
                        deleted = _statementCatalog.Delete(questionAnswerStatement);
                        if (!deleted) return false;
                    }
                }

                //delete word definitions on question
                foreach (var questionWordExplanation in question.WordExplanations)
                {
                    deleted = _wordExplanationCatalog.Delete(questionWordExplanation);
                    if (!deleted) return false;
                }

                deleted = _questionCatalog.Delete(question);
                if (!deleted) return false;
            }

            return true;
        }
    }
}