﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain;
using CENT.BL.Domain.Exception;
using CENT.BL.Domain.Extensions;
using CENT.BL.Domain.PageParameters;
using CENT.BL.Domain.PartyPackage;
using CENT.BL.Domain.TestPackage;
using CENT.BL.Domain.UserPackage;
using CENT.DAL;
using CENT.DAL.TestPackage;

namespace CENT.BL
{
    public class TestService : TransactionalService
    {
        // -- services ---------------------
        private readonly SettingsService _settingsService;
        private readonly PartyService _partyService;
        private readonly AuthService _authService;
        private readonly TagService _tagService;
        private readonly QuestionService _questionService;
        private readonly WordExplanationService _wordExplanationService;
        private readonly UserService _userService;

        // -- catalogs ---------------------
        private readonly TestCatalog _testCatalog;
        private readonly StatementCatalog _statementCatalog;

        public TestService(
            TestCatalog testCatalog,
            SettingsService settingsService,
            PartyService partyService,
            AuthService authService,
            StatementCatalog statementCatalog,
            TagService tagService,
            QuestionService questionService,
            WordExplanationService wordExplanationService,
            UserService userService,
            StemtestDbContext context
        ) : base(context)
        {
            _testCatalog = testCatalog;
            _settingsService = settingsService;
            _partyService = partyService;
            _authService = authService;
            _statementCatalog = statementCatalog;
            _tagService = tagService;
            _questionService = questionService;
            _userService = userService;
            _wordExplanationService = wordExplanationService;
        }


        /**
         * Gets a single test by id and checks if the invoker is
         * allowed to view the certain test. 
         */
        public async Task<Test> GetTestById(string id, string invokerId)
        {
            // fetch test
            Test test = await _testCatalog.GetById(id);
            if (test == null) throw new NotFoundException("No test with id \"" + id + "\" found");

            // fetch invoker
            User invoker = await _userService.GetUser(invokerId);

            _authService.ForceAllowedViewTestCheck(invoker, test);
            return test;
        }

        /**
         * Overload of create test method
         * fetching id owner
         */
        public async Task<Test> CreateTest(string title, Visibility visibility,
            string languageCode, string ownerId)
        {
            User owner = await _userService.GetUser(ownerId);
            return await CreateTest(title, visibility, languageCode, owner);
        }

        /**
         * Creates a test with the given properties and commits it
         */
        public async Task<Test> CreateTest(string title, Visibility visibility,
            string languageCode, User owner)
        {
            // check if user is activated
            _authService.ForceCheckUserActivated(owner);

            Test test = new Test
            {
                Title = title,
                Visibility = visibility,
                LanguageCode = languageCode,
                Owner = owner,
                IsVotingPoll = owner.Role == Role.SUPER_ADMIN,
                Setting = new TestSetting(),
                AmountUsed = 0
            };

            await _settingsService.CreateTestSettings(test.Setting);
            Test returnTest = await _testCatalog.Add(test);

            await Commit();
            return returnTest;
        }

        /**
         * Updates a test and performs authorization checks
         */
        public async Task<Test> EditTest(string testId, string title, Visibility? visibility, string lang,
            string invokerId, List<WordExplanation> wordExplanations)
        {
            // fetch test and perform auth check
            Test test = await GetTestById(testId, invokerId);

            test.Title = title ?? test.Title;
            test.Visibility = visibility ?? test.Visibility;
            test.LanguageCode = lang ?? test.LanguageCode;

            //handle word explanations
            var weList = await _wordExplanationService.PurgeWordExplanations(wordExplanations, test);

            test.WordExplanations = weList;

            await Commit();
            return test;
        }

        /**
         * duplicate test
         */
        public async Task<Test> DuplicateTest(string testId, string invokerId)
        {
            //fetch original test
            Test originalTest = await GetTestById(testId, invokerId);

            //get new owner
            User newOwner = await _userService.GetUser(invokerId);
            //create new test
            Test newTest = await CreateTest(originalTest.Title + " (copy)", originalTest.Visibility,
                originalTest.LanguageCode, newOwner);

            //add test settings
            var oSetting = originalTest.Setting;
            await _settingsService.SetTestSettings(
                newTest.Setting,
                oSetting.AllowFreeAnswering,
                oSetting.AllowSeeScore,
                oSetting.AllowViewAnswersWhenFinished,
                oSetting.TrueColor,
                oSetting.FalseColor);

            //add all word explanations
            newTest = await _wordExplanationService.AddWordExplanationsToTest(originalTest.WordExplanations, newTest);

            //add parties to the new test
            List<string> partyIds = originalTest.TestParties.Select(originalTestParty => originalTestParty.PartyId)
                .ToList();
            await AddPartiesToTest(newTest, partyIds);

            //add questions to test
            newTest.Questions = new List<Question>();
            foreach (var q in originalTest.Questions.ToList())
            {
                //check if question was multiple choice
                bool multipleChoice = !q.Answers.Any(a =>
                    a.Value == CommonAnswerValue.AGREE_VALUE || a.Value == CommonAnswerValue.DISAGREE_VALUE);

                //get correct answer list
                var correctAnswer = new List<string>();
                var incorrectAnswers = new List<string>();
                if (multipleChoice)
                {
                    var corAnswers = q.Answers.Where(a => a.Correct == true);
                    correctAnswer.AddRange(corAnswers.Select(answer => answer.Value));
                    var incorAnswers = q.Answers.Where(a => a.Correct == false);
                    incorrectAnswers.AddRange(incorAnswers.Select(answer => answer.Value));
                }

                //create new question
                var newQ = await CreateQuestion(testId, invokerId, q.QuestionString, q.Video,
                    q.LanguageCode,
                    multipleChoice, q.AllowSkip, q.AllowGiveArgument, q.ForceGiveArgument, correctAnswer,
                    incorrectAnswers, q.WordExplanations.Select(w => new WordExplanation
                    {
                        Explanation = w.Explanation,
                        Word = w.Word
                    }).ToList());
                newTest.Questions.Add(newQ);

                //check if originalTest question has statements
                var hasStatement = originalTest.Statements.Any(a => a.Answer.Question.Id == q.Id);

                if (!hasStatement || multipleChoice) continue;
                //add statement per party to question
                foreach (var partyId in partyIds)
                {
                    //get the original statement based on the question and looped partyID
                    var originalStatement =
                        originalTest.Statements.First(s => s.Answer.Question.Id == q.Id && s.Party.Id == partyId);
                    //create party answer for statement based on the original statement for the new question
                    await SelectPartyAnswerForStatement(newQ, partyId, originalStatement.Answer.Value,
                        originalStatement.Argument);
                }
            }

            //add all tags from original test to new test
            foreach (var tt in originalTest.TestTags)
            {
                await _tagService.AddTagToTest(newTest, tt.TagId);
            }

            await Commit();
            return newTest;
        }

        public async Task<Statement> SelectPartyAnswerForStatement(
            string questionId,
            string partyId,
            string answer,
            string argument)
        {
            // fetch question
            Question question = await _questionService.GetQuestionById(questionId);
            if (question == null) throw new NotFoundException("QuestionId does not reference any existing party");
            return await SelectPartyAnswerForStatement(question, partyId, answer, argument);
        }

        /**
         * Creates a statement for a specific party concerning that question
         */
        public async Task<Statement> SelectPartyAnswerForStatement(
            Question question,
            string partyId,
            string answer,
            string argument)
        {
            // fetch party
            var party = await _partyService.GetParty(partyId);
            if (party == null) throw new NotFoundException("PartyId does not reference any existing party");

            // get answer
            Answer selectedAnswer = question.GetMatchingAnswer(answer);

            // create statement
            var statement = new Statement
            {
                Answer = selectedAnswer,
                Party = party,
                Test = question.Test,
                Argument = argument
            };

            return await _statementCatalog.Add(statement);
        }

        /**
         * Adds parties to a test and then returns all the parties added
         * to that specific test
         */
        public async Task<IEnumerable<Party>> AddPartiesToTest(
            string testId,
            string invokerId,
            IEnumerable<string> parties)
        {
            return await AddPartiesToTest(await GetTestById(testId, invokerId), parties);
        }

        /**
         * Adds parties to a test and then returns all the parties added
         * to that specific test
         */
        public async Task<IEnumerable<Party>> AddPartiesToTest(
            Test test,
            IEnumerable<string> parties)
        {
            foreach (var partyId in parties)
            {
                Party party = await _partyService.GetParty(partyId);
                test.AddParty(party);
            }

            return test.TestParties.Select(tp => tp.Party);
        }

        /**
         * Adds parties to a test based on their name and then returns
         * the complete test object with the new parties
         */
        public async Task<Test> AddPartiesToTestByName(Test test, IList<string> parties)
        {
            foreach (var name in parties)
            {
                Party party = await _partyService.GetPartyByName(name);
                if (party == null) continue;
                test.AddParty(party);
            }

            return test;
        }

        /**
         * Marks or unmark a test as favourite for the invoker
         */
        public async Task<Test> MarkTestAsFavourite(string testId, string invokerId, bool isFavourite)
        {
            Test test = await GetTestById(testId, invokerId);
            if (test == null) throw new NotFoundException("No test found with id " + testId);
            User user = await _userService.GetUser(invokerId);
            bool Predicate(UserTest ut) => ut.UserId == invokerId && ut.TestId == testId;

            if (isFavourite)
            {
                if (user.FavouriteTests.Any(Predicate)) return test;
                var userTest = new UserTest()
                {
                    Test = test,
                    TestId = test.Id,
                    User = user,
                    UserId = user.Id
                };
                user.FavouriteTests.Add(userTest);
                test.UserFavourites.Add(userTest);
                if (!user.FavouriteTests.Any(Predicate)) throw new InternalServerException("Failed to favourite test");
            }
            else
            {
                if (!user.FavouriteTests.Any(Predicate)) return test;
                var userFavourites = test.UserFavourites.ToList();
                var favouriteTests = user.FavouriteTests.ToList();
                userFavourites.RemoveAll(Predicate);
                favouriteTests.RemoveAll(Predicate);
                test.UserFavourites = userFavourites;
                user.FavouriteTests = favouriteTests;
                if (user.FavouriteTests.Any(Predicate))
                    throw new InternalServerException("Failed to un-favourite test");
            }

            await Commit();
            return test;
        }

        /**
         * Gets all test for a specific user with pagination.
         */
        public async Task<PagedList<Test>> GetAllTestsFromUser(string userId, TestPageParameters pageParameters)
        {
            int page = pageParameters.Page;
            int pageSize = pageParameters.GetPageSize();

            List<Test> rawTestQuery;

            if (pageParameters.OnlySharedTest)
            {
                rawTestQuery = await _testCatalog.GetPublicTests();
            }
            else
            {
                rawTestQuery = await _testCatalog.GetAll(test => test.Owner.Id == userId);
            }

            if (pageParameters.OnlyFavourites)
            {
                rawTestQuery = rawTestQuery.FindAll(test =>
                    test.UserFavourites.Any(testUserFavourite => testUserFavourite.UserId == userId)
                );
            }

            List<Test> testQuery = new List<Test>();
            if (string.IsNullOrEmpty(pageParameters.Query))
            {
                testQuery = rawTestQuery;
            }
            else
            {
                foreach (Test test in rawTestQuery)
                {
                    if (test.Title.ContainsIgnoreCase(pageParameters.Query))
                    {
                        testQuery.Add(test);
                        continue;
                    }

                    if (test.TestTags.Any(tt => tt.Tag.Name.ContainsIgnoreCase(pageParameters.Query)))
                    {
                        testQuery.Add(test);
                    }
                }
            }

            var tests = testQuery.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            PagedList<Test> testPagedList = new PagedList<Test>(tests.ToList(), testQuery.Count(), pageParameters.Page,
                pageParameters.GetPageSize());
            return testPagedList;
        }


        /**
         * Override for creating a question and generates the needed answers for
         * that specific test.
         */
        public async Task<Question> CreateQuestion(
            string testId,
            string invokerId,
            string questionString,
            string video,
            string languageCode,
            bool isMultipleChoice,
            bool? allowSkip,
            bool? allowArgument,
            bool? forceArgument,
            List<string> correctAnswers,
            List<string> incorrectAnswers,
            List<WordExplanation> wordExplanations
        )
        {
            var test = await GetTestById(testId, invokerId);
            return await _questionService.CreateQuestion(test, questionString, video, languageCode, isMultipleChoice,
                allowSkip, allowArgument, forceArgument, correctAnswers, incorrectAnswers, wordExplanations);
        }

        /**
         * Deletes a test by id.
         */
        public async Task<bool> DeleteTestById(string testId, string invokerId)
        {
            var test = await GetTestById(testId, invokerId);
            var deleted = false;
            // Test settings removal
            var testSettings = test.Setting;
            //delete testSetting
            deleted = _settingsService.Delete(testSettings);
            if (!deleted) return false;

            //owner remove test from favourites if necessary
            var owner = test.Owner;
            var userTest = owner.FavouriteTests.FirstOrDefault(ut => ut.TestId == test.Id);
            if (userTest != null)
            {
                owner.FavouriteTests.Remove(userTest);
            }

            // WordExplanations;
            if (!_wordExplanationService.DeleteWordExplanationsFromTest(test)) return false;

            //Questions;
            if (!_questionService.DeleteQuestionsFromTest(test)) return false;
            
            //Statements;
            var statements = test.Statements;
            foreach (var statement in statements)
            {
                deleted = _statementCatalog.Delete(statement);
            }

            if (!deleted) return false;

            //delete actual test
            deleted = _testCatalog.Delete(test);
            if (!deleted) return false;
            await Commit();
            return true;
        }

        /**
         * Updates settings for a test.
         */
        public async Task<Test> UpdateTestSettings(
            string testId,
            string invokerId,
            bool? allowFreeAnswering,
            bool? allowSeeScore,
            bool? allowViewAnswersWhenFinished)
        {
            Test test = await GetTestById(testId, invokerId);
            if (test == null) throw new NotFoundException("No test found for given testId");
            test.Setting = await _settingsService.SetTestSettings(test.Setting.Id, allowFreeAnswering, allowSeeScore,
                allowViewAnswersWhenFinished, null, null);
            return test;
        }

        public async Task<Test> UpdateTestSettings(
            Test test,
            bool? allowFreeAnswering,
            bool? allowSeeScore,
            bool? allowViewAnswersWhenFinished,
            string correctColor,
            string wrongColor)
        {
            test.Setting = await
                _settingsService.SetTestSettings(
                    test.Setting,
                    allowFreeAnswering,
                    allowSeeScore,
                    allowViewAnswersWhenFinished,
                    correctColor,
                    wrongColor
                );

            return test;
        }

        public async Task UpdatePartiesFromTest(string testId, string userId, List<string> partyIds)
        {
            Test test = await GetTestById(testId, userId);

            // add new parties to test
            foreach (var partyId in partyIds)
            {
                var match = test.TestParties.FirstOrDefault(tp => tp.Party.Id == partyId);
                if (match != null) continue; // continue if party already exists
                var party = await _partyService.GetParty(partyId);
                test.AddParty(party);
            }

            // remove parties and statements from test
            var omitted = test.TestParties.Where(tp => !partyIds.Contains(tp.Party.Id)).ToList();
            foreach (var tp in omitted)
            {
                _partyService.DeleteTestParty(tp);
            }
        }

        /*
         * Updates an existing party statement
         */
        public async Task UpdatePartyStatement(
            string statementId,
            string partyId,
            string questionId,
            string answer,
            string explanation)
        {
            var statement = await _statementCatalog.GetById(statementId);
            if (statement == null)
            {
                await SelectPartyAnswerForStatement(
                    questionId,
                    partyId,
                    answer,
                    explanation
                );

                return;
            }

            var updatedAnswer = statement.Answer.Question.GetMatchingAnswer(answer);

            // update values
            statement.Answer = updatedAnswer;
            statement.Argument = explanation;

            await Commit();
        }

        /**
         * Gets popular tests sorted by usage view
         * allowed to view popular tests
         */
        public async Task<List<Test>> GetPopularTests()
        {
            var tests = await _testCatalog.GetPopularTests();
            return tests;
        }

        /**
         * Opens or closes a voting test
         */
        public async Task<Test> ToggleVotingTest(string testId, string invoker, bool enabled)
        {
            Test test = await GetTestById(testId, invoker);
            if (!test.IsVotingPoll)
                throw new BadRequestException("Test with id " + testId + " is not a vote_test");
            test.VoteTestEnabled = enabled;
            return test;
        }

        /**
         * Gets all the currently active vote tests for a student to play
         */
        public async Task<List<Test>> GetVoteTests()
        {
            return await _testCatalog.GetActiveVoteTests();
        }

        public async Task<Test> GetVoteTestById(string testId)
        {
            Test test = await _testCatalog.GetById(testId);
            if (test == null) throw new NotFoundException("Not test found with id " + testId);
            if (!test.IsVotingPoll) throw new BadRequestException("Requested test isn't a vote test");
            if (!test.VoteTestEnabled) throw new BadRequestException("Requested test is still locked");
            return test;
        }
    }
}