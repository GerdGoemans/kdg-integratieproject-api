using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain.TestPackage;
using CENT.DAL;
using CENT.DAL.TestPackage;
using Microsoft.Extensions.Logging;
using NReco.Csv;

namespace CENT.BL
{
    public class CsvService : TransactionalService
    {
        // -- services ---------------------
        private readonly TestService _testService;
        private readonly PartyService _partyService;
        private readonly QuestionService _questionService;
        private readonly WordExplanationService _wordExplanationService;
        private readonly ILogger<CsvService> _logger;
        
        // -- catalogs ---------------------
        private readonly StatementCatalog _statementCatalog;

        // -- constants --------------------
        private readonly Tuple<int, string>[] _answerMap =
        {
            new Tuple<int, string>(0, CommonAnswerValue.DISAGREE_VALUE),
            new Tuple<int, string>(1, CommonAnswerValue.AGREE_VALUE), 
            new Tuple<int, string>(2, CommonAnswerValue.NO_OPINION_VALUE), 
        };

        // -- constructor ------------------
        public CsvService(
            TestService testService,
            PartyService partyService,
            ILogger<CsvService> logger,
            StatementCatalog statementCatalog,
            WordExplanationService wordExplanationService,
            QuestionService questionService,
            StemtestDbContext context) : base(context)
        {
            _testService = testService;
            _partyService = partyService;
            _logger = logger;
            _statementCatalog = statementCatalog;
            _wordExplanationService = wordExplanationService;
            _questionService = questionService;
        }

        /**
         * Adds statements, read from a csv stream, to a test.
         */
        public async Task<Test> AddStatementsToTest(Test test, Stream stream)
        {
            // declare readers
            using var streamReader = new StreamReader(stream);
            var csvReader = new CsvReader(streamReader, ";");

            // loop specific variables
            bool headerRead = false;
            Dictionary<int, HeaderDetail> headerMap = null;

            // main csv reading loop
            while (csvReader.Read())
            {
                // extract header map
                if (!headerRead)
                {
                    headerMap = ExtractHeaderMap(csvReader, out var parties);
                    test = await _testService.AddPartiesToTestByName(test, parties);
                    headerRead = true;
                    continue;
                }

                // handle question for this row
                var q = await ExtractQuestionRow(csvReader, headerMap, test);

                // handle each party statement
                var statements = await ExtractStatements(csvReader, headerMap, q);
                
                // add statements to test
                foreach (var statement in statements)
                {
                    await _statementCatalog.Add(statement);
                }
                
            }

            return test;
        }

        /**
         * Adds word explanations, read from a csv stream, to a test.
         */
        public async Task AddWordExplanations(Test test, Stream stream)
        {
            // declare readers
            using var streamReader = new StreamReader(stream);
            var csvReader = new CsvReader(streamReader, ";");

            while (csvReader.Read())
            {
                var word = csvReader[0];
                var definition = csvReader[1];

                await _wordExplanationService.AddWordExplanationToTest(test, word, definition);
            }
        }

        /**
         * Export an existing test as CSV stream.
         */
        public Stream ExportTestToCsv(Test test)
        {
            // declare stream variables
            var stream = new MemoryStream();
            using var streamWriter = new StreamWriter(stream, leaveOpen: true);
            var csvWriter = new CsvWriter(streamWriter, ";");

            // -- output header ------------
            csvWriter.WriteField("Stelling");
            csvWriter.WriteField("Verkorte omschrijving");
            
            foreach (var tp in test.TestParties.OrderBy(tp => tp.Party.Name))
            {
                csvWriter.WriteField($"Antwoord {tp.Party.Name}");
                csvWriter.WriteField($"Motivatie {tp.Party.Name}");
            }
            
            csvWriter.NextRecord();

            // -- output test data ---------
            foreach (var question in test.Questions)
            {
                csvWriter.WriteField(question.QuestionString);
                csvWriter.WriteField(""); // empty field -> no short description in our model

                var statementsForQuestion = test.Statements
                    .Where(s => s.Answer.Question.Id == question.Id)
                    .OrderBy(s => s.Party.Name)
                    .ToList();
                
                // 
                statementsForQuestion.ForEach(s =>
                {
                    var matchingAnswer = _answerMap
                        .FirstOrDefault(t => t.Item2 == s.Answer.Value)
                        ?.Item1.ToString();
                    
                    csvWriter.WriteField(matchingAnswer); // answer
                    csvWriter.WriteField(s.Argument); // argument
                });
                
                
                csvWriter.NextRecord();
            }

            return stream;
        }

        /**
         * Export an existing tests word explanations as CSV stream.
         */
        public Stream ExportExplanationsToCsv(Test test)
        {
            // declare stream variables
            var stream = new MemoryStream();
            using var streamWriter = new StreamWriter(stream, leaveOpen: true);
            var csvWriter = new CsvWriter(streamWriter, ";");

            // gather all explanations
            var explanations = new List<WordExplanation>(test.WordExplanations);
            explanations.AddRange(test.Questions.SelectMany(q => q.WordExplanations));
            
            foreach (var explanation in explanations)
            {
                csvWriter.WriteField(explanation.Word);
                csvWriter.WriteField(explanation.Explanation);
                csvWriter.NextRecord();
            }

            return stream;
        }
        
        // =================================
        // PRIVATE HELPERS
        // =================================
        
        /**
         * Extracts all statements concerning the question passed as
         * parameter to the method.
         */
        private async Task<IList<Statement>> ExtractStatements(CsvReader reader, Dictionary<int, HeaderDetail> headerDetails,
            Question question)
        {
            IList<Statement> statements = new List<Statement>();
            for (int i = 0; i < reader.FieldsCount; i++)
            {
                var detail = headerDetails[i];
                
                if (detail.UnMappable) continue;
                if (detail.PropertyOf != typeof(Statement)) continue;
                if (!(detail is StatementHeaderDetail)) continue;

                var statementDetail = detail as StatementHeaderDetail;
                
                // retrieve party from db
                var party = await _partyService.GetPartyByName(statementDetail.PartyName);
                if (party == null)
                {
                    _logger.LogWarning($"no party found with name '{statementDetail.PartyName}', skipping statements");
                    continue;
                }
                
                // find statement for party if it exists
                Statement statement = statements.FirstOrDefault(s => s.Party.Id == party.Id);
                if (statement == null)
                {
                    statement = new Statement {Party = party, Test = question.Test};
                    statements.Add(statement);
                };
                
                // add value to statement
                switch (statementDetail.ColumnUse)
                {
                    case ColumnUse.ANSWER:
                        var stringAnswerValue = _answerMap.FirstOrDefault(t => t.Item1 == int.Parse(reader[i]))?.Item2;
                        var answer = question.GetMatchingAnswer(stringAnswerValue);
                        statement.Answer = answer;
                        break;
                    case ColumnUse.MOTIVATION:
                        statement.Argument = reader[i];
                        break;
                    default:
                        _logger.LogWarning($"unrecognized column use found while adding statements");
                        continue;
                }
            }

            return statements;
        }

        /**
         * Extracts a single question with possible statements out of
         * a single CSV row.
         */
        private async Task<Question> ExtractQuestionRow(CsvReader reader, Dictionary<int, HeaderDetail> headerDetails,
            Test test)
        {
            Question q = new Question
            {
                LanguageCode = "en" // set default language code
            };
            for (int i = 0; i < reader.FieldsCount; i++)
            {
                var detail = headerDetails[i];

                if (detail.UnMappable) continue;
                if (detail.PropertyOf != typeof(Question)) continue;

                // set property of question  
                detail.PropertyOf.GetProperty(detail.PropertyName)
                    ?.SetValue(q, reader[i]);
            }

            return await _questionService.CreateQuestion(test, q.QuestionString, q.Video, q.LanguageCode,
                false, false, true, false, null, null, null);
        }

        /**
         * Method that extracts the header map when reading
         * all the questions with the party statements
         */
        private Dictionary<int, HeaderDetail> ExtractHeaderMap(CsvReader reader, out IList<string> parties)
        {
            parties = new List<string>();
            var dictionary = new Dictionary<int, HeaderDetail>();
            for (int i = 0; i < reader.FieldsCount; i++)
            {
                switch (i)
                {
                    case 0:
                        dictionary.Add(i, new HeaderDetail(typeof(Question), "QuestionString"));
                        break;
                    case 1:
                        dictionary.Add(i, new HeaderDetail(typeof(Question), null, true));
                        break;
                    default:
                        var value = reader[i];
                        var type = value.Split(" ");
                        var name = value.Substring(value.IndexOf(" ", StringComparison.Ordinal) + 1).Trim();
                        if (!parties.Contains(name)) parties.Add(name); 
                        ColumnUse use = type[0].ToLower() == "antwoord" ? ColumnUse.ANSWER : ColumnUse.MOTIVATION;
                        dictionary.Add(i,
                            new StatementHeaderDetail(use, name, typeof(Statement),
                                use == ColumnUse.ANSWER ? "Answer" : "Argument"));
                        break;
                }
            }

            return dictionary;
        }

        /**
         * Class used for describing a column in a CSV file
         */
        private class HeaderDetail
        {
            public Type PropertyOf { get; }
            public bool UnMappable { get; }
            public string PropertyName { get; set; }

            public HeaderDetail(Type propertyOf, string propertyName, bool unMappable = false)
            {
                PropertyOf = propertyOf;
                PropertyName = propertyName;
                UnMappable = unMappable;
            }
        }

        /**
         * Class used for describing a column in a CSV file
         * that is related to a statement.
         */
        private class StatementHeaderDetail : HeaderDetail
        {
            public ColumnUse ColumnUse { get; }
            public string PartyName { get; }

            public StatementHeaderDetail(
                ColumnUse columnUse,
                string partyName,
                Type propertyOf,
                string propertyName,
                bool unMappable = false
            ) : base(propertyOf, propertyName, unMappable)
            {
                ColumnUse = columnUse;
                PartyName = partyName;
            }
        }

        /**
         * Enum that describes the use for that specific column.
         */
        private enum ColumnUse
        {
            ANSWER,
            MOTIVATION
        }
    }
}