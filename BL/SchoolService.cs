﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain;
using CENT.BL.Domain.Exception;
using CENT.BL.Domain.Extensions;
using CENT.BL.Domain.PageParameters;
using CENT.BL.Domain.UserPackage;
using CENT.DAL;
using CENT.DAL.UserPackage;

namespace CENT.BL
{
    public class SchoolService : TransactionalService
    {
        private readonly SchoolCatalog _schoolCatalog;
        private readonly UserService _userService;

        public SchoolService(SchoolCatalog schoolCatalog, UserService userService, StemtestDbContext context) :
            base(context)
        {
            _schoolCatalog = schoolCatalog;
            _userService = userService;
        }

        public async Task<PagedList<School>> GetAllSchools(SchoolPageParams schoolPageParams)
        {
            var page = schoolPageParams.Page;
            var pageSize = schoolPageParams.GetPageSize();

            var rawSchoolQuery = (await _schoolCatalog.GetAll()).ToList();

            List<School> testQuery;
            if (string.IsNullOrEmpty(schoolPageParams.Query))
                testQuery = rawSchoolQuery;
            else
                testQuery = rawSchoolQuery.FindAll(school => school.Name.ContainsIgnoreCase(schoolPageParams.Query));

            var tests = testQuery.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            var schoolPagedList = new PagedList<School>(tests.ToList(), testQuery.Count(),
                schoolPageParams.Page,
                schoolPageParams.GetPageSize());
            return schoolPagedList;
        }

        public async Task<School> GetSchoolByAdminId(string adminId)
        {
            var school = await _schoolCatalog.GetSchoolByAdmin(adminId);
            if (school == null) throw new NotFoundException("No school found for admin with ID " + adminId);
            return school;
        }

        public async Task<School> CreateSchool(string name, string email, string address, string phone, string lang,
            string loginMail, bool createWithDefaultPwd = false)
        {
            var admin = createWithDefaultPwd
                ? await _userService.Register(loginMail, "admin", Role.ADMIN)
                : await _userService.CreateAdmin(loginMail, name);
            var school = new School
            {
                Address = address,
                Email = email,
                Name = name,
                Phone = phone,
                Teachers = new List<Teacher>(),
                Lang = lang,
                Admin = admin
            };
            return await _schoolCatalog.Add(school);
        }

        public async Task<PagedList<Teacher>> GetAllTeachersAdmin(string adminId, PageParameters pageParameters)
        {
            var school = await GetSchoolByAdminId(adminId);
            var page = pageParameters.Page;
            var pageSize = pageParameters.GetPageSize();


            var rawTeachers = school.Teachers.ToList();
            var teachers = new List<Teacher>();

            if (string.IsNullOrEmpty(pageParameters.Query))
                teachers = rawTeachers;
            else
                foreach (var teacher in rawTeachers)
                {
                    if (teacher.FirstName.ContainsIgnoreCase(pageParameters.Query))
                    {
                        teachers.Add(teacher);
                        continue;
                    }

                    if (teacher.LastName.ContainsIgnoreCase(pageParameters.Query)) teachers.Add(teacher);
                }

            var teachersList = teachers.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            var teacherPagedList = new PagedList<Teacher>(teachersList, teachers.Count,
                pageParameters.Page,
                pageParameters.GetPageSize());
            return teacherPagedList;
        }

        public async Task<School> GetSchoolById(string schoolId)
        {
            var school = await _schoolCatalog.GetById(schoolId);
            if (school == null) throw new NotFoundException("No school found with id " + schoolId);
            return school;
        }

        public async Task<School> UpdateSchoolById(string schoolId, string name, string address, string email,
            string phone, string loginMail)
        {
            var school = await GetSchoolById(schoolId);
            var user = await _userService.GetUser(school.Admin.Id);
            user.Email = loginMail;
            school.Name = name;
            school.Address = address;
            school.Email = email;
            school.Phone = phone;
            await Commit();
            return school;
        }

        public async Task<School> BlockSchool(string schoolId, string invokerId, bool blockStatus)
        {
            var superAdmin = await _userService.GetUser(invokerId);
            if (superAdmin.Role != Role.SUPER_ADMIN) throw new ForbiddenException("You are not allowed to do this");
            var school = await GetSchoolById(schoolId);
            school.IsBlocked = blockStatus;
            await Commit();
            return school;
        }

        public async Task<List<Teacher>> GetLatestTeachers(string schoolId)
        {
            var school = await GetSchoolByAdminId(schoolId);
            return await _userService.GetLatestTeachers(school);
        }

        public IEnumerable<School> GetLatestSchools()
        {
            return _schoolCatalog.GetSchoolsOrderedByDate(5).ToList();
        }
    }
}