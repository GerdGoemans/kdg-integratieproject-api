﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL.Domain;
using CENT.BL.Domain.Exception;
using CENT.BL.Domain.Extensions;
using CENT.BL.Domain.OrganisationPackage;
using CENT.BL.Domain.PageParameters;
using CENT.DAL;
using CENT.DAL.OrganisationPackage;

namespace CENT.BL
{
    public class FaqService : TransactionalService
    {
        private readonly FaqCatalog _faqCatalog;

        public FaqService(FaqCatalog faqCatalog,
            StemtestDbContext context) : base(context)
        {
            _faqCatalog = faqCatalog;
        }

        public async Task<IEnumerable<FAQ>> GetAllFaqEntries()
        {
            return await _faqCatalog.GetAll();
        }

        public async Task<PagedList<FAQ>> GetAllFaqEntries(FaqPageParameters faqPageParameters)
        {
            var page = faqPageParameters.Page;
            var pageSize = faqPageParameters.GetPageSize();

            var faqRawQuery = (await GetAllFaqEntries()).ToList();

            List<FAQ> faqQuery;
            if (string.IsNullOrEmpty(faqPageParameters.Query))
                faqQuery = faqRawQuery;
            else
                faqQuery = faqRawQuery.FindAll(faq => faq.Question.ContainsIgnoreCase(faqPageParameters.Query));

            var faqs = faqQuery.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            var faqPagedList = new PagedList<FAQ>(faqs.ToList(), faqQuery.Count(), faqPageParameters.Page,
                faqPageParameters.GetPageSize());
            return faqPagedList;
        }

        public async Task<FAQ> AddFaq(string question, string answer, string languageCode)
        {
            var faq = new FAQ
            {
                Question = question,
                Answer = answer,
                LanguageCode = languageCode
            };
            faq = await _faqCatalog.Add(faq);
            await Commit();
            return faq;
        }

        public async Task<FAQ> UpdateFaq(string faqId, string question, string answer, string languageCode)
        {
            var faq = await GetFaqById(faqId);
            faq.Question = question;
            faq.Answer = answer;
            faq.LanguageCode = languageCode;
            await Commit();
            return faq;
        }

        private async Task<FAQ> GetFaqById(string id)
        {
            var faq = await _faqCatalog.GetById(id);
            if (faq == null) throw new NotFoundException("No faq found for id " + id);
            return faq;
        }

        public async Task DeleteFaq(string faqId)
        {
            if (!_faqCatalog.Delete(await GetFaqById(faqId)))
                throw new InternalServerException("Could not delete faq with ID " + faqId);
        }
    }
}