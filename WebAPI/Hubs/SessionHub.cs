using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL;
using CENT.BL.Domain.TestPackage;
using CENT.UI.DTOs.Party;
using CENT.UI.DTOs.Tests;
using CENT.UI.DTOs.Tests.Session;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace CENT.UI.Hubs
{
    public class SessionHub : Hub
    {
        private readonly SessionService _sessionService;

        public SessionHub(SessionService sessionService)
        {
            _sessionService = sessionService;
        }

        /**
         * Let a teacher open up a session for students to join.
         * Teacher must be the owner of the session.
         */
        [Authorize]
        public async Task OpenSession(string id)
        {
            var session = await GetAuthorizedSessionId(id);
            if (session.Status != SessionStatus.PREPARED)
                throw new HubException("Unable to open session, incorrect session status");

            // open session
            await _sessionService.ChangeSessionStatus(session.Id, SessionStatus.OPEN);

            // add teacher to group
            await Groups.AddToGroupAsync(Context.ConnectionId, session.Pin);
            await Clients.Caller.SendAsync("OpenSessionResult", new SessionDTO(session));
        }

        /**
         * Lets a teacher end a session
         */
        [Authorize]
        public async Task EndSession(string id)
        {
            var session = await GetAuthorizedSessionId(id);
            if (session.Status != SessionStatus.ACTIVE && session.Status != SessionStatus.OPEN)
                throw new HubException("Unable to close session, incorrect session status");

            // close session
            session = await _sessionService.ChangeSessionStatus(session.Id, SessionStatus.CLOSED);

            await Clients.Caller.SendAsync("SessionClosed", new SessionDTO(session));
        }

        /**
         * Add a student to an active session based on the received pin.
         * Collect the available parties for the session if student joined a Party Game.
         */
        public async Task JoinSession(string pin)
        {
            // validate session
            var session = await _sessionService.GetSessionsByPin(pin);
            if (session.Status != SessionStatus.OPEN && !session.Test.IsVotingPoll)
                throw new HubException("Session is not yet active, unable to join");

            // create and assign session member;
            var sessionMember = new SessionMember()
            {
                Opaque = Guid.NewGuid().ToString(),
                ConnectionId = Context.ConnectionId,
                CurrentIndex = 0,
                Session = session
            };
            sessionMember = await _sessionService.AddMemberToSession(sessionMember);
            var connected = await _sessionService.GetConnectedSessionMembersForSession(session.Id);
            await Groups.AddToGroupAsync(Context.ConnectionId, pin);

            // notify already connected clients with actions
            await Clients.Group(pin).SendAsync("StudentConnected", connected);

            // create session joined dto
            var eventDto = new SessionJoinedDTO()
            {
                Pin = pin,
                Opaque = sessionMember.Opaque,
                Type = Enum.GetName(typeof(GameType), session.Type),
                Connected = connected,
                parties = new List<PartyDTO>(),
                TrueColor = "",
                FalseColor = "",
                AllowViewScore = session.Setting.AllowSeeScore,
                AllowViewAnswerWhenFinished = session.Setting.AllowViewAnswersWhenFinished,
                IsVoteTest = session.Test.IsVotingPoll,
                TestId =  session.Test.Id
            };

            // add parties to event dto if party game
            if (session.Type != GameType.NORMAL_GAME)
            {
                // filter parties associated with test
                eventDto.parties.AddRange(
                    session.Test.TestParties.Select(tp => new PartyDTO(tp.Party))
                );
            }

            if (session.Type == GameType.DEBATE_GAME)
            {
                eventDto.TrueColor = session.Setting.TrueColor;
                eventDto.FalseColor = session.Setting.FalseColor;
            }

            // fire event to caller
            await Clients.Caller.SendAsync("SessionJoined", eventDto);

            // try serving a question
            await TryServeQuestionFor(sessionMember);
        }

        /**
         * Select the party the student has chosen to play.
         * Only used if test is a PartyTest.
         */
        public async Task SelectParty(string opaque, string partyId)
        {
            // validate sessionMember
            SessionMember sessionMember = await _sessionService.GetSessionMemberByOpaque(opaque);
            if (sessionMember.Session.Type != GameType.PARTY_GAME)
                throw new HubException("Illegal operation, session is not of type party game");

            // let user pick a political party even if session is active
            if (sessionMember.Session.Status != SessionStatus.ACTIVE &&
                sessionMember.Session.Status != SessionStatus.OPEN)
                throw new HubException("Unable to start session, incorrect session status");

            // update database with selected party
            // throws error if no party found
            try
            {
                sessionMember = await _sessionService.SelectParty(opaque, partyId);
                await TryServeQuestionFor(sessionMember);
            }
            catch
            {
                throw new HubException("Unable to select party with id " + partyId);
            }
        }

        /**
         * Start the session and start serving questions to the students in the session group
         */
        [Authorize]
        public async Task StartSession(string id, bool forceStart)
        {
            // validate request
            var session = await GetAuthorizedSessionId(id);
            if (session.Status != SessionStatus.OPEN)
                throw new HubException("Unable to start session, incorrect session status");

            var sessionMembers = await _sessionService.GetAllSessionMembersBySession(session);

            if (session.Type == GameType.PARTY_GAME && !forceStart)
            {
                //check how many sessionMembers haven't selected a party yet.
                int sessionMembersNotReady =
                    sessionMembers.Count(sessionMember => string.IsNullOrEmpty(sessionMember.PartyId));

                if (sessionMembersNotReady > 0)
                {
                    await Clients.Caller.SendAsync("NumberOfMembersNotReady", sessionMembersNotReady);
                    return;
                }
            }

            // activate session
            session = await _sessionService.ChangeSessionStatus(session.Id, SessionStatus.ACTIVE);

            // notify teacher
            await Clients.Caller.SendAsync("SessionStarted", new SessionDTO(session));

            // serve first question to students
            await TryServeQuestionFor(session);
        }

        /**
         * Students gives an answer for a question, depending on the configuration of
         * the test a next question is served.
         */
        public async Task Answer(
            string opaque,
            string questionId,
            int questionIndex,
            string answerId,
            string comment = null)
        {
            var sm = await _sessionService.GetSessionMemberByOpaque(opaque);
            var answer = await _sessionService.GetAnswerById(answerId);
            await _sessionService.CreateSessionAnswer(sm, answer, comment);

            // send answer event to whole session for optional data visualisation
            // (primarily used for the teacher)
            await Clients.Group(sm.Session.Pin)
                .SendAsync("AnswerReceived", new AnswerReceivedDTO(questionId, answerId, comment));

            // try serving the next question
            await TryServeQuestionFor(sm);
        }

        /**
         * Notifies all clients in the group for the specific session
         * with the new question. 
         */
        [Authorize]
        public async Task NextQuestion(string sessionId, int questionIndex)
        {
            // fetch session and authorize request
            var session = await GetAuthorizedSessionId(sessionId);
            if (session.Status != SessionStatus.ACTIVE)
                throw new HubException("Unable to serve next question, incorrect session status");

            // try serving next question for the session
            await TryServeQuestionFor(session);
        }

        /**
         * Returns the current question of a session.
         * This allows users who have lost connection somehow to still rejoin
         */
        public async Task GetCurrentQuestion(string pin)
        {
            // fetch session and authorize request
            var session = await _sessionService.GetSessionById(pin);
            if (session.Status != SessionStatus.ACTIVE)
                throw new HubException("Unable to serve question, incorrect session status");

            if (session.CurrentQuestion == null)
                throw new HubException("Session has not yet been started or is in free answering mode");

            var dto = CreateServeQuestionDtoFrom(session.CurrentQuestion);
            await Clients.Caller.SendAsync("CurrentQuestion", dto);
        }

        // == helper methods ===============

        /**
         * Tries serving a question to all connected clients for
         * the session.
         */
        private async Task TryServeQuestionFor(Session session)
        {
            // check if session is active
            if (session.Status != SessionStatus.ACTIVE) return;

            // end of questions reached
            if (session.CurrentQuestion != null && session.CurrentQuestion.Next == null) return;

            // server next question
            var node = session.GetNextQuestion();
            var dto = CreateServeQuestionDtoFrom(node);
            await Clients.Group(session.Pin).SendAsync("ServeQuestion", dto);
        }

        /**
         * Tries serving a question to the session member completing
         * an action on the session hub.
         */
        private async Task TryServeQuestionFor(SessionMember sessionMember)
        {
            var session = sessionMember.Session;

            // do not serve question if session not active
            if (session.Status != SessionStatus.ACTIVE) return;

            // check if member has selected party if needed
            if (session.Type == GameType.PARTY_GAME && sessionMember.PartyId == null) return;

            // check if session has been completed
            if (session.Questions.All(q => q.Index != sessionMember.CurrentIndex))
            {
                await Clients.Caller.SendAsync("SessionCompleted");
                await Clients.GroupExcept(session.Pin, Context.ConnectionId).SendAsync("StudentFinished");
                return;
            }

            // check if user can complete test at own speed
            if (!session.Setting.AllowFreeAnswering) return;

            // serve question 
            var dto = CreateServeQuestionDtoFrom(session, sessionMember.CurrentIndex);
            await Clients.Caller.SendAsync("ServeQuestion", dto);
        }

        /**
         * Creates a serve question DTO from a session and the current index 
         */
        private ServeQuestionDTO CreateServeQuestionDtoFrom(Session session, int index)
        {
            var question = session.Questions.FirstOrDefault(q => q.Index == index);
            return CreateServeQuestionDtoFrom(question, session.Type == GameType.PARTY_GAME);
        }

        /**
         * Creates a serve question DTO from a question node
         */
        private ServeQuestionDTO CreateServeQuestionDtoFrom(QuestionNode node, bool hideArguments = false)
        {
            var questionStatements = hideArguments
                ? new List<PartyArgumentsDTO>()
                : node.Value.Test.Statements
                    .Where(s => s.Party != null && s.Answer.Question.Id == node.Value.Id)
                    .Select(s => new PartyArgumentsDTO(s))
                    .ToList();

            return new ServeQuestionDTO
            {
                QuestionId = node.Value.Id,
                CurrentQuestionIndex = node.Index,
                Question = node.Value.QuestionString,
                Video = node.Value.Video ?? "",
                SkipEnabled = node.Value.AllowSkip,
                ForceArgument = node.Value.ForceGiveArgument,
                EnableArgument = node.Value.AllowGiveArgument,
                IsLast = node.Next == null,
                Answers = node.Value.Answers.Select(a => new ServeAnswerDTO(a)).ToList(),
                Definitions = node.Value.WordExplanations.Select(w => new WordExplanationDTO(w)).ToList(),
                PartyArguments = questionStatements
            };
        }

        private async Task<Session> GetAuthorizedSessionId(string id)
        {
            var session = await _sessionService.GetSessionById(id);
            if (Context.User.Identity.Name != session.Owner.Id)
                throw new HubException("Unauthorized to start this session");
            return session;
        }
    }
}