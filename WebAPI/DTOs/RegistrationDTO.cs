using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.UserPackage;

namespace CENT.UI.DTOs
{
    public class RegistrationDTO
    {
        [Required] public string Email { get; set; }

        [Required] public string Password { get; set; }

        public Role? Role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}