using CENT.BL.Domain.TestPackage;

namespace CENT.UI.DTOs.Tests
{
    public class VoteTestDTO
    {
        public VoteTestDTO(Test test)
        {
            Id = test.Id;
            Title = test.Title;
            IsVoteTest = test.IsVotingPoll;
        }

        public string Id { get; set; }
        public string Title { get; set; }

        public bool IsVoteTest { get; set; } = true;
    }
}