using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.Tests.Question
{
    public class QuestionsDTO
    {
        [Required] public ICollection<QuestionDTO> Questions { get; set; }
    }
}