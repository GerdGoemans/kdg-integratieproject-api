using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CENT.BL.Domain.TestPackage;

namespace CENT.UI.DTOs.Tests.Question
{
    public class QuestionDTO
    {
        public string QuestionId { get; set; }
        [Required] public string QuestionString { get; set; }

        public string Video { get; set; } = "";
        [Required] public string LanguageCode { get; set; }
        [Required] public bool AllowSkip { get; set; }
        [Required] public bool AllowGiveArgument { get; set; }
        [Required] public bool ForceGiveArgument { get; set; }
        public bool IsMultipleChoice { get; set; }
        public List<string> CorrectAnswers { get; set; } = new List<string>();
        public List<string> IncorrectAnswers { get; set; } = new List<string>();
        public List<WordExplanationDTO> Explanations { get; set; } = new List<WordExplanationDTO>();


        // -- constructor ------------------
        public QuestionDTO()
        {
        }

        public QuestionDTO(BL.Domain.TestPackage.Question question)
        {
            QuestionId = question.Id;
            QuestionString = question.QuestionString;
            Video = question.Video;
            LanguageCode = question.LanguageCode;
            AllowSkip = question.AllowSkip;
            AllowGiveArgument = question.AllowGiveArgument;
            ForceGiveArgument = question.ForceGiveArgument;
            IsMultipleChoice = !question.Answers.Any(q =>
                q.Value == CommonAnswerValue.AGREE_VALUE || q.Value == CommonAnswerValue.DISAGREE_VALUE);
            CorrectAnswers = question.Answers.Where(a => a.Correct.HasValue && a.Correct.Value).Select(a => a.Value)
                .ToList();
            IncorrectAnswers = question.Answers.Where(a => a.Correct.HasValue && !a.Correct.Value).Select(a => a.Value)
                .ToList();
            Explanations = question.WordExplanations.Select(w => new WordExplanationDTO(w)).ToList();
        }
    }
}