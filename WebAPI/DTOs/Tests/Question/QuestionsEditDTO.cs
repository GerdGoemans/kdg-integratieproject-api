using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.Tests.Question
{
    public class QuestionsEditDTO
    {
        [Required] public ICollection<QuestionDTO> Questions { get; set; }
    }
}