using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.UI.DTOs.Tests.Answer;

namespace CENT.UI.DTOs.Tests.Question
{
    public class QuestionResultDTO
    {
        public QuestionResultDTO()
        {
        }

        public QuestionResultDTO(BL.Domain.TestPackage.Question question, BL.Domain.TestPackage.Answer answer,
            List<AnswerDTO> answerDtos, List<StatementDTO> statements)
        {
            QuestionId = question.Id;
            QuestionString = question.QuestionString;
            UserAnswer = answer.Value;
            Answers = answerDtos;
            Statements = statements;
        }

        public string QuestionId { get; set; }
        [Required] public string QuestionString { get; set; }

        [Required] public string UserAnswer { get; set; }
        [Required] public List<AnswerDTO> Answers { get; set; }

        [Required] public List<StatementDTO> Statements { get; set; }
    }
}