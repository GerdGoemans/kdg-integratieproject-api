﻿using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.Tests.Session
{
    public class AnswerReceivedDTO
    {
        public AnswerReceivedDTO(string questionId, string answerId, string argument)
        {
            QuestionId = questionId;
            AnswerId = answerId;
            Argument = argument;
        }

        [Required] public string QuestionId { get; set; }
        [Required] public string AnswerId { get; set; }
        public string Argument { get; set; }
    }
}