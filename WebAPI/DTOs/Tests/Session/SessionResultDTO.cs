using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.UI.DTOs.Party;
using CENT.UI.DTOs.Tests.Question;

namespace CENT.UI.DTOs.Tests.Session
{
    public class SessionResultDTO
    {
        [Required] public string Title { get; set; }
        [Required] public string Score { get; set; }
        [Required] public string NumberOfQuestions { get; set; }
        [Required] public PartyDTO Party { get; set; }

        [Required] public List<QuestionResultDTO> Questions { get; set; }
        [Required] public List<PartyRelatedDTO> PartyRelated { get; set; }
    }
}