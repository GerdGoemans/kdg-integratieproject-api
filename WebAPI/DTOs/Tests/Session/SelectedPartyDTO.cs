using CENT.UI.DTOs.Party;

namespace CENT.UI.DTOs.Tests.Session
{
    public class SelectedPartyDTO
    {
        public PartyDTO Party { get; set; }
        public int count { get; set; }
    }
}