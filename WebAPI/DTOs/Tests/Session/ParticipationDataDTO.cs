using System.Collections.Generic;

namespace CENT.UI.DTOs.Tests.Session
{
    public class ParticipationDataDTO
    {
        public int Playercount { get; set; }
        public int AnswerCount { get; set; }
        public List<SelectedPartyDTO> SelectedPartyCount { get; set; }
    }
}