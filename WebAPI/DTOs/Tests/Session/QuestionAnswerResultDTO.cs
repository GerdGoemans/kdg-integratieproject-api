using CENT.UI.DTOs.Tests.Answer;

namespace CENT.UI.DTOs.Tests.Session
{
    public class QuestionAnswerResultDTO
    {
        public AnswerDTO Answer { get; set; }
        public int AnswerCount { get; set; }
    }
}