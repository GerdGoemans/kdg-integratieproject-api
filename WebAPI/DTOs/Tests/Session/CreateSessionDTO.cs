﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.TestPackage;

namespace CENT.UI.DTOs.Tests.Session
{
    public class CreateSessionDTO
    {
        public GameType Type { get; set; }
        public bool? AllowFreeAnswering { get; set; }
        public bool? AllowSeeScore { get; set; }
        public bool? AllowViewAnswersWhenFinished { get; set; }

        [Required] public string AgreesColor { get; set; }

        [Required] public string DisagreesColor { get; set; }

        [Required] public ICollection<string> Questions { get; set; }

        public bool? Randomized { get; set; }
        public string CourseId { get; set; }
    }
}