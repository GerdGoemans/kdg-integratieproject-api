using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.UI.DTOs.Party;

namespace CENT.UI.DTOs.Tests.Session
{
    public class SessionJoinedDTO
    {
        [Required] public string Pin { get; set; }
        [Required] public string Opaque { get; set; }
        [Required] public string Type { get; set; }
        [Required] public int Connected { get; set; }
        [Required] public List<PartyDTO> parties { get; set; }
        public string TrueColor { get; set; }
        public string FalseColor { get; set; }
        public bool AllowViewScore { get; set; }
        public bool AllowViewAnswerWhenFinished { get; set; }
        public bool IsVoteTest { get; set; }
        public string TestId { get; set; }
    }
}