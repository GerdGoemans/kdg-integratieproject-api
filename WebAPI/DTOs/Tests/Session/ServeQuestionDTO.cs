using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.UI.DTOs.Party;

namespace CENT.UI.DTOs.Tests.Session
{
    public class ServeQuestionDTO
    {
        public string QuestionId { get; set; }
        [Required] public int CurrentQuestionIndex { get; set; }
        [Required] public string Question { get; set; }
        public string Video { get; set; }
        [Required] public bool SkipEnabled { get; set; }
        [Required] public bool ForceArgument { get; set; }
        [Required] public bool EnableArgument { get; set; }
        [Required] public bool IsLast { get; set; }
        [Required] public List<ServeAnswerDTO> Answers { get; set; } = new List<ServeAnswerDTO>();
        [Required] public List<WordExplanationDTO> Definitions { get; set; } = new List<WordExplanationDTO>();
        public List<PartyArgumentsDTO> PartyArguments { get; set; } = new List<PartyArgumentsDTO>();
    }
}