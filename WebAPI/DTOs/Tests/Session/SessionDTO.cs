using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CENT.BL.Domain.TestPackage;

namespace CENT.UI.DTOs.Tests.Session
{
    public class SessionDTO
    {
        public SessionDTO(BL.Domain.TestPackage.Session session)
        {
            Id = session.Id;
            Pin = session.Pin;
            Name = session.Name;
            Status = session.Status;
            UserId = session.Owner.Id;
            TestSettingId = session.Setting.Id;
            TestId = session.Test.Id;
            QuestionIds = session.Questions.Select(q => q.Value.Id);
            CurrentQuestionIndex = session.CurrentQuestion?.Index ?? 0;
            if (session.Course != null) ClassName = session.Course.Name;
        }

        public string Id { get; set; }
        public string Pin { get; set; }
        [Required] [MinLength(2)] public string Name { get; set; }
        [Required] public SessionStatus Status { get; set; }
        [Required] public string UserId { get; set; }
        [Required] public string TestSettingId { get; set; }
        [Required] public string TestId { get; set; }
        [Required] public IEnumerable<string> QuestionIds { get; set; }
        [Required] public int CurrentQuestionIndex { get; set; }
        public string ClassName { get; set; } = "";
    }
}