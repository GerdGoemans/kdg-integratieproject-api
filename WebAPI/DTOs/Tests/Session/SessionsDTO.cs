﻿using System.Collections.Generic;

namespace CENT.UI.DTOs.Tests.Session
{
    public class SessionsDTO
    {
        public SessionsDTO()
        {
        }

        public SessionsDTO(IEnumerable<BL.Domain.TestPackage.Session> s)
        {
            foreach (var session in s) Sessions.Add(new SessionDTO(session));
        }

        public List<SessionDTO> Sessions { get; set; } = new List<SessionDTO>();
    }
}