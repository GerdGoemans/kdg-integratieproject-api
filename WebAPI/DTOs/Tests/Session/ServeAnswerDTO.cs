using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.Tests.Session
{
    public class ServeAnswerDTO
    {
        public ServeAnswerDTO()
        {
        }

        public ServeAnswerDTO(BL.Domain.TestPackage.Answer answer)
        {
            Id = answer.Id;
            Value = answer.Value;
        }

        [Required] public string Id { get; set; }

        [Required] public string Value { get; set; }
    }
}