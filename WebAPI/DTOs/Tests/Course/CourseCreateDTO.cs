﻿using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.Tests.Course
{
    public class CourseCreateDTO
    {
        public CourseCreateDTO()
        {
        }

        public CourseCreateDTO(BL.Domain.TestPackage.Course course)
        {
            Name = course.Name;
            AmountOfStudents = course.AmountOfStudents;
            SchoolYear = course.SchoolYear;
        }

        [Required] public string Name { get; set; }

        [Required] public int AmountOfStudents { get; set; }

        public string SchoolYear { get; set; }
    }
}