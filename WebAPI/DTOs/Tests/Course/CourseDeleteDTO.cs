﻿using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.Tests.Course
{
    public class CourseDeleteDTO
    {
        [Required] public string Id { get; set; }
    }
}