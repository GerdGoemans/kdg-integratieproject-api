﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.Tests.Course
{
    public class EditCourseDTO
    {
        [Required] public string Id { get; set; }
        [Required] public string Name { get; set; }
        [Required] public int AmountOfStudents { get; set; }
        public string Schooljaar { get; set; }
        public virtual List<string> TestsIds { get; set; } = new List<string>();
    }
}