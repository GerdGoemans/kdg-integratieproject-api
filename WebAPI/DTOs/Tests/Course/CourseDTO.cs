﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CENT.UI.DTOs.Tests.Course
{
    public class CourseDTO
    {
        public CourseDTO()
        {
        }

        public CourseDTO(BL.Domain.TestPackage.Course course)
        {
            Id = course.Id;
            Name = course.Name;
            AmountOfStudents = course.AmountOfStudents;
            SchoolYear = course.SchoolYear;
            TestsIds = course.TestCourses.Select(tc => tc.Test.Id).ToList();
            UserId = course.Owner.Id;
        }

        [Required] public string Id { get; set; }
        [Required] public string Name { get; set; }
        [Required] public int AmountOfStudents { get; set; }
        public string SchoolYear { get; set; }
        public ICollection<string> TestsIds { get; set; } = new List<string>();
        public string UserId { get; set; }
    }
}