﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CENT.BL.Domain.TestPackage;
using CENT.UI.DTOs.Tests.Course;
using CENT.UI.DTOs.Tests.Tag;

namespace CENT.UI.DTOs.Tests
{
    public class TestDTO
    {
        // -- constructors -----------------
        public TestDTO(Test test, string invokerId)
        {
            Id = test.Id;
            Title = test.Title;
            Visibility = test.Visibility;
            LanguageCode = test.LanguageCode;
            SettingId = test.Setting.Id;
            OwnerId = test.Owner.Id;
            if (!string.IsNullOrEmpty(invokerId)) IsFavourite = test.UserFavourites.Any(u => u.User.Id == invokerId);
            IsLocked = test.IsLocked;
            VoteTestEnabled = test.VoteTestEnabled;
            WordExplanationIds = test.WordExplanations.Select(we => we.Id);
            QuestionIds = test.Questions.Select(q => q.Id);
            Tags = test.TestTags.Select(tt => new TagDTO(tt.Tag));
            PartyIds = test.TestParties.Select(tp => tp.Party.Id);
            CreatedDate = test.CreatedDate;
            Courses = test.TestCourses.Select(tc => new CourseDTO(tc.Course));
            IsVotingPoll = test.IsVotingPoll;
            AmountUsed = test.AmountUsed;
        }

        public string Id { get; set; }
        [Required] public string Title { get; set; }
        [Required] public Visibility Visibility { get; set; }
        [Required] public string LanguageCode { get; set; }
        [Required] public string SettingId { get; set; }
        [Required] public string OwnerId { get; set; }
        [Required] public bool IsFavourite { get; set; }

        [Required] public int AmountUsed { get; set; }
        public bool IsLocked { get; set; }
        [Required] public DateTime CreatedDate { get; set; }
        [Required] public IEnumerable<string> WordExplanationIds { get; set; }
        [Required] public IEnumerable<string> QuestionIds { get; set; }
        [Required] public IEnumerable<TagDTO> Tags { get; set; }
        [Required] public IEnumerable<string> PartyIds { get; set; }
        public IEnumerable<CourseDTO> Courses { get; set; }
        [Required] public bool VoteTestEnabled { get; set; }

        public bool IsVotingPoll { get; set; }
    }
}