﻿using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.Tests.Tag
{
    public class TagDTO
    {
        // -- constructor ------------------
        public TagDTO()
        {
        }

        public TagDTO(BL.Domain.TestPackage.Tag tag)
        {
            Id = tag.Id;
            Color = tag.Color;
            Name = tag.Name;
        }

        public string Id { get; set; }
        public string Color { get; set; }
        [Required] public string Name { get; set; }
    }
}