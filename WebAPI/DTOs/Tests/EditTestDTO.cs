﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.TestPackage;
using CENT.UI.DTOs.Tests.Settings;

namespace CENT.UI.DTOs.Tests
{
    public class EditTestDTO
    {
        [Required] public string Title { get; set; }
        [Required] public Visibility Visibility { get; set; }
        [Required] public string LanguageCode { get; set; }
        [Required] public TestSettingsDTO Settings { get; set; }
        public List<WordExplanationDTO> WordExplanations { get; set; }
    }
}