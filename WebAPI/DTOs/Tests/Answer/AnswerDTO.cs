namespace CENT.UI.DTOs.Tests.Answer
{
    public class AnswerDTO
    {
        public AnswerDTO(BL.Domain.TestPackage.Answer answer)
        {
            Value = answer.Value;
            Correct = answer.Correct;
        }

        public string Value { get; set; }
        public bool? Correct { get; set; }
    }
}