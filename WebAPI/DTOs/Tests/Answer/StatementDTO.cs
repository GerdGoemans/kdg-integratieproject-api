using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.TestPackage;
using CENT.UI.DTOs.Party;

namespace CENT.UI.DTOs.Tests.Answer
{
    public class StatementDTO
    {
        public StatementDTO(Statement statement, BL.Domain.PartyPackage.Party party,
            BL.Domain.TestPackage.Answer answer)
        {
            Id = statement.Id;
            Argument = statement.Argument;
            PartyDto = new PartyDTO(party);
            PartyAnswer = answer.Value;
        }

        public StatementDTO(Statement statement)
        {
            Id = statement.Id;
            Argument = statement.Argument;
            PartyDto = new PartyDTO(statement.Party);
            PartyAnswer = statement.Answer.Value;
        }

        [Required] public string Id { get; set; }
        [Required] public string Argument { get; set; }
        [Required] public PartyDTO PartyDto { get; set; }
        [Required] public string PartyAnswer { get; set; }
    }
}