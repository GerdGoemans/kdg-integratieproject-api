﻿using CENT.BL.Domain.ValidationAttributes;

namespace CENT.UI.DTOs.Tests.Settings
{
    public class TestSettingsDTO
    {
        public bool? AllowFreeAnswering { get; set; }
        public bool? AllowSeeScore { get; set; }
        public bool? AllowViewAnswersWhenFinished { get; set; }
        [IsHexColorCode(IsRequired = false)] public string TrueColor { get; set; }
        [IsHexColorCode(IsRequired = false)] public string FalseColor { get; set; }
    }
}