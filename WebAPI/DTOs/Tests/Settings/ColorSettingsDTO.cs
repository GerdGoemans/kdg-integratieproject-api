﻿using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.ValidationAttributes;

namespace CENT.UI.DTOs.Tests.Settings
{
    public class ColorSettingsDTO
    {
        [Required] [IsHexColorCode] public string Color { get; set; }
    }
}