﻿using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.TestPackage;

namespace CENT.UI.DTOs.Tests
{
    public class WordExplanationDTO
    {
        [Required] public string Word { get; set; }
        [Required] public string Explanation { get; set; }

        public WordExplanationDTO() {}
        public WordExplanationDTO(WordExplanation wordExplanation)
        {
            Word = wordExplanation.Word;
            Explanation = wordExplanation.Explanation;
        }
    }
}