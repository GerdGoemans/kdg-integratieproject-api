﻿using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.Tests
{
    public class CsvDTO
    {
        [Required] public string csv { get; set; }
    }
}