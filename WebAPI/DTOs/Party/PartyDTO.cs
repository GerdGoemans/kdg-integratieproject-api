﻿using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.ValidationAttributes;

namespace CENT.UI.DTOs.Party
{
    public class PartyDTO
    {
        public PartyDTO()
        {
        }

        public PartyDTO(BL.Domain.PartyPackage.Party party)
        {
            Id = party.Id;
            Name = party.Name;
            Slogan = party.Slogan;
            Color = party.Color;
            Logo = party.Logo;
        }

        public string Id { get; set; }
        [Required] public string Name { get; set; }
        [Required] public string Slogan { get; set; }

        [Required]
        [StringLength(7)]
        [IsHexColorCode]
        public string Color { get; set; } //Hex color code => #000000 => 7 chars

        [Required] public string Logo { get; set; }
    }
}