﻿namespace CENT.UI.DTOs.Party
{
    public class PartyRelatedDTO
    {
        public PartyRelatedDTO(BL.Domain.PartyPackage.Party party, int amount)
        {
            Party = new PartyDTO(party);
            AmountRelated = amount;
        }

        public PartyRelatedDTO()
        {
        }

        public PartyDTO Party { get; set; }
        public int AmountRelated { get; set; }
    }
}