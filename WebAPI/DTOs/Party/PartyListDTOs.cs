﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.Party
{
    public class PartyListDTOs
    {
        [Required] public ICollection<string> PartyIds { get; set; }
    }
}