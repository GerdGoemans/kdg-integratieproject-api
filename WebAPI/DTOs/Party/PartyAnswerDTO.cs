﻿using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.Party
{
    public class PartyAnswerDTO
    {
        public string StatementId { get; set; }
        public string QuestionId { get; set; }
        [Required] public string PartyId { get; set; }
        [Required] public string Answer { get; set; }
        [Required] public string Explanation { get; set; }
    }
}