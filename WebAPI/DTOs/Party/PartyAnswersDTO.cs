﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.Party
{
    public class PartyAnswersDTO
    {
        [Required] public ICollection<PartyAnswerDTO> PartyAnswers { get; set; }
    }
}