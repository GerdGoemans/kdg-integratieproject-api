using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.TestPackage;

namespace CENT.UI.DTOs.Party
{
    public class PartyArgumentsDTO
    {
        public PartyArgumentsDTO()
        {
        }

        public PartyArgumentsDTO(Statement statement)
        {
            Party = new PartyDTO(statement.Party);
            Answer = statement.Answer.Value;
            Explanation = statement.Argument;
        }

        [Required] public PartyDTO Party { get; set; }

        [Required] public string Answer { get; set; }

        [Required] public string Explanation { get; set; }
    }
}