using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.Party
{
    public class PartyTestPartyDto
    {
        [Required] public string PartyId { get; set; }
        [Required] public ICollection<PartyAnswerDTO> Answers { get; set; }
    }
}