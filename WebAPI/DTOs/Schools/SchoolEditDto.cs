﻿namespace CENT.UI.DTOs.Schools
{
    public class SchoolEditDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public string LoginMail { get; set; }
    }
}