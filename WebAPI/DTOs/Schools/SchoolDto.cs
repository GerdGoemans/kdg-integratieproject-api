﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.UserPackage;
using CENT.BL.Domain.ValidationAttributes;

namespace CENT.UI.DTOs.Schools
{
    public class SchoolDto
    {
        public SchoolDto()
        {
        }

        public SchoolDto(School school)
        {
            Id = school.Id;
            Name = school.Name;
            Address = school.Address;
            Phone = school.Phone;
            Email = school.Email;
            LoginMail = school.Admin.Email;
            Lang = school.Lang;
            AdminId = school.Admin.Id;
            IsBlocked = school.IsBlocked;
            foreach (var t in school.Teachers) TeacherIds.Add(t.Id);
        }

        public string Id { get; set; }
        [Required] [MinLength(2)] public string Name { get; set; }
        [Required] [MinLength(2)] public string Address { get; set; }
        [Required] [MinLength(2)] public string Phone { get; set; }

        [Required]
        [MinLength(2)]
        [IsValidEmail]
        public string Email { get; set; }

        [Required]
        [MinLength(2)]
        [IsValidEmail]
        public string LoginMail { get; set; }

        [Required] [MinLength(2)] public string Lang { get; set; }
        public ICollection<string> TeacherIds { get; set; } = new List<string>();
        public string AdminId { get; set; }
        public bool IsBlocked { get; set; }
    }
}