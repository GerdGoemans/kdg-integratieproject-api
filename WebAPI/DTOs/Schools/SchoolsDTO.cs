﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.UserPackage;

namespace CENT.UI.DTOs.Schools
{
    public class SchoolsDTO
    {
        public SchoolsDTO(IEnumerable<School> schools)
        {
            SchoolsDto = new List<SchoolDto>();
            foreach (var school in schools) SchoolsDto.Add(new SchoolDto(school));
        }

        [Required] public ICollection<SchoolDto> SchoolsDto { get; set; }
    }
}