﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.UserPackage;

namespace CENT.UI.DTOs.Users
{
    public class UsersDTO
    {
        public UsersDTO()
        {
        }

        public UsersDTO(IEnumerable<User> users)
        {
            foreach (var user in users) Users.Add(new UserDTO(user, ""));
        }

        [Required] public List<UserDTO> Users { get; set; } = new List<UserDTO>();
    }
}