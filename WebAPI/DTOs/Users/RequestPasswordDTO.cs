using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.ValidationAttributes;

namespace CENT.UI.DTOs.Users
{
    public class RequestPasswordDTO
    {
        [Required]
        [MinLength(2)]
        [IsValidEmail]
        public string Email { get; set; }
    }
}