using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.Users
{
    public class ResetPasswordDTO
    {
        [Required] [StringLength(32)] public string Key { get; set; }
    }
}