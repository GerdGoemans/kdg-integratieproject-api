﻿using System.Collections.Generic;
using CENT.UI.DTOs.Tests.Session;

namespace CENT.UI.DTOs.Users
{
    public class TeacherSessionQuestionResultDTO
    {
        public string QuestionString { get; set; }
        public List<QuestionAnswerResultDTO> AnswerResultDto { get; set; }
        public List<string> Arguments { get; set; }
    }
}