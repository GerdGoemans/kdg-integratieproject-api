﻿using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.UserPackage;
using CENT.BL.Domain.ValidationAttributes;

namespace CENT.UI.DTOs.Users
{
    public class UserDTO
    {
        public UserDTO()
        {
        }

        public UserDTO(User user) : this(user, null)
        {
        }

        public UserDTO(User user, string token)
        {
            Id = user.Id;
            Email = user.Email;
            Role = user.Role;
            Token = token;
            if (user is Teacher teacher)
            {
                FirstName = teacher.FirstName;
                LastName = teacher.LastName;
                SchoolId = teacher.School?.Id;
            }

            if (user.AdminSchool != null) SchoolId = user.AdminSchool.Id;
        }

        public string Id { get; set; }

        [Required]
        [MinLength(2)]
        [IsValidEmail]
        public string Email { get; set; }

        public Role? Role { get; set; }
        [Required] [MinLength(2)] public string FirstName { get; set; }
        [Required] [MinLength(2)] public string LastName { get; set; }
        public string SchoolId { get; set; }
        public string Token { get; set; }
    }
}