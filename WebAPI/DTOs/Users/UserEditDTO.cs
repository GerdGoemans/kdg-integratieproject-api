﻿using System.ComponentModel.DataAnnotations;
using CENT.BL.Domain.ValidationAttributes;

namespace CENT.UI.DTOs.Users
{
    public class UserEditDTO
    {
        [Required] [MinLength(2)] public string FirstName { get; set; }
        [Required] [MinLength(2)] public string LastName { get; set; }

        [Required]
        [MinLength(2)]
        [IsValidEmail]
        public string Email { get; set; }

        public string Password { get; set; }
        public string NewPassword { get; set; }
    }
}