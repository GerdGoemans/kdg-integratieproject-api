﻿using System.Collections.Generic;

namespace CENT.UI.DTOs.Users
{
    public class TeacherSessionResultDTO
    {
        public IEnumerable<TeacherSessionQuestionResultDTO> QuestionResults { get; set; }
    }
}