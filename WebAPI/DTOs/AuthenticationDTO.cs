using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs
{
    public class AuthenticationDTO
    {
        [Required] public string Email { get; set; }

        [Required] public string Password { get; set; }
    }
}