﻿using System.ComponentModel.DataAnnotations;

namespace CENT.UI.DTOs.UsageData
{
    public class PageVisitDTO
    {
        public string FromPage { get; set; }
        [Required] public string ToPage { get; set; }
        [Required] public int Role { get; set; } = 0;
    }
}