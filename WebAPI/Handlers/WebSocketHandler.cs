using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CENT.UI.Handlers
{
    public class WebSocketHandler
    {
        public static async Task Handle(HttpContext context, WebSocket socket)
        {
            var buffer = new byte[1024 * 4];
            var result =
                await socket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);

            while (!result.CloseStatus.HasValue)
            {
                await socket.SendAsync(new ArraySegment<byte>(buffer, 0, result.Count), result.MessageType,
                    result.EndOfMessage, CancellationToken.None);
                result = await socket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            }

            await socket.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
        }

        public static async Task Handler(HttpContext context, Func<Task> next)
        {
            //  check if websocket request
            if (!context.WebSockets.IsWebSocketRequest)
            {
                await next();
                return;
            }

            Console.WriteLine(
                $"Websocket client connected from {context.Connection.RemoteIpAddress}:{context.Connection.RemotePort}");
            var socket = await context.WebSockets.AcceptWebSocketAsync();
            // TODO: pass context and socket to correct handler
        }
    }
}