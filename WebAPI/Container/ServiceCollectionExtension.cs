using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace CENT.UI.Container
{
    public static class ServiceCollectionExtension
    {
        public delegate void AddServiceDelegate(IServiceCollection collection, Type t);

        public static IServiceCollection Scan(this IServiceCollection serviceCollection, Type type,
            AddServiceDelegate addServiceDelegate)
        {
            // get all types that match the references type
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(type.IsAssignableFrom);

            foreach (var t in types)
            {
                // ignore interfaces
                if (t.IsInterface) continue;
                addServiceDelegate(serviceCollection, t);
            }

            return serviceCollection;
        }

        public static IServiceCollection ScanGeneric(this IServiceCollection serviceCollection, Type type)
        {
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(assembly => assembly.GetTypes())
                .Where(s => s.ImplementsGeneric(type));

            foreach (var t in types)
            {
                if (t.IsGenericType) continue;
                serviceCollection.AddScoped(t);
            }


            return serviceCollection;
        }
    }
}