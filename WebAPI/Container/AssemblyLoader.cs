using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CENT.UI.Container
{
    public static class AssemblyLoader
    {
        /**
         * Loads all assemblies that match the search pattern.
         */
        public static int Load(string search)
        {
            // get loaded assemblies
            var loaded = AppDomain.CurrentDomain.GetAssemblies().ToList();
            var loadedPaths = loaded
                .Where(a => !a.IsDynamic)
                .Select(a => a.Location).ToArray();

            // get referenced assemblies matching search string
            var referenced = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, search);
            var toLoad = referenced
                .Where(r => !loadedPaths.Contains(r, StringComparer.InvariantCultureIgnoreCase))
                .ToList();

            // load each assembly into the current 
            toLoad.ForEach(path =>
            {
                var assemblyName = AssemblyName.GetAssemblyName(path);
                var assembly = AppDomain.CurrentDomain.Load(assemblyName);
                loaded.Add(assembly);
            });

            return toLoad.Count;
        }
    }
}