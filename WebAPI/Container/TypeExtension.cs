using System;

namespace CENT.UI.Container
{
    public static class TypeExtension
    {
        public static bool ImplementsGeneric(this Type type, Type generic)
        {
            if (!generic.IsGenericType) return false;
            var interfaces = type.GetInterfaces();

            foreach (var @interface in interfaces)
            {
                if (!@interface.IsGenericType) continue;
                if (@interface.GetGenericTypeDefinition() == generic) return true;
            }

            // if not match in loop -> return false
            return false;
        }
    }
}