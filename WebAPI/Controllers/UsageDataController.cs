﻿using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using CENT.BL;
using CENT.UI.DTOs.UsageData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CENT.UI.Controllers
{
    [ApiController]
    [Route("API/[controller]")]
    [Authorize(Roles = "SUPER_ADMIN")]
    public class UsageDataController : Controller
    {
        private readonly UsageDataService _usageDataService;

        public UsageDataController(UsageDataService usageDataService)
        {
            _usageDataService = usageDataService;
        }

        [HttpGet("pagevisists")]
        public async Task<IActionResult> GetPageVisits()
        {
            return Ok(await _usageDataService.GetPageVisits());
        }

        [HttpGet("tests")]
        public async Task<IActionResult> GetTestsUsageData()
        {
            return Ok(await _usageDataService.GetTestsUsageDate());
        }

        [HttpGet("schools")]
        public async Task<IActionResult> GetSchoolUsageData()
        {
            return Ok(await _usageDataService.GetSchoolData());
        }


        [HttpPost("pagevisit")]
        [AllowAnonymous]
        public async Task<IActionResult> VisitPage([FromBody] PageVisitDTO pageVisitDto)
        {
            var ipHash = GetIpAddressHashFromHttpRequest(HttpContext.Request);
            await _usageDataService.PageVisit(pageVisitDto.FromPage, pageVisitDto.ToPage, pageVisitDto.Role, ipHash);
            return NoContent();
        }

        /*
         * As we do not want to be storing specific IPs we'll hash any IP we store.
         * This is not 100% secure as these hashes can be looked up in lookup tables
         */
        private string GetIpAddressHashFromHttpRequest(HttpRequest httpRequest)
        {
            var ipAddressString = string.Empty;
            if (httpRequest == null) return ipAddressString;

            //Get the IP
            if (httpRequest.Headers != null && httpRequest.Headers.Count > 0)
            {
                if (httpRequest.Headers.ContainsKey("X-Forwarded-For"))
                {
                    string headerXForwardedFor = httpRequest.Headers["X-Forwarded-For"];
                    if (!string.IsNullOrEmpty(headerXForwardedFor))
                    {
                        var xForwardedForIpAddress = headerXForwardedFor.Split(':')[0];
                        if (!string.IsNullOrEmpty(xForwardedForIpAddress)) ipAddressString = xForwardedForIpAddress;
                    }
                }
            }
            else if (httpRequest.HttpContext == null ||
                     httpRequest.HttpContext.Connection == null ||
                     httpRequest.HttpContext.Connection.RemoteIpAddress == null)
            {
                ipAddressString = httpRequest.HttpContext.Connection.RemoteIpAddress.ToString();
            }

            //Hash the IP
            using (var sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(ipAddressString));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (var b in hash) sb.Append(b.ToString("x2"));

                return sb.ToString();
            }
        }
    }
}