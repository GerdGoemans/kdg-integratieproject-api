﻿using System.Linq;
using System.Threading.Tasks;
using CENT.BL;
using CENT.UI.DTOs.Party;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CENT.UI.Controllers
{
    [ApiController]
    [Route("API/[controller]")]
    public class PartyController : Controller
    {
        private readonly PartyService _partyService;

        public PartyController(PartyService partyService)
        {
            _partyService = partyService;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAllParties()
        {
            return Ok((await _partyService.GetAllParties()).Select(party => new PartyDTO(party)).ToList());
        }

        [HttpPut("")]
        [Authorize(Roles = "SUPER_ADMIN")]
        public async Task<IActionResult> CreateParty([FromBody] PartyDTO partyDto)
        {
            var party =
                await _partyService.CreateParty(partyDto.Name, partyDto.Slogan, partyDto.Color, partyDto.Logo);
            return Ok(new PartyDTO(party));
        }

        [HttpPatch("{partyId}")]
        [Authorize(Roles = "SUPER_ADMIN")]
        public async Task<IActionResult> EditParty(string partyId, [FromBody] PartyDTO partyDto)
        {
            var party = await _partyService.EditParty(partyId, partyDto.Name, partyDto.Slogan, partyDto.Color,
                partyDto.Logo);
            return Ok(new PartyDTO(party));
        }

        [HttpDelete("{partyId}")]
        [Authorize(Roles = "SUPER_ADMIN")]
        public async Task<IActionResult> DeleteParty(string partyId)
        {
            await _partyService.DeleteParty(partyId);
            return Ok();
        }
    }
}