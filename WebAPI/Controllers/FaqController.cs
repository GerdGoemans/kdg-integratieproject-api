﻿using System.Threading.Tasks;
using CENT.BL;
using CENT.BL.Domain.OrganisationPackage;
using CENT.BL.Domain.PageParameters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CENT.UI.Controllers
{
    [ApiController]
    [Route("API/[controller]")]
    public class FaqController : Controller
    {
        private readonly FaqService _faqService;

        public FaqController(FaqService service)
        {
            _faqService = service;
        }

        // GET
        [HttpGet]
        public async Task<IActionResult> Index([FromQuery] FaqPageParameters faqPageParameters)
        {
            if (faqPageParameters == null) return Ok(await _faqService.GetAllFaqEntries());

            var faqPageList = await _faqService.GetAllFaqEntries(faqPageParameters);

            var metadata = new
            {
                faqPageList.TotalCount,
                faqPageList.PageSize,
                faqPageList.CurrentPage,
                faqPageList.TotalPages,
                faqPageList.HasNext,
                faqPageList.HasPrevious
            };
            HttpContext.Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

            return Ok(faqPageList.List);
        }

        [HttpPut]
        [Authorize(Roles = "SUPER_ADMIN")]
        public async Task<IActionResult> CreateFaqEntry([FromBody] FAQ faq)
        {
            var madeFaq = await _faqService.AddFaq(faq.Question, faq.Answer, faq.LanguageCode);
            return Ok(madeFaq);
        }

        [HttpPatch("{faqId}")]
        [Authorize(Roles = "SUPER_ADMIN")]
        public async Task<IActionResult> EditFaqEntry(string faqId, [FromBody] FAQ faq)
        {
            var madeFaq = await _faqService.UpdateFaq(faqId, faq.Question, faq.Answer, faq.LanguageCode);
            return Ok(madeFaq);
        }

        [HttpDelete("{faqId}")]
        [Authorize(Roles = "SUPER_ADMIN")]
        public async Task<IActionResult> DeleteFaqEntry(string faqId)
        {
            await _faqService.DeleteFaq(faqId);
            return Ok();
        }
    }
}