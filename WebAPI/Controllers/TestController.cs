﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CENT.BL;
using CENT.BL.Domain.Exception;
using CENT.BL.Domain.PageParameters;
using CENT.BL.Domain.TestPackage;
using CENT.UI.DTOs.Party;
using CENT.UI.DTOs.Tests;
using CENT.UI.DTOs.Tests.Question;
using CENT.UI.DTOs.Tests.Settings;
using CENT.UI.DTOs.Tests.Tag;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CENT.UI.Controllers
{
    [ApiController]
    [Route("API/[controller]s")]
    [Authorize]
    public class TestController : Controller
    {
        private readonly QuestionService _questionService;
        private readonly TagService _tagService;
        private readonly TestService _testService;
        private readonly WordExplanationService _wordExplanationService;

        public TestController(TestService testService, TagService tagService, QuestionService questionService,
            WordExplanationService wordExplanationService)
        {
            _testService = testService;
            _tagService = tagService;
            _wordExplanationService = wordExplanationService;
            _questionService = questionService;
        }

        // =================================
        // TEST
        // =================================

        // GET     /api/tests
        [HttpGet]
        public async Task<IActionResult> GetTestByUserId([FromQuery] TestPageParameters pageParameters)
        {
            var testsPageList = await _testService.GetAllTestsFromUser(User.Identity.Name, pageParameters);

            var metadata = new
            {
                testsPageList.TotalCount,
                testsPageList.PageSize,
                testsPageList.CurrentPage,
                testsPageList.TotalPages,
                testsPageList.HasNext,
                testsPageList.HasPrevious
            };
            HttpContext.Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            var testDtos = new List<TestDTO>();

            foreach (var test in testsPageList.List) testDtos.Add(new TestDTO(test, User.Identity.Name));

            return Ok(testDtos);
        }

        // POST    /api/tests
        [HttpPost]
        public async Task<IActionResult> CreateTest([FromBody] TestCreateDTO dto)
        {
            // create basic test
            var t = await _testService.CreateTest(
                dto.Title,
                dto.Visibility,
                dto.LanguageCode,
                User.Identity.Name);

            foreach (var exp in dto.Explanations)
                t = await _wordExplanationService.AddWordExplanationToTest(t, exp.Word, exp.Explanation);

            // update settings from DTO
            var settingDto = dto.Settings;
            t = await _testService.UpdateTestSettings(t,
                settingDto.AllowFreeAnswering,
                settingDto.AllowSeeScore,
                settingDto.AllowViewAnswersWhenFinished,
                settingDto.TrueColor,
                settingDto.FalseColor);

            return Ok(new TestDTO(t, User.Identity.Name));
        }

        // GET     /api/tests/:testId
        [HttpGet("{testId}")]
        public async Task<IActionResult> GetTestById(string testId)
        {
            var test = await _testService.GetTestById(testId, User.Identity.Name);
            return Ok(new TestDTO(test, User.Identity.Name));
        }

        // PUT    /api/tests/:testId
        [HttpPut("{testId}")]
        public async Task<IActionResult> EditTest(string testId, [FromBody] EditTestDTO dto)
        {
            // update test data
            var test = await _testService.EditTest(testId,
                dto.Title,
                dto.Visibility,
                dto.LanguageCode,
                User.Identity.Name,
                dto.WordExplanations.Select(w => new WordExplanation
                {
                    Word = w.Word,
                    Explanation = w.Explanation
                }).ToList());

            // update settings 
            await _testService.UpdateTestSettings(test,
                dto.Settings.AllowFreeAnswering,
                dto.Settings.AllowSeeScore,
                dto.Settings.AllowViewAnswersWhenFinished,
                dto.Settings.TrueColor,
                dto.Settings.FalseColor);

            return Ok(new TestDTO(test, User.Identity.Name));
        }

        [HttpPut("{testId}/favourite")]
        public async Task<IActionResult> MarkTestAsFavourite(string testId)
        {
            var test = await _testService.MarkTestAsFavourite(testId, User.Identity.Name, true);
            return Ok(new TestDTO(test, User.Identity.Name));
        }

        [HttpPut("{testId}/unfavourite")]
        public async Task<IActionResult> MarkTestAsUnFavourite(string testId)
        {
            var test = await _testService.MarkTestAsFavourite(testId, User.Identity.Name, false);
            return Ok(new TestDTO(test, User.Identity.Name));
        }

        [Authorize(Roles = "SUPER_ADMIN")]
        [HttpPatch("{testId}/openVoteTest")]
        public async Task<IActionResult> OpenVoteTest(string testId)
        {
            var test = await _testService.ToggleVotingTest(testId, User.Identity.Name, true);
            return Ok(new TestDTO(test, User.Identity.Name));
        }

        [Authorize(Roles = "SUPER_ADMIN")]
        [HttpPatch("{testId}/closeVoteTest")]
        public async Task<IActionResult> CloseVoteTest(string testId)
        {
            var test = await _testService.ToggleVotingTest(testId, User.Identity.Name, false);
            return Ok(new TestDTO(test, User.Identity.Name));
        }

        [HttpDelete("{testId}")]
        public async Task<IActionResult> DeleteTest(string testId)
        {
            return Ok(await _testService.DeleteTestById(testId, User.Identity.Name));
        }

        // GET    /api/tests/popular
        [HttpGet("popular")]
        public async Task<IActionResult> PopularTests()
        {
            var testDtos = new List<TestDTO>();
            var tests = await _testService.GetPopularTests();

            foreach (var test in tests) testDtos.Add(new TestDTO(test, User.Identity.Name));

            return Ok(testDtos);
        }

        // GET    /api/tests/:testId/duplicate
        [HttpGet("{testId}/duplicate")]
        public async Task<IActionResult> DuplicateTest(string testId)
        {
            if (string.IsNullOrEmpty(testId)) throw new BadRequestException("No testId or empty testId given");
            var test = await _testService.DuplicateTest(testId, User.Identity.Name);
            return Ok(new TestDTO(test, User.Identity.Name));
        }

        [AllowAnonymous]
        [HttpGet("votetest")]
        public async Task<IActionResult> GetVoteTests([FromQuery] bool isMobile = false)
        {
            var voteTests = await _testService.GetVoteTests();
            if (isMobile)
            {
                var voteTestDtos = voteTests.Select(test => new VoteTestDTO(test)).ToList();
                return Ok(voteTestDtos);
            }

            var testDtos = voteTests.Select(test => new TestDTO(test, null));
            return Ok(testDtos);
        }

        // =================================
        // SETTINGS
        // =================================

        // PUT     /api/tests/:testId/settings
        [HttpPut("{testId}/settings")]
        public async Task<IActionResult> SetTestSettings(string testId, [FromBody] TestSettingsDTO testSettingsDto)
        {
            var test = await _testService.UpdateTestSettings(
                testId,
                User.Identity.Name,
                testSettingsDto.AllowFreeAnswering,
                testSettingsDto.AllowSeeScore,
                testSettingsDto.AllowViewAnswersWhenFinished
            );

            return Ok(new TestDTO(test, User.Identity.Name));
        }

        // =================================
        // PARTIES IN TEST
        // =================================

        // GET    /api/tests/:testId/parties
        [HttpGet("{testId}/parties")]
        public async Task<IActionResult> GetPartiesByTestId(string testId)
        {
            var dto = new List<PartyTestPartyDto>();
            var test = await _testService.GetTestById(testId, User.Identity.Name);

            foreach (var tp in test.TestParties)
            {
                // select statements from the current party
                var partyStatements = test.Statements
                    .Where(s => s.Party != null && s.Party.Id == tp.Party.Id)
                    .Select(s => new PartyAnswerDTO
                    {
                        StatementId = s.Id,
                        Answer = s.Answer.Value,
                        Explanation = s.Argument,
                        PartyId = tp.Party.Id,
                        QuestionId = s.Answer.Question.Id
                    })
                    .ToList();

                // add dto object to list
                dto.Add(new PartyTestPartyDto
                {
                    PartyId = tp.Party.Id,
                    Answers = partyStatements
                });
            }

            return Ok(dto);
        }

        // POST    /api/tests/:testId/parties
        [HttpPost("{testId}/parties")]
        public async Task<IActionResult> AddPartiesToTest(string testId, [FromBody] PartyListDTOs partyListDtOs)
        {
            var parties =
                await _testService.AddPartiesToTest(testId, User.Identity.Name, partyListDtOs.PartyIds);
            var partyDtos = new List<PartyDTO>();
            foreach (var party in parties) partyDtos.Add(new PartyDTO(party));

            return Ok(partyDtos);
        }

        // PUT    /api/tests/:testid/parties
        [HttpPut("{testId}/parties")]
        public async Task<IActionResult> UpdatePartiesFromTest(string testId, [FromBody] PartyListDTOs dto)
        {
            await _testService.UpdatePartiesFromTest(testId, User.Identity.Name, dto.PartyIds.ToList());
            return NoContent();
        }

        // POST    /api/tests/:testId/parties/statements
        [HttpPost("{testId}/parties/statements")]
        public async Task<IActionResult> SelectPartyAnswerForStatement([FromBody] PartyAnswersDTO partyAnswersDto)
        {
            foreach (var partyAnswer in partyAnswersDto.PartyAnswers)
                await _testService.SelectPartyAnswerForStatement(partyAnswer.QuestionId, partyAnswer.PartyId,
                    partyAnswer.Answer, partyAnswer.Explanation);

            return Ok();
        }

        // PUT    /api/tests/:testId/parties/statements
        [HttpPut("{testId}/parties/statements")]
        public async Task<IActionResult> UpdatePartyStatements([FromBody] PartyAnswersDTO dto)
        {
            foreach (var answer in dto.PartyAnswers)
                if (string.IsNullOrEmpty(answer.StatementId))
                    // create new statement
                    await _testService.SelectPartyAnswerForStatement(
                        answer.QuestionId,
                        answer.PartyId,
                        answer.Answer,
                        answer.Explanation);
                else
                    // update existing statement
                    await _testService.UpdatePartyStatement(
                        answer.StatementId,
                        answer.QuestionId,
                        answer.PartyId,
                        answer.Answer,
                        answer.Explanation
                    );

            return NoContent();
        }

        // ==================================
        // QUESTIONS
        // ==================================

        // GET     /api/tests/:testId/questions
        [HttpGet("{testId}/questions")]
        public async Task<IActionResult> GetQuestionsByTestId(string testId)
        {
            var test = await _testService.GetTestById(testId, User.Identity.Name);
            return Ok(test.Questions.Select(q => new QuestionDTO(q)));
        }

        // POST    /api/tests/:testId/questions
        [HttpPost("{testId}/questions")]
        public async Task<IActionResult> AddQuestionsToTest(string testId, [FromBody] QuestionsDTO dto)
        {
            var questions = new List<Question>();

            foreach (var q in dto.Questions)
            {
                var question = await _testService.CreateQuestion(
                    testId,
                    User.Identity.Name,
                    q.QuestionString,
                    q.Video,
                    q.LanguageCode,
                    q.IsMultipleChoice,
                    q.AllowSkip,
                    q.AllowGiveArgument,
                    q.ForceGiveArgument,
                    q.CorrectAnswers,
                    q.IncorrectAnswers,
                    q.Explanations.Select(w => new WordExplanation
                    {
                        Explanation = w.Explanation,
                        Word = w.Word
                    }).ToList()
                );
                questions.Add(question);
            }


            // return question dto's
            var questionDtos = new List<QuestionDTO>();
            questions.ForEach(q => questionDtos.Add(new QuestionDTO(q)));
            return Ok(questionDtos);
        }

        // PUT    /api/tests/:testId/questions
        [HttpPut("{testId}/questions")]
        public async Task<IActionResult> UpdateQuestionsFromTest(string testId, [FromBody] QuestionsEditDTO dto)
        {
            var questions = new List<Question>();
            var test = await _testService.GetTestById(testId, User.Identity.Name);
            var questionIds = dto.Questions
                .Where(q => q.QuestionId != null)
                .Select(q => q.QuestionId)
                .ToList();

            // remove omitted questions
            var omitted = test.Questions.Where(q => !questionIds.Contains(q.Id)).ToList();
            foreach (var question in omitted) await _questionService.DeleteQuestion(question);

            foreach (var q in dto.Questions)
                if (string.IsNullOrEmpty(q.QuestionId))
                {
                    // create new question
                    var question = await _testService.CreateQuestion(
                        testId,
                        User.Identity.Name,
                        q.QuestionString,
                        q.Video,
                        q.LanguageCode,
                        q.IsMultipleChoice,
                        q.AllowSkip,
                        q.AllowGiveArgument,
                        q.ForceGiveArgument,
                        q.CorrectAnswers,
                        q.IncorrectAnswers,
                        q.Explanations.Select(w => new WordExplanation
                        {
                            Explanation = w.Explanation,
                            Word = w.Word
                        }).ToList()
                    );
                    questions.Add(question);
                }
                else
                {
                    // update question
                    var question = await _questionService.UpdateQuestion(q.QuestionId,
                        q.QuestionString,
                        q.Video,
                        q.LanguageCode,
                        q.AllowSkip,
                        q.AllowGiveArgument,
                        q.ForceGiveArgument,
                        q.IsMultipleChoice,
                        q.CorrectAnswers,
                        q.IncorrectAnswers,
                        q.Explanations.Select(w => new WordExplanation
                        {
                            Explanation = w.Explanation,
                            Word = w.Word
                        }).ToList()
                    );
                    questions.Add(question);
                }

            // return question dto's
            var questionDtos = new List<QuestionDTO>();
            questions.ForEach(q => questionDtos.Add(new QuestionDTO(q)));
            return Ok(questionDtos);
        }

        // =================================
        // TAGS
        // =================================

        // GET    /api/tests/tags
        [HttpGet("tags")]
        public async Task<IActionResult> GetAllTags()
        {
            var tags = await _tagService.GetAllTags();
            var tagDtos = new List<TagDTO>();

            foreach (var tag in tags) tagDtos.Add(new TagDTO(tag));

            return Ok(tagDtos);
        }

        // PUT     /api/tests/:testid/tags
        [HttpPut("{testId}/tags")]
        public async Task<IActionResult> UpdateTagsForTest(string testId, [FromBody] string[] tagIds)
        {
            var test = await _testService.GetTestById(testId, User.Identity.Name);
            var omittedTags = test.TestTags.Where(tt => !tagIds.Contains(tt.Tag.Id)).ToList();

            // remove omitted tags
            foreach (var tag in omittedTags)
                await _tagService.RemoveTagFromTest(test, tag.Tag.Id);

            // add not yet added tags to test
            var testTagIds = test.TestTags.Select(tt => tt.Tag.Id).ToList();
            foreach (var tagId in tagIds)
                if (!testTagIds.Contains(tagId))
                    await _tagService.AddTagToTest(test, tagId);

            return NoContent();
        }

        // =================================
        // Enums
        // =================================

        // GET    /api/tests/visbilities
        [HttpGet("visibilities")]
        public IActionResult GetVisibilities()
        {
            return Ok(Enum.GetNames(typeof(Visibility)));
        }

        // GET     /api/tests/types
        [HttpGet("types")]
        public IActionResult GetTypes()
        {
            return Ok(Enum.GetNames(typeof(GameType)));
        }

        // PUT    /api/tests/{testId}/visibility/{visib}
        [HttpPut("{testId}/visibility/{visib}")]
        public async Task<IActionResult> UpdateVisibilty(string testId, Visibility visib)
        {
            var test = await _testService.EditTest(testId, null, visib, null, User.Identity.Name, null);
            return Ok(new TestDTO(test, User.Identity.Name));
        }

        [HttpGet("{testId}/explanations")]
        public async Task<IActionResult> GetWordExplanationsById(string testId)
        {
            var test = await _testService.GetTestById(testId, User.Identity.Name);

            var wordExplanations = test.WordExplanations.Select(w => new WordExplanationDTO(w)).ToList();

            return Ok(wordExplanations);
        }

        [HttpGet("{testId}/testExplanations")]
        [AllowAnonymous]
        public async Task<IActionResult> GetWordExplanationsByTestId(string testId)
        {
            var wordExplanations = (await _wordExplanationService.GetWordExplanationsByTestId(testId))
                .Select(w => new WordExplanationDTO(w)).ToList();
            return Ok(wordExplanations);
        }
    }
}