using Microsoft.AspNetCore.Mvc;

namespace CENT.UI.Controllers
{
    [ApiController]
    [Route("status")]
    public class StatusController : ControllerBase
    {
        [HttpGet]
        public IActionResult StatusResponse()
        {
            return Ok();
        }
        
    }
}