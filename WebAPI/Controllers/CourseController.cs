﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CENT.BL;
using CENT.BL.Domain.TestPackage;
using CENT.UI.DTOs.Tests.Course;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CENT.UI.Controllers
{
    [ApiController]
    [Route("API/[controller]s")]
    [Authorize]
    public class CourseController : Controller
    {
        private readonly CourseService _courseService;

        public CourseController(CourseService courseService)
        {
            _courseService = courseService;
        }


        [HttpGet]
        public async Task<IActionResult> GetCoursesByUserId()
        {
            var courses = await _courseService.GetCoursesByUserId(User.Identity.Name);
            var courseDtos = new List<CourseDTO>();

            foreach (var course in courses) courseDtos.Add(CreateCourseDTO(course));

            return Ok(courseDtos);
        }

        // POST
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CourseCreateDTO course)
        {
            var crs = await _courseService.CreateCourse(course.Name, course.AmountOfStudents, course.SchoolYear,
                User.Identity.Name);
            return Ok(new CourseCreateDTO(crs));
        }

        // DELETE
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] CourseDeleteDTO course)
        {
            return Ok(await _courseService.DeleteCourse(course.Id, User.Identity.Name));
        }

        // PUT
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] EditCourseDTO course)
        {
            await _courseService.UpdateCourse(course.Id, course.Name,
                course.AmountOfStudents, course.Schooljaar, course.TestsIds, User.Identity.Name);
            return NoContent();
        }

        // PUT    /api/courses/:courseId/tests/:testId
        [HttpPut("{courseId}/tests/{testId}")]
        public async Task<IActionResult> AddTestToCourse(string courseId, string testId)
        {
            await _courseService.AddTestToCourse(courseId, testId, User.Identity.Name);
            return NoContent();
        }

        // DELETE    /api/courses/:courseId/tests/:testId
        [HttpDelete("{courseId}/tests/{testId}")]
        public async Task<IActionResult> RemoveTestFromCourse(string courseId, string testId)
        {
            await _courseService.RemoveTestFromCourse(courseId, testId, User.Identity.Name);
            return NoContent();
        }

        [HttpGet("latest")]
        public async Task<IActionResult> GetLatestCourses()
        {
            var courses = await _courseService.GetLatestCourses(User.Identity.Name);
            var courseDtos = new List<CourseDTO>();

            foreach (var course in courses) courseDtos.Add(CreateCourseDTO(course));

            return Ok(courseDtos);
        }

        private CourseDTO CreateCourseDTO(Course course)
        {
            var testIds = new List<string>();
            foreach (var t in course.TestCourses) testIds.Add(t.TestId);

            var cour = new CourseDTO
            {
                Id = course.Id,
                Name = course.Name,
                AmountOfStudents = course.AmountOfStudents,
                SchoolYear = course.SchoolYear,
                UserId = course.Owner.Id,
                TestsIds = testIds
            };
            return cour;
        }
    }
}