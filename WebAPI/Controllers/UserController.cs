﻿using System.Threading.Tasks;
using CENT.BL;
using CENT.UI.DTOs;
using CENT.UI.DTOs.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CENT.UI.Controllers
{
    [ApiController]
    [Route("API/[controller]s")]
    public class UserController : Controller
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        // POST   api/users
        [HttpPost]
        [Authorize(Roles = "SUPER_ADMIN")]
        public async Task<IActionResult> RegisterUser([FromBody] RegistrationDTO dto)
        {
            var user = await _userService.Register(dto.Email, dto.Password, dto.Role, dto.FirstName, dto.LastName);
            return Created($"/api/users/{user.Id}", new UserDTO(user));
        }

        // POST   api/users/requestpassword
        [HttpPost("requestpassword")]
        public async Task<IActionResult> RequestNewPassword([FromBody] RequestPasswordDTO dto)
        {
            var user = await _userService.RequestNewPassword(dto.Email);
            if (!user) return NotFound();
            return Ok("1");
        }

        // POST   api/users/resetpassword
        [HttpPost("resetpassword")]
        public async Task<IActionResult> SetNewPassword([FromBody] ResetPasswordDTO dto)
        {
            var user = await _userService.SetNewPassword(dto.Key);
            if (!user) return NotFound();
            return Ok("1");
        }

        // PUT    api/users/:userId
        [Authorize]
        [HttpPut("{userId}")]
        public async Task<IActionResult> UpdateUser(string userId, [FromBody] UserEditDTO dto)
        {
            var user = await _userService.UpdateUser(userId, User.Identity.Name,
                dto.FirstName, dto.LastName, dto.Email, dto.Password, dto.NewPassword);
            return Ok(new UserDTO(user));
        }

        // GET    api/users/me
        [HttpGet("me")]
        [Authorize]
        public async Task<IActionResult> GetCurrentUserInfo()
        {
            var user = await _userService.GetUser(User.Identity.Name);
            return Ok(new UserDTO(user));
        }
    }
}