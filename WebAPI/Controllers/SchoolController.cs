﻿using System.Threading.Tasks;
using CENT.BL;
using CENT.BL.Domain.PageParameters;
using CENT.UI.DTOs.Schools;
using CENT.UI.DTOs.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CENT.UI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("API/[controller]s")]
    public class SchoolController : Controller
    {
        private readonly SchoolService _schoolService;

        public SchoolController(SchoolService schoolService)
        {
            _schoolService = schoolService;
        }

        [HttpGet]
        [Authorize(Roles = "SUPER_ADMIN")]
        public async Task<IActionResult> Index([FromQuery] SchoolPageParams schoolPageParams)
        {
            var schoolPageList = await _schoolService.GetAllSchools(schoolPageParams);

            var metadata = new
            {
                schoolPageList.TotalCount,
                schoolPageList.PageSize,
                schoolPageList.CurrentPage,
                schoolPageList.TotalPages,
                schoolPageList.HasNext,
                schoolPageList.HasPrevious
            };
            HttpContext.Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

            return Ok(new SchoolsDTO(schoolPageList.List));
        }

        [HttpGet("{schoolId}")]
        [Authorize]
        public async Task<IActionResult> GetSchoolById(string schoolId)
        {
            var school = await _schoolService.GetSchoolById(schoolId);
            return Ok(new SchoolDto(school));
        }

        [HttpPut("{schoolId}")]
        [Authorize]
        public async Task<IActionResult> UpdateSchoolById(string schoolId, [FromBody] SchoolEditDto schoolEditDto)
        {
            var school = await _schoolService.UpdateSchoolById(schoolId, schoolEditDto.Name, schoolEditDto.Address,
                schoolEditDto.Email, schoolEditDto.Phone, schoolEditDto.LoginMail);
            return Ok(new SchoolDto(school));
        }

        [HttpPost]
        [Authorize(Roles = "SUPER_ADMIN")]
        public async Task<IActionResult> CreateSchool([FromBody] SchoolDto schoolDto)
        {
            var school = await _schoolService.CreateSchool(schoolDto.Name, schoolDto.Email, schoolDto.Address,
                schoolDto.Phone, schoolDto.Lang, schoolDto.LoginMail);
            return Ok(new SchoolDto(school));
        }

        [HttpGet("teachers")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> GetTeachersFromSchool([FromQuery] PageParameters pageParameters)
        {
            var teachersPagedList = await _schoolService.GetAllTeachersAdmin(User.Identity.Name, pageParameters);

            var metadata = new
            {
                teachersPagedList.TotalCount,
                teachersPagedList.PageSize,
                teachersPagedList.CurrentPage,
                teachersPagedList.TotalPages,
                teachersPagedList.HasNext,
                teachersPagedList.HasPrevious
            };
            HttpContext.Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            return Ok(new UsersDTO(teachersPagedList.List));
        }

        [HttpPatch("{schoolId}/block")]
        [Authorize(Roles = "SUPER_ADMIN")]
        public async Task<IActionResult> BlockSchool(string schoolId)
        {
            return Ok(new SchoolDto(await _schoolService.BlockSchool(schoolId, User.Identity.Name, true)));
        }

        [HttpPatch("{schoolId}/unblock")]
        [Authorize(Roles = "SUPER_ADMIN")]
        public async Task<IActionResult> UnblockSchool(string schoolId)
        {
            return Ok(new SchoolDto(await _schoolService.BlockSchool(schoolId, User.Identity.Name, false)));
        }

        // GET    api/teachers/:schoolid
        [HttpGet("teachers/{schoolId}")]
        [Authorize]
        public async Task<IActionResult> GetLatestTeachers(string schoolId)
        {
            var users = await _schoolService.GetLatestTeachers(schoolId);
            return Ok(new UsersDTO(users));
        }

        // GET    api/schools/latest
        [HttpGet("latest")]
        [Authorize(Roles = "SUPER_ADMIN")]
        public IActionResult GetLatestSchools()
        {
            var schools = _schoolService.GetLatestSchools();
            return Ok(new SchoolsDTO(schools));
        }
    }
}