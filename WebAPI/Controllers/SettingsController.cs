﻿using System.Threading.Tasks;
using CENT.BL;
using CENT.UI.DTOs.Tests.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CENT.UI.Controllers
{
    [ApiController]
    [Route("API/[controller]")]
    [Authorize]
    public class SettingsController : Controller
    {
        private readonly SettingsService _settingsService;

        public SettingsController(SettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        [HttpGet("{settingsId}")]
        public async Task<IActionResult> GetSettingById(string settingsId)
        {
            var testSetting = await _settingsService.GetSettingsById(settingsId);
            return Ok(testSetting);
        }

        [HttpPut("{testSettingId}/edit")]
        public async Task<IActionResult> EditTest(string testSettingId, [FromBody] TestSettingsDTO testSettingsEdit)
        {
            return Ok(
                await _settingsService.SetTestSettings(testSettingId, testSettingsEdit.AllowFreeAnswering,
                    testSettingsEdit.AllowSeeScore,
                    testSettingsEdit.AllowViewAnswersWhenFinished, testSettingsEdit.TrueColor,
                    testSettingsEdit.FalseColor)
            );
        }

        [HttpPut("{settingsId}/color/true")]
        public async Task<IActionResult> SetTrueColorForSettings(string settingsId,
            [FromBody] ColorSettingsDTO colorSettingsDto)
        {
            return Ok(await _settingsService.SetTrueColor(settingsId, colorSettingsDto.Color));
        }

        [HttpPut("{settingsId}/color/false")]
        public async Task<IActionResult> SetFalseColorForSettings(string settingsId,
            [FromBody] ColorSettingsDTO colorSettingsDto)
        {
            return Ok(await _settingsService.SetFalseColor(settingsId, colorSettingsDto.Color));
        }
    }
}