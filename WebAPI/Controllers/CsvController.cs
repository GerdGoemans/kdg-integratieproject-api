using System;
using System.Threading.Tasks;
using CENT.BL;
using CENT.BL.Domain.Exception;
using CENT.BL.Domain.TestPackage;
using CENT.UI.DTOs.Tests;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CENT.UI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class CsvController : ControllerBase
    {
        private readonly CsvService _csvService;
        private readonly TestService _testService;

        public CsvController(CsvService csvService, TestService testService)
        {
            _csvService = csvService;
            _testService = testService;
        }


        // create a new test based on a CSV file
        // POST   /api/csv
        [HttpPost]
        public async Task<IActionResult> UploadTestFromCsv(IFormFile file)
        {
            // perform file check
            ForceFileCheck(file);

            // create empty test with title
            var test = await _testService.CreateTest(
                file.FileName,
                Visibility.PRIVATE,
                "en",
                User.Identity.Name
            );

            // insert questions and statements into test
            test = await _csvService.AddStatementsToTest(test, file.OpenReadStream());

            return Created(new Uri($"/api/tests/{test.Id}", UriKind.Relative),
                new TestDTO(test, User.Identity.Name));
        }

        [HttpPost("{testId}/explanations")]
        public async Task<IActionResult> AddWordExplanations(IFormFile file, string testId)
        {
            // perform file check
            ForceFileCheck(file);

            // fetch test
            var test = await _testService.GetTestById(testId, User.Identity.Name);

            // add word explanations
            await _csvService.AddWordExplanations(test, file.OpenReadStream());

            return NoContent();
        }

        // output an existing test to a CSV file
        // GET    /api/csv/:testId
        [HttpGet("{testId}")]
        public async Task<IActionResult> GenerateCsvFile(string testId)
        {
            // fetch test
            var test = await _testService.GetTestById(testId, User.Identity.Name);

            // generate csv file
            var stream = _csvService.ExportTestToCsv(test);
            stream.Position = 0; // reset position
            return File(stream, "text/csv", $"{test.Title}");
        }

        [HttpGet("{testId}/explanations")]
        public async Task<IActionResult> GenerateWordExplanationCsv(string testId)
        {
            // fetch test
            var test = await _testService.GetTestById(testId, User.Identity.Name);

            // generate csv file
            var stream = _csvService.ExportExplanationsToCsv(test);
            stream.Position = 0; // reset position
            return File(stream, "text/csv", $"{test.Title}-explanations");
        }

        // == HELPERS ======================
        private void ForceFileCheck(IFormFile file, string csvText = "text/csv",
            string excel = "application/vnd.ms-excel")
        {
            if (file == null)
                throw new BadRequestException("file can not be null, please upload a file");

            if (file.ContentType != csvText && file.ContentType != excel)
                throw new BadRequestException($"incorrect file type, must be of type '{csvText}' or '{excel}'");
        }
    }
}