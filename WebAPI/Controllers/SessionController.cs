using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.Core.Internal;
using CENT.BL;
using CENT.BL.Domain.PageParameters;
using CENT.BL.Domain.PartyPackage;
using CENT.BL.Domain.TestPackage;
using CENT.UI.DTOs.Party;
using CENT.UI.DTOs.Tests.Answer;
using CENT.UI.DTOs.Tests.Question;
using CENT.UI.DTOs.Tests.Session;
using CENT.UI.DTOs.Users;
using CENT.UI.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CENT.UI.Controllers
{
    [ApiController]
    [Route("API/[controller]s")]
    public class SessionController : Controller
    {
        private readonly PartyService _partyService;
        private readonly SessionService _sessionService;
        private readonly TestService _testService;

        public SessionController(SessionService sessionService, TestService testService, PartyService partyService)
        {
            _sessionService = sessionService;
            _testService = testService;
            _partyService = partyService;
        }

        [HttpPost("create/{testId}")]
        [Authorize]
        public async Task<IActionResult> CreateSession(string testId, [FromBody] CreateSessionDTO sessionDto)
        {
            var s = await _sessionService.CreateSession(
                testId,
                User.Identity.Name,
                sessionDto.Questions.ToList(),
                sessionDto.Type,
                sessionDto.Randomized,
                sessionDto.AllowFreeAnswering,
                sessionDto.AllowSeeScore,
                sessionDto.AllowViewAnswersWhenFinished,
                sessionDto.AgreesColor,
                sessionDto.DisagreesColor,
                sessionDto.CourseId
            );

            return Ok(new SessionDTO(s));
        }

        [HttpGet("{pin}")]
        public async Task<IActionResult> GetSessionByPin(string pin)
        {
            return Ok(new SessionDTO(await _sessionService.GetSessionsByPin(pin)));
        }

        [HttpGet("{testId}/votetest")]
        public async Task<IActionResult> GetVoteTestSessionPin(string testId)
        {
            var test = await _testService.GetVoteTestById(testId);
            return Ok(await _sessionService.GetVoteTestSessionPin(test));
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetSessionsByUserId([FromQuery] SessionPageParams pageParameters)
        {
            var testsPageList = await _sessionService.GetAllSessionsFromTeacher(User.Identity.Name, pageParameters);

            var metadata = new
            {
                testsPageList.TotalCount,
                testsPageList.PageSize,
                testsPageList.CurrentPage,
                testsPageList.TotalPages,
                testsPageList.HasNext,
                testsPageList.HasPrevious
            };
            HttpContext.Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

            return Ok(new SessionsDTO(testsPageList.List));
        }

        [HttpGet("id/{id}")]
        public async Task<IActionResult> GetSessionById(string id)
        {
            return Ok(new SessionDTO(await _sessionService.GetSessionById(id)));
        }

        [HttpGet("{sessionId}/results")]
        public async Task<IActionResult> GetSessionResultAsTeacher(string sessionId)
        {
            var session = await _sessionService.GetSessionById(sessionId);
            var sessionMembers = (await _sessionService.GetAllSessionMembersBySession(session)).ToList();
            var questions = session.Questions;

            var questionResults = new List<TeacherSessionQuestionResultDTO>();
            var answers = new List<QuestionAnswerResultDTO>();
            var arguments = new List<string>();

            //loop trough all questions
            foreach (var questionNode in questions)
            {
                //get the question string
                var questionString = questionNode.Value.QuestionString;

                //loop through all possible answers
                foreach (var valueAnswer in questionNode.Value.Answers)
                {
                    var count = 0;
                    foreach (var sessionMember in sessionMembers)
                    {
                        var asm = (await _sessionService.GetSessionAnswersBySessionMember(sessionMember.Id))
                            .Where(s => s.Answer.Id == valueAnswer.Id).ToList();

                        count += asm.Count();

                        //split comments on new line and add to argument list
                        foreach (var sessionAnswer in asm)
                        {
                            if (sessionAnswer.Argument.IsNullOrEmpty()) continue;
                            var args = sessionAnswer.Argument.Split("\n").ToList();
                            arguments.AddRange(args);
                        }
                    }

                    answers.Add(new QuestionAnswerResultDTO
                    {
                        Answer = new AnswerDTO(valueAnswer),
                        AnswerCount = count
                    });
                }

                //return question string and possible answers
                var questionResultDto = new TeacherSessionQuestionResultDTO
                {
                    QuestionString = questionString,
                    AnswerResultDto = new List<QuestionAnswerResultDTO>(answers),
                    Arguments = new List<string>(arguments)
                };
                questionResults.Add(questionResultDto);
                answers.Clear();
                arguments.Clear();
            }

            var teacherSessionResultDto = new TeacherSessionResultDTO
            {
                QuestionResults = questionResults
            };

            return Ok(teacherSessionResultDto);
        }

        [HttpGet("results/{opaque}")]
        public async Task<IActionResult> GetSessionResult(string opaque)
        {
            var correct = 0;
            // Lists to get the questions and parties related to user
            var questionResultDtos = new List<QuestionResultDTO>();
            var partyRelatedDtos = new List<PartyRelatedDTO>();

            var party = new Party();

            // fetch current session member
            var currentSessionMember = await _sessionService.GetSessionMemberByOpaque(opaque);
            var session = currentSessionMember.Session;

            switch (session.Type)
            {
                case GameType.PARTY_GAME:
                    party = await _partyService.GetParty(currentSessionMember.PartyId);
                    var resultParty = GetResultPartyGame.getResults(party, currentSessionMember, session);
                    questionResultDtos = resultParty.dtos;
                    correct = resultParty.correct;
                    break;
                case GameType.DEBATE_GAME:
                    var resultDebate = GetResultDebateGame.GetResults(currentSessionMember, session);
                    questionResultDtos = resultDebate.questionResultDtos;
                    partyRelatedDtos = resultDebate.relatedDtos;
                    break;
                case GameType.NORMAL_GAME:
                    if (session.Test.IsVotingPoll)
                    {
                        var resultVotingGame = GetResultDebateGame.GetResults(currentSessionMember, session);
                        questionResultDtos = resultVotingGame.questionResultDtos;
                        partyRelatedDtos = resultVotingGame.relatedDtos;
                    }
                    else
                    {
                        var resultNormal = GetResultNormalGame.getResults(currentSessionMember, session);
                        questionResultDtos = resultNormal.dtos;
                        correct = resultNormal.correct;
                    }

                    break;
                default:
                    correct = currentSessionMember.Answers.Count(a =>
                        a.Answer.Correct.HasValue && a.Answer.Correct.Value);
                    break;
            }

            var totalQuestionCount = session.Questions.Count();

            return Ok(new SessionResultDTO
            {
                NumberOfQuestions = totalQuestionCount.ToString(),
                Party = new PartyDTO(party),
                Score = correct.ToString(),
                Title = session.Name,
                Questions = questionResultDtos,
                PartyRelated = partyRelatedDtos
            });
        }

        [HttpGet("latest")]
        public async Task<IActionResult> GetLatestSessions()
        {
            var sessions = await _sessionService.GetLatestSessions(User.Identity.Name);
            var sessionDtos = new List<SessionDTO>();
            foreach (var session in sessions) sessionDtos.Add(new SessionDTO(session));

            return Ok(sessionDtos);
        }

        [HttpGet("{sessionId}/results/participation")]
        public async Task<IActionResult> GetParticipationData(string sessionId)
        {
            var session = await _sessionService.GetSessionById(sessionId);
            var sessionMemberList = await _sessionService.GetAllSessionMembersBySession(session);
            //to list because possible enumerations
            var sessionMembers = sessionMemberList.ToList();
            //how many players played
            var playerCount = sessionMembers.Count;

            //how many answers were registered
            var answerCount = sessionMembers.Sum(sessionMember => sessionMember.Answers.Count);

            //party test
            //How many players selected what party
            var selectedPartyDtos = new List<SelectedPartyDTO>();
            if (session.Type != GameType.PARTY_GAME)
                return Ok(new ParticipationDataDTO
                {
                    Playercount = playerCount,
                    AnswerCount = answerCount,
                    SelectedPartyCount = selectedPartyDtos
                });
            var testParties = session.Test.TestParties.AsEnumerable();

            foreach (var testParty in testParties)
            {
                var smForParty = (await _sessionService.GetSessionMembersByParty(testParty.PartyId)).ToList();
                var count = smForParty.Count(sm => sm.Session.Id == session.Id);

                selectedPartyDtos.Add(new SelectedPartyDTO
                {
                    Party = new PartyDTO(await _partyService.GetParty(testParty.PartyId)),
                    count = count
                });
            }

            return Ok(new ParticipationDataDTO
            {
                Playercount = playerCount,
                AnswerCount = answerCount,
                SelectedPartyCount = selectedPartyDtos
            });
        }
    }
}