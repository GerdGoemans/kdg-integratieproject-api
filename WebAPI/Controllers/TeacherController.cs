using System.Threading.Tasks;
using CENT.BL;
using CENT.UI.DTOs.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CENT.UI.Controllers
{
    [ApiController]
    [Route("api/[controller]s")]
    public class TeacherController : ControllerBase
    {
        private readonly UserService _userService;

        public TeacherController(UserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> CreateTeacher([FromBody] UserDTO dto)
        {
            var teacher = await _userService.CreateTeacher(
                User.Identity.Name,
                dto.Email, dto.FirstName, dto.LastName);
            return Created($"/api/teachers/{teacher.Id}", new UserDTO(teacher));
        }

        [HttpDelete("{teacherId}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> DeleteTeacher(string teacherId)
        {
            await _userService.DeleteTeacher(User.Identity.Name, teacherId);
            return NoContent();
        }
    }
}