using System.Threading.Tasks;
using CENT.BL;
using CENT.UI.DTOs;
using CENT.UI.DTOs.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CENT.UI.Controllers
{
    [ApiController]
    [Route("/auth")]
    public class AuthController : Controller
    {
        private readonly AuthService _authService;
        private readonly UserService _userService;

        public AuthController(AuthService authService, UserService userService)
        {
            _authService = authService;
            _userService = userService;
        }


        /**
        * Returns an auth token for a user with given email and password
        */
        [HttpPost("token")] // POST    /auth/token
        public async Task<IActionResult> Authenticate([FromBody] AuthenticationDTO dto)
        {
            var user = await _userService.GetUserByEmail(dto.Email);
            var token = _authService.Authenticate(user, dto.Password);
            return Ok(new UserDTO(user, token));
        }

        [HttpGet]
        [Authorize(Roles = "ADMIN, SUPER_ADMIN")]
        public IActionResult protectedRoute()
        {
            // returns the id of the current auth user
            // authorize in this context means ->
            //     a valid JWT token signed with the correct sym signature and from the correct issuer
            return Ok(User.Identity.Name);
        }
    }
}