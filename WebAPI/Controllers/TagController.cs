﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CENT.BL;
using CENT.UI.DTOs.Tests.Tag;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CENT.UI.Controllers
{
    [ApiController]
    [Authorize(Roles = "SUPER_ADMIN")]
    [Route("API/[controller]s")]
    public class TagController : Controller
    {
        private readonly TagService _tagService;

        public TagController(TagService tagService)
        {
            _tagService = tagService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetAllTags()
        {
            var tags = await _tagService.GetAllTags();
            var tagDtos = new List<TagDTO>();

            foreach (var tag in tags) tagDtos.Add(new TagDTO(tag));

            return Ok(tagDtos);
        }

        [HttpPut]
        public async Task<IActionResult> CreateTag([FromBody] TagDTO tag)
        {
            return Ok(await _tagService.CreateTag(tag.Name, tag.Color));
        }

        [HttpPatch("{tagId}")]
        public async Task<IActionResult> EditTag(string tagId, [FromBody] TagDTO tag)
        {
            return Ok(await _tagService.UpdateTag(tagId, tag.Name, tag.Color));
        }
    }
}