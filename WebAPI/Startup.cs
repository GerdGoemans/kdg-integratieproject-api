using CENT.BL.Domain.Interfaces;
using CENT.BL.Domain.StartupSettings;
using CENT.DAL;
using CENT.UI.Configuration;
using CENT.UI.Container;
using CENT.UI.Helpers;
using CENT.UI.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CENT.UI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // -- load application settings ----------
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            var appSettings = appSettingsSection.Get<AppSettings>();

            // -- preload assemblies -----------------
            AssemblyLoader.Load("Centenials*.dll");

            // -- register: db context ---------------
            services.AddStemtestContext(appSettings);

            // -- register: catalogs & services ------
            services.ScanGeneric(typeof(ICatalog<>));
            services.Scan(typeof(IService), (collection, type) => collection.AddScoped(type));
            services.Scan(typeof(ISingleton), (collection, type) => collection.AddSingleton(type));

            // -- setup: middleware ------------------
            services.AddStemtestAuthentication(appSettings);
            services.AddStemtestCors(appSettings);

            // register controllers with http exception filter
            services.AddControllers(options =>
                options.Filters.Add(new HttpExceptionFilter()));

            // register signalR hubs
            services.AddStemtestSignalR(appSettings);

            // -- setup: initializers -----------------
            services.AddScoped<DummyDataInitializer>();
            services.AddScoped<FirstBootAccountCheck>();
        }

        // This method gets called by the runtime. Use this method to add services to the container.

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCors(StemtestCors.DefaultCorsPolicy);
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<SessionHub>("/sessionHub").RequireCors(StemtestCors.CorsWithAuthPolicy);
            });
        }
    }
}