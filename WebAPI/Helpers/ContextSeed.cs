using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CENT.UI.Helpers
{
    public static class ContextSeed
    {
        public static IHost Initialize(this IHost host)
        {
            // create new scope for initialization
            using var scope = host.Services.GetService<IServiceScopeFactory>().CreateScope();

            // try to seed data
            var seeder = scope.ServiceProvider.GetService<DummyDataInitializer>();
            seeder.TrySeed().Wait();

            // try to create 
            var bootCheck = scope.ServiceProvider.GetService<FirstBootAccountCheck>();
            bootCheck.SuperAdminCheck().Wait();

            return host;
        }
    }
}