namespace CENT.UI.Helpers
{
    public class ExceptionResponse
    {
        public ExceptionResponse(string message, int statusCode)
        {
            Message = message;
            StatusCode = statusCode;
        }

        public string Message { get; }
        public int StatusCode { get; }
    }
}