﻿using System.Collections.Generic;
using System.Linq;
using CENT.BL.Domain.TestPackage;
using CENT.UI.DTOs.Tests.Answer;
using CENT.UI.DTOs.Tests.Question;

namespace CENT.UI.Helpers
{
    public class GetResultNormalGame
    {
        public static (List<QuestionResultDTO> dtos, int correct) getResults(SessionMember currentSessionMember,
            Session session)
        {
            var questionResultDtos = new List<QuestionResultDTO>();
            var correct = 0;
            // get statements
            foreach (var question in session.Questions)
            {
                var memberAnswer = currentSessionMember.Answers
                    .FirstOrDefault(a => a.Answer.Question.Id == question.Value.Id);
                var answerList = new List<AnswerDTO>();
                foreach (var answer in question.Value.Answers)
                {
                    if (memberAnswer != null && memberAnswer.Answer.Id == answer.Id && answer.Correct == true)
                        correct++;

                    answerList.Add(new AnswerDTO(answer));
                }


                var questionResultDto = new QuestionResultDTO(question.Value, memberAnswer.Answer, answerList, null);
                questionResultDtos.Add(questionResultDto);
            }

            return (questionResultDtos, correct);
        }
    }
}