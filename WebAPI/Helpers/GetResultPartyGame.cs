﻿using System.Collections.Generic;
using System.Linq;
using CENT.BL.Domain.PartyPackage;
using CENT.BL.Domain.TestPackage;
using CENT.UI.DTOs.Tests.Answer;
using CENT.UI.DTOs.Tests.Question;

namespace CENT.UI.Helpers
{
    public class GetResultPartyGame
    {
        public static (List<QuestionResultDTO> dtos, int correct) getResults(Party party,
            SessionMember currentSessionMember, Session session)
        {
            var questionResultDtos = new List<QuestionResultDTO>();
            var correct = 0;

            // get statements
            foreach (var question in session.Questions)
            {
                // fetch all statements from party
                var partyStatementsDto = session.Test.Statements
                    .Where(s => s.Answer.Question.Id == question.Value.Id && s.Party.Id == party.Id)
                    .Select(s => new StatementDTO(s))
                    .ToList();

                // get matching user answer
                var matchingUserAnswer = currentSessionMember.Answers
                    .FirstOrDefault(a => a.Answer.Question.Id == question.Value.Id);


                // count the amount of times the user answered correct
                foreach (var statement in partyStatementsDto)
                    if (matchingUserAnswer != null && matchingUserAnswer.Answer.Value == statement.PartyAnswer)
                        correct++;


                // add new question result dto
                questionResultDtos.Add(new QuestionResultDTO
                {
                    Answers = new List<AnswerDTO>(),
                    QuestionId = question.Value.Id,
                    QuestionString = question.Value.QuestionString,
                    UserAnswer = matchingUserAnswer?.Answer.Value,
                    Statements = partyStatementsDto
                });
            }

            return (questionResultDtos, correct);
        }
    }
}