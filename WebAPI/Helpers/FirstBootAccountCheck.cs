﻿using System;
using System.Threading.Tasks;
using CENT.BL;
using CENT.BL.Domain.Interfaces;
using CENT.BL.Domain.StartupSettings;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CENT.UI.Helpers
{
    /**
     * A helper class that on boot checks if any super-admin accounts are known in the system.
     */
    public class FirstBootAccountCheck : IService
    {
        private readonly UserService _userService;
        private readonly AppSettings _settings;
        private readonly ILogger<FirstBootAccountCheck> _logger;

        public FirstBootAccountCheck(
            UserService userService, 
            IOptions<AppSettings> settings,
            ILogger<FirstBootAccountCheck> logger
            )
        {
            _userService = userService;
            _settings = settings.Value;
            _logger = logger;
        }

        /**
         * Checks if a super admin is present, if there is at least one, it won't do anything.
         * If no super admins are found, a new one will be created using the data provided in appsettings.json
         */
        public async Task SuperAdminCheck()
        {
            if (string.IsNullOrEmpty(_settings.SuperAdminEmail) || string.IsNullOrEmpty(_settings.SuperAdminPassword))
            {
                _logger.LogWarning("No default super admin email found. You're application might be started" +
                                   "without any accounts!");
                return;
            }


            var accountMade =
                await _userService.SuperAdminCheck(_settings.SuperAdminEmail, _settings.SuperAdminPassword);
            if (accountMade)
                _logger.LogInformation("Super admin account has been created");
            else
                _logger.LogWarning("Super admin account creation was skipped");
        }
    }
}