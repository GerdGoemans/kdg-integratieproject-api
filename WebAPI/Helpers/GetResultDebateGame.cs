﻿using System;
using System.Collections.Generic;
using System.Linq;
using CENT.BL.Domain.TestPackage;
using CENT.UI.DTOs.Party;
using CENT.UI.DTOs.Tests.Answer;
using CENT.UI.DTOs.Tests.Question;

namespace CENT.UI.Helpers
{
    public class GetResultDebateGame
    {
        public static (List<QuestionResultDTO> questionResultDtos, List<PartyRelatedDTO> relatedDtos) GetResults(
            SessionMember currentSessionMember, Session session)
        {
            var questionResultDtos = new List<QuestionResultDTO>();
            var partyRelatedDtos = new List<PartyRelatedDTO>();

            // get party related values
            foreach (var tp in session.Test.TestParties)
            {
                var statements = tp.Party.Statements.ToList();
                var answerIds = statements.Where(s => s.Answer.Question.Test.Id == session.Test.Id)
                    .Select(s => s.Answer.Id).ToList();

                double count = currentSessionMember.Answers.Count(a => answerIds.Contains(a.Answer.Id));
                double answerIdsCount = answerIds.Count();
                var related = count / answerIdsCount * 100.0;

                partyRelatedDtos.Add(
                    new PartyRelatedDTO
                    {
                        Party = new PartyDTO(tp.Party),
                        AmountRelated = (int) related
                    });
            }

            // get statements
            foreach (var question in session.Questions)
            {
                // fetch all statements from party
                var s = session.Test.Statements.ToList();
                var statementDtos = s.Where(s => s.Answer.Question.Id == question.Value.Id)
                    .Select(s => new StatementDTO(s)).ToList();

                // get matching user answer
                var matchingUserAnswer = currentSessionMember.Answers
                    .FirstOrDefault(a => a.Answer.Question.Id == question.Value.Id);

                // add new question result dto
                questionResultDtos.Add(new QuestionResultDTO
                {
                    Answers = new List<AnswerDTO>(),
                    QuestionId = question.Value.Id,
                    QuestionString = question.Value.QuestionString,
                    UserAnswer = matchingUserAnswer?.Answer.Value,
                    Statements = statementDtos
                });
            }

            return (questionResultDtos, partyRelatedDtos);
        }
    }
}