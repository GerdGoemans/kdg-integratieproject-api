using System;
using CENT.BL.Domain.Exception;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CENT.UI.Helpers
{
    /**
     * A class that automatically handles exceptions for us.
     * Certain types of exceptions (defined in Domain/Exceptions) will give certain responses
     * If another type of exception is thrown we return a 500
     */
    public class HttpExceptionFilter : IActionFilter, IOrderedFilter
    {
        // no action needed during execution
        public void OnActionExecuting(ActionExecutingContext context)
        {
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception == null) return; //Obviously only return an exceptionResponse if there is an exception
            if (context.Exception is HttpException exception)
            {
                var response = new ExceptionResponse(exception.Message, exception.Status);
                context.Result = new ObjectResult(response)
                {
                    StatusCode = exception.Status
                };
            }
            else
            {
                // default error handling
                var response = new ExceptionResponse("an internal error has occured", 500);
                Console.Error.WriteLine(context.Exception);
                context.Result = new ObjectResult(response)
                {
                    StatusCode = 500
                };
            }

            // exception has been handled
            context.ExceptionHandled = true;
        }

        public int Order { get; } = int.MaxValue - 10; // high running order -> has proiority
    }
}