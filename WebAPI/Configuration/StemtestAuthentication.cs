using System.Text;
using System.Threading.Tasks;
using CENT.BL.Domain.StartupSettings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace CENT.UI.Configuration
{
    public static class StemtestAuthentication
    {
        public static void AddStemtestAuthentication(this IServiceCollection services, AppSettings settings)
        {
            // get byte array from secret
            var key = Encoding.ASCII.GetBytes(settings.Secret);

            services.AddAuthentication(ao =>
            {
                ao.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                ao.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(bearer =>
            {
                // add token validation rules
                bearer.RequireHttpsMetadata = false;
                bearer.SaveToken = true;
                bearer.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateAudience = false,
                    ValidateIssuer = true,
                    ValidIssuer = "centenials.stemtest" // TODO: get value from settings
                };


                bearer.Events = new JwtBearerEvents
                {
                    // bind token to context for signalR
                    OnMessageReceived = context =>
                    {
                        var token = context.Request.Query["access_token"];
                        var path = context.HttpContext.Request.Path;
                        if (!string.IsNullOrEmpty(token) && path.StartsWithSegments("/sessionHub"))
                            context.Token = token;

                        return Task.CompletedTask;
                    }
                };
            });
        }
    }
}