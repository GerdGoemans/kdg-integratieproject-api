using System;
using CENT.BL.Domain.StartupSettings;
using CENT.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CENT.UI.Configuration
{
    public static class StemtestContext
    {
        private static readonly ILoggerFactory MyLoggerFactory
            = LoggerFactory.Create(builder => { builder.AddDebug(); });

        public static void AddStemtestContext(this IServiceCollection services, AppSettings settings)
        {
            services.AddDbContext<StemtestDbContext>(options =>
            {
                // default configuration
                options.UseLazyLoadingProxies();

                options.UseLoggerFactory(MyLoggerFactory);

                // database provider configuration
                if (settings.DatabaseType == "UseSqlite")
                {
                    string connectionString;
                    if (string.IsNullOrEmpty(settings.ConnectionString))
                    {
                        /*Get the users folder and create the database there.  If user folder cannot be found it'll be created in the folder where the app is executed from*/
                        var folder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                        if (folder == "") folder = "./";
                        if (!folder.EndsWith("/") && !folder.EndsWith("\\")) folder = folder + "/";
                        var file = folder + "StemTest_EFCodeFirst.db";
                        connectionString = "Data Source=" + file;
                    }
                    else
                    {
                        connectionString = settings.ConnectionString;
                    }

                    //Configure data source
                    options.UseSqlite(connectionString);
                }
                else if (settings.DatabaseType == "UseMySQL")
                {
                    options.UseMySQL(settings.ConnectionString);
                }
                else
                {
                    throw new Exception(
                        "Invalid database type received, please use any of the following: \"UseMySQL\", \"UseSqlite\"");
                }
            });
        }
    }
}