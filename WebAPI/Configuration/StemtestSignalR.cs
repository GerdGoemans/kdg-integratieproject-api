using System;
using CENT.BL.Domain.StartupSettings;
using Microsoft.Extensions.DependencyInjection;

namespace CENT.UI.Configuration
{
    public static class StemtestSignalR
    {
        public static void AddStemtestSignalR(this IServiceCollection services, AppSettings settings)
        {
            var builder = services.AddSignalR();

            if (!String.IsNullOrEmpty(settings.RedisConnection))
            {
                builder.AddStackExchangeRedis(settings.RedisConnection);
            }
        }
    }
}