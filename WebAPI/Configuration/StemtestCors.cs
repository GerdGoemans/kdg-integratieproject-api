using CENT.BL.Domain.StartupSettings;
using Microsoft.Extensions.DependencyInjection;

namespace CENT.UI.Configuration
{
    public static class StemtestCors
    {
        public static readonly string DefaultCorsPolicy = "_CentenialsCorsPolicy_default";
        public static readonly string CorsWithAuthPolicy = "_CentenialsCorsPolicy_withAuth";

        public static void AddStemtestCors(this IServiceCollection services, AppSettings settings)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowCors", builder =>
                {
                    // default cors policy
                    options.AddPolicy(DefaultCorsPolicy, policyBuilder =>
                    {
                        policyBuilder
                            .AllowAnyHeader() // allow all header
                            .WithExposedHeaders("X-Pagination") // allow exposed headers
                            .AllowAnyOrigin() // allow all origins
                            .AllowAnyMethod(); // allow all methods
                    });

                    // cors with auth policy
                    options.AddPolicy(CorsWithAuthPolicy, policyBuilder =>
                    {
                        // add origins from settings
                        foreach (var origin in settings.Origins) policyBuilder = policyBuilder.WithOrigins(origin);

                        // rest of configuration
                        policyBuilder
                            .SetIsOriginAllowedToAllowWildcardSubdomains()
                            .AllowAnyHeader()
                            .WithExposedHeaders("X-Pagination")
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials();
                    });
                });
            });
        }
    }
}